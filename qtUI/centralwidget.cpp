#include "centralwidget.h"

#include "objparser.h"

CentralWidget::CentralWidget(QWidget *parent) : QWidget(parent) {

  initializeCentralWidget();

  connectObjects();

  //  viewer_.setFocusPolicy(Qt::FocusPolicy::StrongFocus);
}

CentralWidget::~CentralWidget() {
  delete vBoxLayoutSelectionButtons;
  delete vBoxLayoutGeneralButtons;
  delete hBoxLayoutButtons;
  delete vBoxLayout;

  delete signalMapper_;
}

void CentralWidget::initializeCentralWidget() {
  importBtn.setText(QString("Import file"));
  cancelBtn.setText(QString("Cancel"));
  plotBtn.setText(QString("Plot"));
  solverBtn.setText(QString("Run Solver"));
  defaultSelectionBtn.setText("Default Selection Mode");
  pickSelectionBtn.setText("Pick Selection Mode");
  polySelectionBtn.setText("Polygon Selection Mode");
  clearBtn.setText("Clear");

  vBoxLayoutGeneralButtons->addWidget(&importBtn);
  vBoxLayoutGeneralButtons->addWidget(&cancelBtn);
  vBoxLayoutGeneralButtons->addWidget(&plotBtn);
  vBoxLayoutGeneralButtons->addWidget(&solverBtn);
  vBoxLayoutSelectionButtons->addWidget(&defaultSelectionBtn);
  vBoxLayoutSelectionButtons->addWidget(&pickSelectionBtn);
  vBoxLayoutSelectionButtons->addWidget(&polySelectionBtn);
  vBoxLayoutSelectionButtons->addWidget(&clearBtn);

  vBoxLayout->addLayout(vBoxLayoutGeneralButtons);
  vBoxLayout->addLayout(vBoxLayoutSelectionButtons);

  hBoxLayoutButtons->addLayout(vBoxLayout);
  hBoxLayoutButtons->addWidget(&ploter_);
  hBoxLayoutButtons->setStretch(0, 50);
  hBoxLayoutButtons->setStretch(1, 850);

  //  Viewer *tmpViewer = new Viewer;
  //  vBoxLayout->addWidget(&viewer_);

  this->setLayout(hBoxLayoutButtons);
}

void CentralWidget::connectObjects() {

  initSignalMap();

  //  QObject::connect(&plotBtn, SIGNAL(clicked()), &this->ploter_,
  //                   SLOT(plotData(bbqPolyLine *)));

  QObject::connect(&cancelBtn, SIGNAL(clicked()), this->parent(),
                   SLOT(close()));
  QObject::connect(&importBtn, SIGNAL(clicked()), &fileManager_,
                   SLOT(openFiles()));

  connect(&defaultSelectionBtn, SIGNAL(clicked()), signalMapper_, SLOT(map()));
  connect(&pickSelectionBtn, SIGNAL(clicked()), signalMapper_, SLOT(map()));
  connect(&polySelectionBtn, SIGNAL(clicked()), signalMapper_, SLOT(map()));
}

void CentralWidget::initSignalMap() {
  signalMapper_ = new QSignalMapper(this);
  signalMapper_->setMapping(&defaultSelectionBtn, QString("DEFAULT"));
  signalMapper_->setMapping(&pickSelectionBtn, QString("PICK"));
  signalMapper_->setMapping(&polySelectionBtn, QString("POLY"));

  //  signalMapper_->setMapping(&plotBtn, );

  //  connect(signalMapper_, SIGNAL(mapped(QString)), &viewer_,
  //          SLOT(activateViewerMode(QString)));
  //  connect(signalMapper_, SIGNAL(mapped(bbqPolyLine &)), &ploter_,
  //          SLOT(plotData(bbqPolyLine &)));
}
