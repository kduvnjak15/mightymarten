#ifndef VIEWERMODE_H
#define VIEWERMODE_H

#include "BB_Mesh.h"
#include "viewermode.h"
#include <QMatrix4x4>
#include <QOpenGLBuffer>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>

#include <QOpenGLWidget>
#include <QWidget>
#include <camera3d.h>
#include <set>
#include <transform3d.h>

class ViewerMode : public QOpenGLFunctions_4_3_Core {
public:
  ViewerMode(uint &width, uint &height, bb::nVec<uint> &selected,
             bb::nVec<bb::Vec4> &colors);
  virtual void initViewerMode() = 0;
  virtual void resizeGL(int width, int height) = 0;
  virtual void paintGL() = 0;
  virtual void updateSelection() = 0;
  virtual ~ViewerMode();

  virtual void keyPressEvent(QKeyEvent *event) = 0;
  virtual void mouseReleaseEvent(QMouseEvent *event) = 0;
  virtual void mouseMoveEvent(QMouseEvent *event) = 0;
  void setFramebufferObject(QOpenGLFramebufferObject **fbo) { fbo_ = fbo; }

protected:
  virtual void createShaderProgram() = 0;

public:
  QOpenGLFramebufferObject **fbo_;
  uint &width_;
  uint &height_;
  bb::nVec<uint> &selected_;
  bb::nVec<bb::Vec4> &colors_;
  std::set<int> tmpMap;

  QOpenGLShaderProgram *tmpShaderProgram;
  uint vaoID;
  uint colID;
  uint numOfIndices_;
};

class DefaultViewerMode : public ViewerMode {

public:
  DefaultViewerMode(QMatrix4x4 &projection, Camera3D &camera,
                    Transform3D &model, uint &width, uint &height,
                    bb::nVec<uint> &selected, bb::nVec<bb::Vec4> &colors);

  ~DefaultViewerMode();

  // ViewerMode interface
public:
  virtual void initViewerMode();
  virtual void resizeGL(int width, int height) {}
  virtual void paintGL();
  virtual void keyPressEvent(QKeyEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void updateSelection() {}

private:
  virtual void createShaderProgram();

protected:
  QMatrix4x4 &m_projection;
  Camera3D &m_camera;
  Transform3D &m_model;

  // Shader Uniform Locations
  int u_modelToWorld;
  int u_worldToCamera;
  int u_cameraToView;
  int u_modelRotationMatrix;
  int u_color;
};

class PickViewerMode : public ViewerMode {
public:
  PickViewerMode(QMatrix4x4 &projection, Camera3D &camera, Transform3D &model,
                 uint &width, uint &height, bb::nVec<uint> &selected,
                 bb::nVec<bb::Vec4> &colors);
  ~PickViewerMode();

public:
  virtual void initViewerMode();
  virtual void resizeGL(int width, int height);
  virtual void paintGL();

  virtual void keyPressEvent(QKeyEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void updateSelection();

private:
  virtual void createShaderProgram();

protected:
  QMatrix4x4 &m_projection;
  Camera3D &m_camera;
  Transform3D &m_model;

  // Framebuffer object
  QOpenGLShaderProgram fbo_program;

  // FBO shader uniform location
  int fbo_modelToWorld;
  int fbo_worldToCamera;
  int fbo_cameraToView;
  int fbo_modelRotationMatrix;
  int fbo_color;
  int fbo_selected;
  int fbo_numOfPrimitives;

  // shader information
  int u_modelToWorld;
  int u_worldToCamera;
  int u_cameraToView;
  int u_modelRotationMatrix;
  int u_color;
  int u_selected;

  bb::Mesh resMesh;
  uint colBuff_;
};

class PolygonViewerMode : public ViewerMode {
public:
  PolygonViewerMode(QMatrix4x4 &projection, Camera3D &camera,
                    Transform3D &model, uint &width, uint &height,
                    bb::nVec<uint> &selected, bb::nVec<bb::Vec4> &colors);
  ~PolygonViewerMode();

public:
  virtual void initViewerMode();
  virtual void resizeGL(int width, int height);
  virtual void paintGL();

  virtual void keyPressEvent(QKeyEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void updateSelection() {
    for (uint el = 0; el < numOfIndices_ / 3; el++) {

      if (tmpMap.find(el) != tmpMap.end()) {
        for (uint i = 0; i < 3; i++) {
          colors_[el * 3 + i] = bb::Vec4(1.0, 0.0, 0.0, 1.0f);
        }
      } else {
        for (uint i = 0; i < 3; i++) {
          colors_[el * 3 + i] = bb::Vec4(0.0, 1.0, 1.0, 1.0f);
        }
      }
    }

    glBindBuffer(GL_ARRAY_BUFFER, colID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(double) * 4 * colors_.size(),
                    colors_.first());
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    for (auto &el : tmpMap) {
      std::cout << el << ", ";
    }
    std::cout << std::endl;
  }

  void activate() { selectingActive = true; }
  void deactivate() { selectingActive = false; }

private:
  virtual void createShaderProgram();
  void detectPrimitiveIDs();
  void resetPolyline();

protected:
  QMatrix4x4 &m_projection;
  Camera3D &m_camera;
  Transform3D &m_model;

protected:
  // shader information
  int u_modelToWorld;
  int u_worldToCamera;
  int u_cameraToView;
  int u_modelRotationMatrix;
  int u_color;
  int u_viewerWidth;
  int u_viewerHeight;

  int pl_modelToWorld;
  int pl_worldToCamera;
  int pl_cameraToView;
  int pl_modelRotationMatrix;

  int pl_viewerWidth;
  int pl_viewerHeight;

  bool selectingActive = true;
  bool detectPrimitiveIDs_ = false;
  int tf_numOfPrimitives;
  int tf_dots;

  uint polyVertBuff;
  uint polyLineVao_;
  uint tbo;

  QOpenGLShaderProgram *polyLineProgram;
  bb::nVec<bb::Vec2> clickedPt_;
};

#endif // VIEWERMODE_H
