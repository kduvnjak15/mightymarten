QT += core  widgets

TARGET = 2_3DRendering
CONFIG += c++14 console
CONFIG -= app_bundle



# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    femsolver.cpp \
        main.cpp \
    vertex.cpp \
    transform3d.cpp \
    glwidget.cpp \
    keyconverter.cpp \
    inputmanager.cpp \
    camera3d.cpp \
    plotwidget.cpp \
    ../Source/BB_Mesh.cpp \
    ../Source/BB_LinAlgebra.cpp \
    filemanager.cpp \
    centralwidget.cpp \
    viewer.cpp \
    objparser.cpp \
    viewermode.cpp


HEADERS += \
    femsolver.h \
    solver.h \
    vertex.h \
    transform3d.h \
    glwidget.h \
    keyconverter.h \
    inputmanager.h \
    camera3d.h \
    plotwidget.h \
    filemanager.h \
    centralwidget.h \
    viewer.h \
    objparser.h \
    viewermode.h

RESOURCES += \
    resource.qrc

DISTFILES += \
    simple.vert \
    simple.frag

INCLUDEPATH += $$PWD/../Include

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build_support/release/ -lLIBCUDA
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build_support/debug/ -lLIBCUDA
#else:unix: LIBS += -L$$PWD/../build_support/ -lLIBCUDA

INCLUDEPATH += $$PWD/../build_support
DEPENDPATH += $$PWD/../build_support

#win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build_support/release/libLIBCUDA.a
#else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build_support/debug/libLIBCUDA.a
#else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build_support/release/LIBCUDA.lib
#else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build_support/debug/LIBCUDA.lib
#else:unix: PRE_TARGETDEPS += $$PWD/../build_support/libLIBCUDA.a
