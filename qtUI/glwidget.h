#ifndef GLWIDGET_H
#define GLWIDGET_H

#include "transform3d.h"
#include <QMatrix4x4>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>

class QOpenGLShaderProgram;

class glWidget : public QOpenGLWidget, protected QOpenGLFunctions {
  Q_OBJECT

public:
  glWidget();
  ~glWidget();

  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();
  void tearDownGL();

protected slots:
  void update();

private:
  QOpenGLBuffer m_vertex;
  QOpenGLVertexArrayObject m_object;
  QOpenGLShaderProgram *m_program;

  // Shader information
  int u_modelToWorld;
  int u_worldToView;
  QMatrix4x4 m_projection;
  Transform3D m_transform;

  // private helpers
  void printContextInformation();
};

#endif // GLWIDGET_H
