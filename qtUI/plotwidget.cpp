#include "plotwidget.h"
#include <QPainter>
#include <inputmanager.h>
#include <iostream>

PlotWidget::PlotWidget(QOpenGLWidget *parent) : QOpenGLWidget(parent) {
  m_model.translate(0.0f, 0.0f, -15.0f);
}

void PlotWidget::initializeGL() { //   Initialize OpenGL Backend
  glInitialized = initializeOpenGLFunctions();
  std::cout << "PlotWidget::initializeGL" << glInitialized << std::endl;

  connect(context(), SIGNAL(aboutToBeDestroyed()), this, SLOT(teardownGL()),
          Qt::DirectConnection);
  connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));

  bool res;
  res = plotShader_.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                            ":/shaders/plot2d.vert");
  std::cout << "Vertex shader complete " << res << std::endl;
  res = plotShader_.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                            ":/shaders/plot2d.frag");
  std::cout << "Frag shader complete " << res << std::endl;

  plotShader_.link();

  u_projMatrix = plotShader_.uniformLocation("projMatrix");

  glClearColor(0.2, 0.2, 0.22, 0.1);

  // init axes
  bb::Vec2 tmp(-0.9, 0.9);
  axespts.push_back(tmp);
  tmp = bb::Vec2(-0.9, -0.9);
  axespts.push_back(tmp);
  tmp = bb::Vec2(0.9, -0.9);
  axespts.push_back(tmp);
  axes_ = new bbqPolyLinePoints(axespts);

  return;
}

void PlotWidget::resizeGL(int width, int height) {}

void PlotWidget::paintGL() {

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(this->plotShader_.programId());

  //  qDebug() << projMatrix_;

  glUniformMatrix4fv(u_projMatrix, 1, GL_FALSE, projMatrix_.constData());
  for (uint i = 0; i < avatars.size(); i++) {
    avatars[i]->render();
  }
  glUseProgram(0);

  renderAxes();
  renderText();

  return;
}

PlotWidget::~PlotWidget() { this->avatars.clear(); }

bool PlotWidget::checkGLInitialized() {
  if (!this->glInitialized) {
    //    throw std::runtime_error("MAJOR ERROR!!!  Cannot plot anything until "
    //                             "plotWidget has initialized GL context!!!");
    return false;
  }

  return glInitialized;
}

void PlotWidget::plotData(bb::nVec<bb::Vec2> &points) {

  //// BUG; TBD
  if (points.size() < 2)
    return;

  checkGLInitialized();

  bbqPolyLine *linedata = new bbqPolyLinePoints(points);

  this->appendAvatar(linedata);
}

void PlotWidget::plotData(bb::nVec<double> &data) {

  //// BUG; TBD
  if (data.size() < 2)
    return;

  checkGLInitialized();

  bbqPolyLine *linedata = new bbqPolyLineData(data);

  this->appendAvatar(linedata);
}

void PlotWidget::plotData(bb::Mesh &meshObj) {

  if (meshObj.getNumVertices() < 1)
    return;

  checkGLInitialized();

  bbqMesh *meshData = new bbqMesh(meshObj);

  this->appendAvatar(meshData);
}

void PlotWidget::teardownGL() {
  // Actually destroy our OpenGL information
}

void PlotWidget::update() {
  Input::update();

  static const float transSpeed = 0.5f;
  static const float rotSpeed = 0.5f;

  // Camera Transformation
  if (Input::buttonPressed(Qt::LeftButton)) {
    m_model.rotate(-rotSpeed * Input::mouseDelta().x(), Camera3D::LocalUp);
    m_model.rotate(-rotSpeed * Input::mouseDelta().y(), m_model.right());
  }

  if (Input::buttonPressed(Qt::RightButton)) {
    m_model.translate(transSpeed * 0.05 * Input::mouseDelta().x() *
                      Camera3D::LocalRight);
    m_model.translate(transSpeed * -0.05 * Input::mouseDelta().y() *
                      Camera3D::LocalUp);
  }

  if (Input::buttonTriggered(Qt::LeftButton)) {
    //    pickedPoints_.push_back(
    //        QVector2D(Input::mousePosition().x(),
    //        Input::mousePosition().y()));
  }

  // Handle rotations
  //    m_camera.rotate(-rotSpeed * Input::mouseDelta().x(),
  //    Camera3D::LocalUp);
  //    m_camera.rotate(-rotSpeed * Input::mouseDelta().y(),
  //    m_camera.right());

  // Handle translations
  QVector3D translation;
  // Update instance information
  if (Input::getWheelQueue().getSize() > 0) {

    //    m_model.rotate(2.0f * (Input::getWheelQueue().front() /
    //                           std::abs(Input::getWheelQueue().front())),
    //                   QVector3D(0.4f, 0.3f, 0.3f));
    translation += m_camera.forward() *
                   (Input::getWheelQueue().front() /
                    std::abs(Input::getWheelQueue().front())) *
                   0.2;

    Input::getWheelQueue().dequeue();
  }

  if (Input::keyPressed(Qt::Key_W)) {
    translation += m_camera.forward();
  }
  if (Input::keyPressed(Qt::Key_S)) {
    translation -= m_camera.forward();
  }
  if (Input::keyPressed(Qt::Key_A)) {
    translation -= m_camera.right();
  }
  if (Input::keyPressed(Qt::Key_D)) {
    translation += m_camera.right();
  }
  if (Input::keyPressed(Qt::Key_Q)) {
    translation -= m_camera.up();
  }
  if (Input::keyPressed(Qt::Key_E)) {
    translation += m_camera.up();
  }
  m_camera.translate(transSpeed * translation);
  //  }
  // Schedule a redraw
  QOpenGLWidget::update();
}

void PlotWidget::keyPressEvent(QKeyEvent *event) {
  if (event->isAutoRepeat()) {
    event->ignore();
  } else {
    Input::registerKeyPress(event->key());
  }
}

void PlotWidget::keyReleaseEvent(QKeyEvent *event) {
  if (event->isAutoRepeat()) {
    event->ignore();
  } else {
    Input::registerKeyRelease(event->key());
  }
}

void PlotWidget::mousePressEvent(QMouseEvent *event) {
  Input::registerMousePress(event->button());
  this->buttonDown = true;
}

void PlotWidget::mouseReleaseEvent(QMouseEvent *event) {
  Input::registerMouseRelease(event->button());
  this->buttonDown = false;

  std::cout << event->pos().x() << std::endl;
  //  this->capture(event->pos().x(), this->size().height() -
  //  event->pos().y());

  makeCurrent();
}

void PlotWidget::mouseMoveEvent(QMouseEvent *event) {}

void PlotWidget::wheelEvent(QWheelEvent *wheel) {
  Input::registerMouseWheel(wheel->delta());
}

void PlotWidget::appendAvatar(bbqGeomView *avatarPtr) {
  //  std::cout << lineData->getMin() << lineData->getMax() << std::endl;

  std::cout << "append line avatar " << avatarPtr << ", "
            << this->isInitialized() << std::endl;

  //  std::cout << avatarPtr->getMinPt() << std::endl;
  this->updateMinMax(avatarPtr->getMinPt(), avatarPtr->getMaxPt());
  this->updateProjMatrix();

  avatars.push_back(avatarPtr);
}

void PlotWidget::renderAxes() { axes_->render(); }

void PlotWidget::renderText() {

  if (!painter_.isActive()) {
    painter_.begin(this);
    painter_.setPen(Qt::red);
  }

  painter_.drawText(QPointF(100, 310), QString("backo"));
  painter_.end();
}

void PlotWidget::updateMinMax(const bb::Vec2 &tmpMin, const bb::Vec2 &tmpMax) {
  if (this->minPt_.x() > tmpMin.x())
    this->minPt_.x() = tmpMin.x();
  if (this->minPt_.y() > tmpMin.y())
    this->minPt_.y() = tmpMin.y();

  if (this->maxPt_.x() < tmpMax.x())
    this->maxPt_.x() = tmpMax.x();
  if (this->maxPt_.y() < tmpMax.y())
    this->maxPt_.y() = tmpMax.y();

  return;
}

void PlotWidget::updateProjMatrix() {

  double dx = maxPt_.x() - minPt_.x();
  double dy = maxPt_.y() - minPt_.y();

  std::cout << "YMCA" << maxPt_.x() << ", " << maxPt_.y() << " | " << minPt_.x()
            << ", " << minPt_.y() << std::endl;

  //  std::cout << dx << ":" << dy << std::endl;
  if (dx <= 0 || dy <= 0)
    throw std::runtime_error("catastrophic error, negative screen width ");

  projMatrix_.setToIdentity();
  projMatrix_.ortho(minPt_.x(), maxPt_.x(), minPt_.y(), maxPt_.y(), 1.0f,
                    -1.0f);

  qDebug() << projMatrix_;
  for (uint i = 0; i < 4; i++) {
    for (uint j = 0; j < 4; j++) {
      std::cout << projMatrix_.column(i)[j] << std::endl;
    }
  }

  return;
}

PlotWindow::PlotWindow() {
  plotWidget_ = new PlotWidget;
  plotWidget_->setParent(this);

  this->layout = new QHBoxLayout;

  this->layout->addWidget(plotWidget_);

  this->setLayout(this->layout);

  PlotDockWidget *plw = new PlotDockWidget;
  plw->setParent(this);
  plw->show();
  this->addDockWidget(Qt::TopDockWidgetArea, plw);
}

void PlotWindow::setData(bb::nVec<bb::Vec2> &pts) {
  plotWidget_->plotData(pts);
}

void PlotWindow::show() { this->plotWidget_->repaint(); }

PlotWindow::~PlotWindow() { delete plotWidget_; }

PlotDockWidget::PlotDockWidget() { widget_.setParent(this->widget()); }

bbqPolyLine::bbqPolyLine() {}

bbqPolyLine::bbqPolyLine(bb::nVec<double> &pts) {

  this->linePtr_ = new bbqPolyLineData(pts);

  this->min_ = this->linePtr_->getMinPt();
  this->max_ = this->linePtr_->getMaxPt();
}

bbqPolyLine::bbqPolyLine(bb::nVec<bb::Vec2> &pts) {

  this->linePtr_ = new bbqPolyLinePoints(pts);

  pts.subscribe("update", this);

  this->min_ = this->linePtr_->getMinPt();
  this->max_ = this->linePtr_->getMaxPt();
}

void bbqPolyLine::setData(bb::nVec<bb::Vec2> &pts) {

  bbqPolyLinePoints *pointsPtr =
      dynamic_cast<bbqPolyLinePoints *>(this->linePtr_);
  if (pointsPtr != nullptr) {
    pointsPtr->setData(pts);
  }

  this->calcMinMax();
}

void bbqPolyLine::setData(bb::nVec<double> &data) {

  bbqPolyLineData *dataPtr = dynamic_cast<bbqPolyLineData *>(this->linePtr_);
  if (dataPtr != nullptr) {
    dataPtr->setData(data);
  }

  this->calcMinMax();
}

bbqPolyLine::~bbqPolyLine() {
  if (this->linePtr_)
    delete this->linePtr_;
}

void bbqPolyLine::initializeGLdata() {

  bool status = initializeOpenGLFunctions();
  std::cout << "bbqLine::initializeOpenGLFunctions()" << status << std::endl;
  glInitialized = status;

  if (!glInitialized) {
    std::cout << "bbqPolyLine::initializeGLdata :  glInitialized is false; "
                 "MAJOR WARNING!!!"
              << std::endl;
    return;
  }

  this->createShaderProgram();

  glGenVertexArrays(1, &this->VAO_);
  glGenBuffers(1, &this->VBO_);

  return;
}

void bbqPolyLinePoints::resetGLdata() {

  glBindVertexArray(this->VAO_);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, this->VBO_);
  glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * this->points_->size(),
               &this->points_->first()->x(), GL_DYNAMIC_DRAW);

  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return;
}

const bb::Vec2 &bbqGeomView::getMaxPt() { return this->max_; }

const bb::Vec2 &bbqGeomView::getMinPt() { return this->min_; }

bbqGeomView::bbqGeomView() {}

GLuint bbqGeomView::getVAO() { return this->VAO_; }

bbqGeomView::~bbqGeomView() {}

bbqMesh::bbqMesh(bb::Mesh &meshObj) : mesh_(&meshObj) {

  this->mesh_->subscribe("update", this);

  this->initializeGLdata();

  this->calcMinMax();
}

bbqMesh::~bbqMesh() { this->mesh_->unsubscribe("update", this); }

void bbqMesh::update(string message) {

  this->resetGLdata();

  std::cout << "bbqMesh::update " << message << std::endl;
}

void bbqMesh::render() {

  glBindVertexArray(this->VAO_);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glDrawElements(GL_POLYGON, this->mesh_->getNumFaces() * 5, GL_UNSIGNED_INT,
                 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glBindVertexArray(0);
}

void bbqMesh::createShaderProgram() {

  bool res;
  res = shaderProgram_.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                               ":/shaders/bbqMesh2D.vert");
  std::cout << "bbqMesh::createShaderProgram::Vertex " << res << std::endl;
  res = shaderProgram_.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                               ":/shaders/bbqMesh2D.frag");
  std::cout << "bbqMesh::createShaderProgram::Frag " << res << std::endl;

  shaderProgram_.link();

  shaderProgram_.bind();

  glEnable(GL_PRIMITIVE_RESTART);
  glPrimitiveRestartIndex(RESTART_INDEX);

  u_projMatrix = shaderProgram_.uniformLocation("projMatrix");

  std::cout << "bbqMesh::createShaderProgram complete" << std::endl;
}

void bbqMesh::initializeGLdata() {

  bool status = initializeOpenGLFunctions();
  std::cout << "bbqMesh::initializeOpenGLFunctions()" << status << std::endl;
  glInitialized = status;

  if (!glInitialized) {
    std::cout << "bbqMesh::initializeGLdata :  glInitialized is false; "
                 "MAJOR WARNING!!!"
              << std::endl;
    return;
  }

  this->createShaderProgram();

  glGenVertexArrays(1, &this->VAO_);
  glGenBuffers(1, &this->VBO_);

  this->resetGLdata();
}

void bbqMesh::resetGLdata() {

  glBindVertexArray(this->VAO_);
  glEnableVertexAttribArray(0);

  // get data
  const uint numVertices = this->mesh_->getNumVertices();
  bb::Vec2 vertices[numVertices];
  for (uint i = 0; i < numVertices; i++) {
    vertices[i] = this->mesh_->getVertices()[i].xy();
  }

  std::cout << vertices << std::endl;

  const uint numCells = this->mesh_->getNumCells();
  bb::nVec<uint> indices;
  for (uint i = 0; i < numCells; i++) {
    bb::PolyCell tmpCell = this->mesh_->getCell(i);
    bb::nVec<uint> polyFaceIDs = tmpCell.getCellPolyFaceIDs();
    for (uint j = 0; j < polyFaceIDs.size(); j++) {
      bb::PolyFace tmpFace = this->mesh_->getFace(polyFaceIDs[j]);
      bb::nVec<uint> polyFaceNodeIDs = tmpFace.polyFaceNodes_();
      std::cout << polyFaceNodeIDs << std::endl;
      for (uint k = 0; k < polyFaceNodeIDs.size(); k++) {

        indices.push_back(polyFaceNodeIDs[k]);
      }
      indices.push_back(RESTART_INDEX);
    }
  }

  std::cout << indices << std::endl;
  // get GL buffers
  glBindBuffer(GL_ARRAY_BUFFER, this->VBO_);
  glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
  glBufferData(GL_ARRAY_BUFFER,
               sizeof(bb::Vec2) * this->mesh_->getNumVertices(), vertices,
               GL_DYNAMIC_DRAW);

  glGenBuffers(1, &this->IBO_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->IBO_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint),
               indices.first(), GL_DYNAMIC_DRAW);

  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void bbqMesh::calcMinMax() {
  this->max_ =
      bb::Vec2(this->mesh_->getMaxPt().x(), this->mesh_->getMaxPt().y());

  this->min_ =
      bb::Vec2(this->mesh_->getMinPt().x(), this->mesh_->getMinPt().y());

  std::cout << "bbqMesh::calcMinMax" << this->mesh_->getMinPt() << "<"
            << this->max_ << std::endl;
}

bbqPolyLinePoints::bbqPolyLinePoints(bb::nVec<bb::Vec2> &pts) : bbqPolyLine() {

  this->points_ = &pts;
  this->points_->subscribe("update", this);

  this->initializeGLdata();

  this->calcMinMax();
}

void bbqPolyLinePoints::render() {

  glBindVertexArray(this->VAO_);
  glDrawArrays(GL_LINE_STRIP, 0, this->points_->size());
  glBindVertexArray(0);

  return;
}

void bbqPolyLinePoints::update(string message) {

  this->resetGLdata();
  std::cout << "bbqPolyLinePoints::update " << message << std::endl;
}

bbqPolyLinePoints::~bbqPolyLinePoints() {
  this->points_->unsubscribe("update", this);
}

void bbqPolyLinePoints::createShaderProgram() {
  bool res;
  res = shaderProgram_.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                               ":/shaders/bbqPolyLine.vert");
  std::cout << "Vertex shader complete " << res << std::endl;
  res = shaderProgram_.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                               ":/shaders/bbqPolyLine.frag");
  std::cout << "Frag shader complete " << res << std::endl;

  shaderProgram_.link();

  u_projMatrix = shaderProgram_.uniformLocation("projMatrix");

  std::cout << "bbqPolyLinePoints::createShaderProgram" << std::endl;
}

void bbqPolyLinePoints::initializeGLdata() {
  bbqPolyLine::initializeGLdata();
  this->resetGLdata();
}

void bbqPolyLinePoints::calcMinMax() {
  for (uint i = 0; i < this->points_->size(); i++) {
    if (this->points_->at(i).x() > this->max_.x()) {
      this->max_.x() = this->points_->operator[](i).x();
    }

    if (this->points_->at(i).y() > this->max_.y()) {
      this->max_.y() = this->points_->at(i).y();
    }

    if (this->points_->at(i).x() < this->min_.x()) {
      this->min_.x() = this->points_->at(i).x();
    }

    if (this->points_->at(i).y() < this->min_.y()) {
      this->min_.y() = this->points_->at(i).y();
    }
  }

  std::cout << "bbqPolyLinePoints::calcMinMax(" << this << ") : " << this->min_
            << std::endl;
}

bbqPolyLineData::bbqPolyLineData(bb::nVec<double> &data) {

  this->data_ = &data;
  this->data_->subscribe("update", this);

  this->initializeGLdata();

  this->calcMinMax();
}

void bbqPolyLineData::render() {

  glBindVertexArray(this->VAO_);
  glDrawArrays(GL_LINE_STRIP, 0, this->data_->size());
  glBindVertexArray(0);

  return;
}

void bbqPolyLineData::update(string message) { this->resetGLdata(); }

bbqPolyLineData::~bbqPolyLineData() {
  this->data_->unsubscribe("update", this);
}

void bbqPolyLineData::createShaderProgram() {
  bool res;
  res = shaderProgram_.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                               ":/shaders/bbqPolyLine.vert");
  std::cout << "bbqPolyLineData::createShaderProgram::Vertex " << res
            << std::endl;
  res = shaderProgram_.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                               ":/shaders/bbqPolyLine.frag");
  std::cout << "bbqPolyLineData::createShaderProgram::Frag " << res
            << std::endl;

  shaderProgram_.link();

  u_projMatrix = shaderProgram_.uniformLocation("projMatrix");

  std::cout << "bbqPolyLineData::createShaderProgram end" << std::endl;
}

void bbqPolyLineData::initializeGLdata() {
  bbqPolyLine::initializeGLdata();
  this->resetGLdata();
}

void bbqPolyLineData::resetGLdata() {

  bb::Vec2 tmpData[this->data_->size()];
  for (uint i = 0; i < this->data_->size(); i++) {
    tmpData[i].x() = i;
    tmpData[i].y() = this->data_->at(i);
  }

  glBindVertexArray(this->VAO_);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, this->VBO_);
  glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
  glBufferData(GL_ARRAY_BUFFER, sizeof(tmpData), tmpData, GL_DYNAMIC_DRAW);

  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return;
}

void bbqPolyLineData::calcMinMax() {

  double tmpMin = 1e300;
  double tmpMax = -1e300;
  for (uint i = 0; i < this->data_->size(); ++i) {
    if (data_->at(i) < tmpMin)
      tmpMin = data_->at(i);

    if (data_->at(i) > tmpMax)
      tmpMax = data_->at(i);
  }

  this->min_ = bb::Vec2(0, tmpMin);
  this->max_ = bb::Vec2(this->data_->size(), tmpMax);
}
