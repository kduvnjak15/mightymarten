#include "femsolver.h"

FEMsolver::FEMsolver(QObject *parent) : QObject(parent) {}

void FEMsolver::setMesh(bb::Mesh &mesh) {
  this->meshPtr_ = std::make_unique<bb::Mesh>(mesh);
}

void FEMsolver::setNodeSelection(bb::NodeSelection &selection) {}

void FEMsolver::runSolver() {}
