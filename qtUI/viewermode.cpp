#include "viewermode.h"
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <inputmanager.h>
#include <objparser.h>

#include <QOpenGLFramebufferObjectFormat>

ViewerMode::ViewerMode(uint &width, uint &height, bb::nVec<uint> &selected,
                       bb::nVec<bb::Vec4> &colors)
    : width_(width), height_(height), selected_(selected), colors_(colors) {

  // Set global information
  std::cout << "ViewerMode::initializeOpenGLFunctions: "
            << initializeOpenGLFunctions() << std::endl;

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glClearColor(0.2f, 0.6f, 0.5f, 1.0f);
}

ViewerMode::~ViewerMode() {}

DefaultViewerMode::DefaultViewerMode(QMatrix4x4 &projection, Camera3D &camera,
                                     Transform3D &model, uint &width,
                                     uint &height, bb::nVec<uint> &selected,
                                     bb::nVec<bb::Vec4> &colors)
    : ViewerMode(width, height, selected, colors), m_projection(projection),
      m_camera(camera), m_model(model) {

  this->createShaderProgram();
}

DefaultViewerMode::~DefaultViewerMode() {}

void DefaultViewerMode::initViewerMode() {}

void DefaultViewerMode::paintGL() {
  // Clear
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // Render using our shader
  glUseProgram(tmpShaderProgram->programId());
  glUniformMatrix3fv(u_modelRotationMatrix, 1, GL_FALSE,
                     m_model.rotation().toRotationMatrix().constData());
  glUniformMatrix4fv(u_worldToCamera, 1, GL_FALSE,
                     m_camera.toMatrix().constData());
  glUniformMatrix4fv(u_cameraToView, 1, GL_FALSE, m_projection.constData());
  glUniformMatrix4fv(u_modelToWorld, 1, GL_FALSE,
                     m_model.toMatrix().constData());

  QOpenGLFramebufferObject::bindDefault();
  {
    glBindVertexArray(vaoID);
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 0.0, 0));

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLE_FAN, numOfIndices_, GL_UNSIGNED_INT, 0);
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 0.0, 1));
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDrawElements(GL_TRIANGLE_FAN, numOfIndices_, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
  }

  glUseProgram(0);
}

void DefaultViewerMode::keyPressEvent(QKeyEvent *event) {}

void DefaultViewerMode::createShaderProgram() {
  // Application-specific initialization
  {
    //    // Create Shader (Do not release until VAO is created)
    tmpShaderProgram = new QOpenGLShaderProgram();
    std::cout << "Vertex shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Vertex, ":/shaders/simple.vert")
              << std::endl;
    std::cout << "Geometry shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Geometry, ":/shaders/simple.geom")
              << std::endl;
    std::cout << "Fragment shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Fragment, ":/shaders/simple.frag")
              << std::endl;

    tmpShaderProgram->link();
    std::cout << "DefaultViewerModeShader::OpenGLShaderProgram.isLinked: "
              << tmpShaderProgram->isLinked() << std::endl;
    tmpShaderProgram->bind();

    glEnable(GL_PRIMITIVE_RESTART);
    glPrimitiveRestartIndex(RESTART_INDEX);

    // Cache Uniform Locations
    u_modelToWorld = tmpShaderProgram->uniformLocation("modelToWorld");
    u_worldToCamera = tmpShaderProgram->uniformLocation("worldToCamera");
    u_cameraToView = tmpShaderProgram->uniformLocation("cameraToView");
    u_modelRotationMatrix = tmpShaderProgram->uniformLocation("modelRotation");
    u_color = tmpShaderProgram->uniformLocation("u_color");
  }
}

void DefaultViewerMode::mouseReleaseEvent(QMouseEvent *event) { return; }

void DefaultViewerMode::mouseMoveEvent(QMouseEvent *event) {}

PickViewerMode::PickViewerMode(QMatrix4x4 &projection, Camera3D &camera,
                               Transform3D &model, uint &width, uint &height,
                               bb::nVec<uint> &selected,
                               bb::nVec<bb::Vec4> &colors)
    : ViewerMode(width, height, selected, colors), m_projection(projection),
      m_camera(camera), m_model(model) {
  this->createShaderProgram();
}

void PickViewerMode::createShaderProgram() {

  {
    //    // Create Shader (Do not release until VAO is created)
    tmpShaderProgram = new QOpenGLShaderProgram();
    std::cout << "Vertex shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Vertex, ":/shaders/simple.vert")
              << std::endl;
    std::cout << "Geometry shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Geometry, ":/shaders/simple.geom")
              << std::endl;
    std::cout << "Fragment shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Fragment, ":/shaders/simple.frag")
              << std::endl;

    tmpShaderProgram->link();
    std::cout << "DefaultViewerModeShader::OpenGLShaderProgram.isLinked: "
              << tmpShaderProgram->isLinked() << std::endl;
    tmpShaderProgram->bind();

    // Cache Uniform Locations
    u_modelToWorld = tmpShaderProgram->uniformLocation("modelToWorld");
    u_worldToCamera = tmpShaderProgram->uniformLocation("worldToCamera");
    u_cameraToView = tmpShaderProgram->uniformLocation("cameraToView");
    u_modelRotationMatrix = tmpShaderProgram->uniformLocation("modelRotation");
    u_color = tmpShaderProgram->uniformLocation("u_color");
  }

  {
    //    // Create Shader (Do not release until VAO is created)
    fbo_program.create();
    std::cout << "Vertex shader successefull: "
              << fbo_program.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                                     ":/shaders/fbo.vert")
              << std::endl;

    std::cout << "Fragment shader successefull: "
              << fbo_program.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                                     ":/shaders/fbo.frag")
              << std::endl;

    fbo_program.link();
    std::cout << "PickViewerModeShader::OpenGLShaderProgram.isLinked: "
              << fbo_program.isLinked() << std::endl;
    fbo_program.bind();

    // Cache Uniform Locations
    fbo_modelToWorld = fbo_program.uniformLocation("modelToWorld");
    fbo_worldToCamera = fbo_program.uniformLocation("worldToCamera");
    fbo_cameraToView = fbo_program.uniformLocation("cameraToView");
    fbo_modelRotationMatrix = fbo_program.uniformLocation("modelRotation");
    fbo_color = fbo_program.uniformLocation("u_color");
    fbo_numOfPrimitives = fbo_program.uniformLocation("numOfPrimitives");

    glUniform1i(fbo_numOfPrimitives, numOfIndices_);
  }
}

PickViewerMode::~PickViewerMode() {}

void PickViewerMode::initViewerMode() {
  std::cout << "PickViewerMode Initialized :" << initializeOpenGLFunctions()
            << std::endl;
}

void PickViewerMode::resizeGL(int width, int height) {
  this->width_ = width;
  this->height_ = height;
}

void PickViewerMode::paintGL() {
  // Clear

  QOpenGLFramebufferObject::bindDefault();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // Render using our shader
  glUseProgram(tmpShaderProgram->programId());
  glUniformMatrix3fv(u_modelRotationMatrix, 1, GL_FALSE,
                     m_model.rotation().toRotationMatrix().constData());
  glUniformMatrix4fv(u_worldToCamera, 1, GL_FALSE,
                     m_camera.toMatrix().constData());
  glUniformMatrix4fv(u_cameraToView, 1, GL_FALSE, m_projection.constData());
  glUniformMatrix4fv(u_modelToWorld, 1, GL_FALSE,
                     m_model.toMatrix().constData());

  {
    glBindVertexArray(vaoID);
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 0.0, 0));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 1.0, 1));
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
  }
  glUseProgram(0);

  if (*fbo_ == nullptr) {
    throw std::runtime_error("PAJDSAD");
  }

  (*fbo_)->bind();

  glUseProgram(fbo_program.programId());
  glUniformMatrix3fv(fbo_modelRotationMatrix, 1, GL_FALSE,
                     m_model.rotation().toRotationMatrix().constData());
  glUniformMatrix4fv(fbo_worldToCamera, 1, GL_FALSE,
                     m_camera.toMatrix().constData());
  glUniformMatrix4fv(fbo_cameraToView, 1, GL_FALSE, m_projection.constData());
  glUniformMatrix4fv(fbo_modelToWorld, 1, GL_FALSE,
                     m_model.toMatrix().constData());
  glUniform1i(fbo_numOfPrimitives, numOfIndices_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  {
    glBindVertexArray(vaoID);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);
  }
  glUseProgram(0);

  //  (*fbo_)->bindDefault();

  //  glUseProgram(fbo_program.programId());
  //  glUniformMatrix3fv(fbo_modelRotationMatrix, 1, GL_FALSE,
  //                     m_model.rotation().toRotationMatrix().constData());
  //  glUniformMatrix4fv(fbo_worldToCamera, 1, GL_FALSE,
  //                     m_camera.toMatrix().constData());
  //  glUniformMatrix4fv(fbo_cameraToView, 1, GL_FALSE,
  //  m_projection.constData()); glUniformMatrix4fv(fbo_modelToWorld, 1,
  //  GL_FALSE,
  //                     m_model.toMatrix().constData());
  //  glUniform1i(fbo_numOfPrimitives, numOfIndices_);
  //  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //  {
  //    glBindVertexArray(vaoID);
  //    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  //    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);
  //  }
  //  glUseProgram(0);
}

void PickViewerMode::keyPressEvent(QKeyEvent *event) {}

void PickViewerMode::mouseReleaseEvent(QMouseEvent *event) {

  (*fbo_)->bind();
  std::cout << "is valid " << (*fbo_)->isValid() << std::endl;
  std::cout << "is NULL " << (*fbo_)->toImage().isNull() << std::endl;
  std::cout << "bound " << (*fbo_)->isBound() << std::endl;
  std::cout << "saved " << (*fbo_)->toImage().save("PickViewerModeFBO", "png")
            << std::endl;

  std::cout << "is bound() after capture " << (*fbo_)->isBound() << std::endl;
  uint xpos = event->pos().x();
  uint ypos = event->pos().y();
  std::cout << event->pos().x() << " , " << event->pos().y() << std::endl;
  std::cout << "w " << width_ << "h " << height_ << std::endl;
  float data[3];
  glReadPixels(xpos, (*fbo_)->height() - ypos, 1, 1, GL_RGB, GL_FLOAT, data);
  std::cout << data[0] * numOfIndices_ << std::endl;
  std::cout << data[1] << std::endl;
  std::cout << data[2] << std::endl;

  if (data[1] > 0) {
    std::cout << "exiting " << std::endl;
    return;
  }
  uint tmpsel;
  tmpsel = (data[0] * numOfIndices_);

  auto sit = tmpMap.find(tmpsel);
  if (sit != tmpMap.end()) {
    tmpMap.erase(sit);
  } else {
    tmpMap.insert(tmpsel);
    std::cout << "Selected : " << tmpsel << std::endl;
  }

  updateSelection();

  return;
}

void PickViewerMode::mouseMoveEvent(QMouseEvent *event) {}
void PickViewerMode::updateSelection() {
  for (uint el = 0; el < numOfIndices_ / 3; el++) {

    if (tmpMap.find(el) != tmpMap.end()) {
      for (uint i = 0; i < 3; i++) {
        colors_[el * 3 + i] = bb::Vec4(1.0, 0.0, 0.0, 1.0f);
      }
    } else {
      for (uint i = 0; i < 3; i++) {
        colors_[el * 3 + i] = bb::Vec4(0.0, 1.0, 1.0, 1.0f);
      }
    }
  }

  glBindBuffer(GL_ARRAY_BUFFER, colID);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(double) * 4 * colors_.size(),
                  colors_.first());
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  for (auto &el : tmpMap) {
    std::cout << el << ", ";
  }
  std::cout << std::endl;

  return;
}

PolygonViewerMode::PolygonViewerMode(QMatrix4x4 &projection, Camera3D &camera,
                                     Transform3D &model, uint &width,
                                     uint &height, bb::nVec<uint> &selected,
                                     bb::nVec<bb::Vec4> &colors)
    : ViewerMode(width, height, selected, colors), m_projection(projection),
      m_camera(camera), m_model(model) {
  this->createShaderProgram();
}

PolygonViewerMode::~PolygonViewerMode() {}

void PolygonViewerMode::initViewerMode() {
  /*
    glGenVertexArrays(1, &polyLineVao_);
    glBindVertexArray(polyLineVao_);
    glEnableVertexAttribArray(0);

    glGenBuffers(1, &polyVertBuff);
    glBindBuffer(GL_ARRAY_BUFFER, polyVertBuff);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * (1 + clickedPt_.size()),
                 clickedPt_.first(), GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &tbo);
    glBindBuffer(GL_ARRAY_BUFFER, tbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * numOfIndices_, nullptr,
                 GL_DYNAMIC_READ);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    */
  resetPolyline();
}

void PolygonViewerMode::resizeGL(int width, int height) {}

void PolygonViewerMode::paintGL() {

  QOpenGLFramebufferObject::bindDefault();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  {
    // Render using our shader
    glUseProgram(tmpShaderProgram->programId());
    glUniformMatrix3fv(u_modelRotationMatrix, 1, GL_FALSE,
                       m_model.rotation().toRotationMatrix().constData());
    glUniformMatrix4fv(u_worldToCamera, 1, GL_FALSE,
                       m_camera.toMatrix().constData());
    glUniformMatrix4fv(u_cameraToView, 1, GL_FALSE, m_projection.constData());
    glUniformMatrix4fv(u_modelToWorld, 1, GL_FALSE,
                       m_model.toMatrix().constData());
    glUniform1i(u_viewerWidth, static_cast<int>(this->width_));
    glUniform1i(u_viewerHeight, static_cast<int>(this->height_));

    glBindVertexArray(vaoID);
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 0.0, 0));
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 1.0, 1));
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    /////
    glUseProgram(0);
  }

  if (selectingActive) {
    glUseProgram(polyLineProgram->programId());
    glUniformMatrix3fv(pl_modelRotationMatrix, 1, GL_FALSE,
                       m_model.rotation().toRotationMatrix().constData());
    glUniformMatrix4fv(pl_worldToCamera, 1, GL_FALSE,
                       m_camera.toMatrix().constData());
    glUniformMatrix4fv(pl_cameraToView, 1, GL_FALSE, m_projection.constData());
    glUniformMatrix4fv(pl_modelToWorld, 1, GL_FALSE,
                       m_model.toMatrix().constData());

    glUniform1i(pl_viewerWidth, static_cast<int>(this->width_));
    glUniform1i(pl_viewerHeight, static_cast<int>(this->height_));

    glBindVertexArray(polyLineVao_);
    glDrawArrays(GL_LINE_LOOP, 0, clickedPt_.size() + 1);
    glBindVertexArray(0);
    glUseProgram(0);
  }

  if (detectPrimitiveIDs_) {
    // Render using our shader
    std::vector<GLfloat> feedback;
    feedback.resize(numOfIndices_, 0);

    glUseProgram(tmpShaderProgram->programId());

    glUniformMatrix3fv(u_modelRotationMatrix, 1, GL_FALSE,
                       m_model.rotation().toRotationMatrix().constData());
    glUniformMatrix4fv(u_worldToCamera, 1, GL_FALSE,
                       m_camera.toMatrix().constData());
    glUniformMatrix4fv(u_cameraToView, 1, GL_FALSE, m_projection.constData());
    glUniformMatrix4fv(u_modelToWorld, 1, GL_FALSE,
                       m_model.toMatrix().constData());
    tmpShaderProgram->setUniformValue(u_color, QVector4D(0.0, 1.0, 0.0, 0));
    tmpShaderProgram->setUniformValue(tf_numOfPrimitives, clickedPt_.size());

    //    GLfloat tmpdejta[clickedPt_.size() * 4];
    std::vector<GLfloat> tmpdejta;
    tmpdejta.resize(clickedPt_.size() * 4);
    for (uint i = 0; i < clickedPt_.size(); i++) {
      tmpdejta[i * 4] = clickedPt_[i].x();
      tmpdejta[i * 4 + 1] = clickedPt_[i].y();
      tmpdejta[i * 4 + 2] = clickedPt_[(i + 1) % clickedPt_.size()].x();
      tmpdejta[i * 4 + 3] = clickedPt_[(i + 1) % clickedPt_.size()].y();
      std::cout << i << ": (" << tmpdejta[i * 4] << ", " << tmpdejta[i * 4 + 1]
                << ") | (" << tmpdejta[i * 4 + 2] << ", " << tmpdejta[i * 4 + 3]
                << "), ";
    }
    std::cout << std::endl;

    tmpShaderProgram->setUniformValueArray(tf_dots, tmpdejta.data(),
                                           clickedPt_.size() * 4, 4);
    glBindBuffer(GL_ARRAY_BUFFER, tbo);

    glBindVertexArray(vaoID);
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 5" << std::endl;
    }

    glEnable(GL_RASTERIZER_DISCARD);
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 1" << std::endl;
    }

    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, tbo);
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 2" << std::endl;
    }
    glBeginTransformFeedback(GL_TRIANGLES);
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 3" << std::endl;
    }
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawElements(GL_TRIANGLES, numOfIndices_, GL_UNSIGNED_INT, 0);

    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 6" << std::endl;
    }
    glBindVertexArray(0);
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 7" << std::endl;
    }
    glEndTransformFeedback();
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 8" << std::endl;
    }
    glDisable(GL_RASTERIZER_DISCARD);
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 9" << std::endl;
    }
    glFlush();
    if (glGetError() != GL_NO_ERROR) {
      std::cout << "sranje 10" << std::endl;
    }

    glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0,
                       sizeof(GLfloat) * feedback.size(), feedback.data());

    for (uint i = 0; i < numOfIndices_; i++) {
      std::cout << feedback[i] << ", ";
      if (feedback[i] >= 0) {
        tmpMap.insert(static_cast<uint>(feedback[i]));
      }
    }
    std::cout << std::endl;

    detectPrimitiveIDs_ = false;
    updateSelection();
    /////

    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }
}

void PolygonViewerMode::keyPressEvent(QKeyEvent *event) {

  if (event->key() == Qt::Key_Return) {
    std::cout << "Enter pressed" << std::endl;
    detectPrimitiveIDs();
    this->deactivate();
  } else if (event->key() == Qt::Key_Escape) {
    std::cout << "Escape pressed" << std::endl;
    this->deactivate();
  }
}

void PolygonViewerMode::mouseReleaseEvent(QMouseEvent *event) {

  if (selectingActive) {

    clickedPt_.push_back(bb::Vec2(event->x(), event->y()));

    glBindBuffer(GL_ARRAY_BUFFER, polyVertBuff);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * (clickedPt_.size() + 1),
                 clickedPt_.first(), GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * (clickedPt_.size()),
                    sizeof(bb::Vec2),
                    clickedPt_.first() + clickedPt_.size() - 1);
    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    std::cout << "PICKED " << clickedPt_ << std::endl;
  } else {

    clickedPt_.clear();

    clickedPt_.push_back(bb::Vec2(event->x(), event->y()));

    this->resetPolyline();

    glBindBuffer(GL_ARRAY_BUFFER, polyVertBuff);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * (clickedPt_.size() + 1),
                 clickedPt_.first(), GL_DYNAMIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * (clickedPt_.size()),
                    sizeof(bb::Vec2),
                    clickedPt_.first() + clickedPt_.size() - 1);
    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    this->activate();
  }
}

void PolygonViewerMode::mouseMoveEvent(QMouseEvent *event) {

  //  std::cout << event->x() << ", " << event->y() << std::endl;

  if (selectingActive) {
    double tmpMousePos[2];
    tmpMousePos[0] = event->x();
    tmpMousePos[1] = event->y();
    glBindBuffer(GL_ARRAY_BUFFER, polyVertBuff);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * clickedPt_.size(),
                    sizeof(double) * 2, tmpMousePos);
    glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
  } else {
  }
}

void PolygonViewerMode::createShaderProgram() {
  {
    //    // Create Shader (Do not release until VAO is created)
    tmpShaderProgram = new QOpenGLShaderProgram();
    std::cout << "Vertex shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Vertex, ":/shaders/simple.vert")
              << std::endl;
    std::cout << "Geometry shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Geometry, ":/shaders/simple.geom")
              << std::endl;
    std::cout << "Fragment shader successefull: "
              << tmpShaderProgram->addShaderFromSourceFile(
                     QOpenGLShader::Fragment, ":/shaders/simple.frag")
              << std::endl;

    const GLchar *feedbackVaryings[] = {"outID"};
    glTransformFeedbackVaryings(tmpShaderProgram->programId(), 1,
                                feedbackVaryings, GL_INTERLEAVED_ATTRIBS);
    tmpShaderProgram->link();

    std::cout << "PolygonViewerModeShader::OpenGLShaderProgram.isLinked: "
              << tmpShaderProgram->isLinked() << std::endl;
    tmpShaderProgram->bind();

    // Cache Uniform Locations
    u_modelToWorld = tmpShaderProgram->uniformLocation("modelToWorld");
    u_worldToCamera = tmpShaderProgram->uniformLocation("worldToCamera");
    u_cameraToView = tmpShaderProgram->uniformLocation("cameraToView");
    u_modelRotationMatrix = tmpShaderProgram->uniformLocation("modelRotation");
    u_color = tmpShaderProgram->uniformLocation("u_color");
    u_viewerWidth = tmpShaderProgram->uniformLocation("u_viewerWidth");
    u_viewerHeight = tmpShaderProgram->uniformLocation("u_viewerHeight");

    tf_numOfPrimitives = tmpShaderProgram->uniformLocation("numOfPickedPoints");
    tf_dots = tmpShaderProgram->uniformLocation("dots");
  }

  {
    //    // Create Shader (Do not release until VAO is created)
    polyLineProgram = new QOpenGLShaderProgram();
    std::cout << "Vertex shader successefull: "
              << polyLineProgram->addShaderFromSourceFile(
                     QOpenGLShader::Vertex, ":/shaders/polyLine.vert")
              << std::endl;

    std::cout << "Fragment shader successefull: "
              << polyLineProgram->addShaderFromSourceFile(
                     QOpenGLShader::Fragment, ":/shaders/polyLine.frag")
              << std::endl;

    polyLineProgram->link();
    std::cout << "PolyLine::OpenGLShaderProgram.isLinked: "
              << polyLineProgram->isLinked() << std::endl;
    polyLineProgram->bind();

    // Cache Uniform Locations
    pl_modelToWorld = polyLineProgram->uniformLocation("modelToWorld");
    pl_worldToCamera = polyLineProgram->uniformLocation("worldToCamera");
    pl_cameraToView = polyLineProgram->uniformLocation("cameraToView");
    pl_modelRotationMatrix = polyLineProgram->uniformLocation("modelRotation");

    pl_viewerWidth = polyLineProgram->uniformLocation("pl_viewerWidth");
    pl_viewerHeight = polyLineProgram->uniformLocation("pl_viewerHeight");
  }
}

void PolygonViewerMode::detectPrimitiveIDs() { detectPrimitiveIDs_ = true; }

void PolygonViewerMode::resetPolyline() {

  glDeleteVertexArrays(1, &polyLineVao_);
  glDeleteBuffers(1, &polyVertBuff);

  glGenVertexArrays(1, &polyLineVao_);
  glBindVertexArray(polyLineVao_);
  glEnableVertexAttribArray(0);

  glGenBuffers(1, &polyVertBuff);
  glBindBuffer(GL_ARRAY_BUFFER, polyVertBuff);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec2) * (1 + clickedPt_.size()),
               clickedPt_.first(), GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_DOUBLE, GL_FALSE, 0, 0);
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glGenBuffers(1, &tbo);
  glBindBuffer(GL_ARRAY_BUFFER, tbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * numOfIndices_, nullptr,
               GL_DYNAMIC_READ);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
}
