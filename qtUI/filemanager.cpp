#include "filemanager.h"
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <iostream>

#include "objparser.h"

bb::nVec<bb::Mesh> FileManager::meshes_;

FileManager::FileManager(QObject *parent) : QObject(parent) {}

bb::nVec<bb::Mesh> &FileManager::getImportedMeshes() { return meshes_; }

QStringList FileManager::getOpenFileNames() {
  return QFileDialog::getOpenFileNames(0, QString("Choose file"),
                                       QDir::homePath(), QString("*.obj"), 0,
                                       QFileDialog::DontUseNativeDialog);
}

void FileManager::openFiles() {

  QStringList files_ = this->getOpenFileNames();

  qDebug() << files_.toStdList();

  for (QString fileName : files_) {

    bb::Mesh newMesh;
    ObjParser::parse(fileName, newMesh);

    //    meshes_.push_back(newMesh);
  }

  bb::TopoGen::generateQuadRect(bb::Vec2(0.0, 0.0), bb::Vec2(10, 20), 10, 20);

  meshes_.push_back(bb::TopoGen::getMeshObj());

  this->importedMeshes();
}
