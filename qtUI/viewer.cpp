#include "viewer.h"
#include "inputmanager.h"
#include "objparser.h"
#include "vertex.h"
#include <QDebug>
#include <QKeyEvent>
#include <QOpenGLDebugLogger>
#include <QOpenGLShaderProgram>
#include <QString>
#include <cstdlib>
#include <iostream>
#include <stdlib.h>

#include "filemanager.h"

#include <QString>
using namespace std;

// Front Verticies
#define VERTEX_FTR                                                             \
  Vertex(QVector3D(0.5f, 0.5f, 0.5f), QVector3D(1.0f, 0.0f, 0.0f))
#define VERTEX_FTL                                                             \
  Vertex(QVector3D(-0.5f, 0.5f, 0.5f), QVector3D(0.0f, 1.0f, 0.0f))
#define VERTEX_FBL                                                             \
  Vertex(QVector3D(-0.5f, -0.5f, 0.5f), QVector3D(0.0f, 0.0f, 1.0f))
#define VERTEX_FBR                                                             \
  Vertex(QVector3D(0.5f, -0.5f, 0.5f), QVector3D(0.0f, 0.0f, 0.0f))

// Back Verticies
#define VERTEX_BTR                                                             \
  Vertex(QVector3D(0.5f, 0.5f, -0.5f), QVector3D(1.0f, 1.0f, 0.0f))
#define VERTEX_BTL                                                             \
  Vertex(QVector3D(-0.5f, 0.5f, -0.5f), QVector3D(0.0f, 1.0f, 1.0f))
#define VERTEX_BBL                                                             \
  Vertex(QVector3D(-0.5f, -0.5f, -0.5f), QVector3D(1.0f, 0.0f, 1.0f))
#define VERTEX_BBR                                                             \
  Vertex(QVector3D(0.5f, -0.5f, -0.5f), QVector3D(1.0f, 1.0f, 1.0f))

// Create a colored cube
static const Vertex sg_vertexes[] = {
    // Face 1 (Front)
    VERTEX_FTR, VERTEX_FTL, VERTEX_FBL, VERTEX_FBL, VERTEX_FBR, VERTEX_FTR,
    // Face 2 (Back)
    VERTEX_BBR, VERTEX_BTL, VERTEX_BTR, VERTEX_BTL, VERTEX_BBR, VERTEX_BBL,
    // Face 3 (Top)
    VERTEX_FTR, VERTEX_BTR, VERTEX_BTL, VERTEX_BTL, VERTEX_FTL, VERTEX_FTR,
    // Face 4 (Bottom)
    VERTEX_FBR, VERTEX_FBL, VERTEX_BBL, VERTEX_BBL, VERTEX_BBR, VERTEX_FBR,
    // Face 5 (Left)
    VERTEX_FBL, VERTEX_FTL, VERTEX_BTL, VERTEX_FBL, VERTEX_BTL, VERTEX_BBL,
    // Face 6 (Right)
    VERTEX_FTR, VERTEX_FBR, VERTEX_BBR, VERTEX_BBR, VERTEX_BTR, VERTEX_FTR};

static const float vertices_cube[] = {0.5f, -0.5f, 0.5f,

                                      0.5f, 0.5f, 0.5f,

                                      -0.5f, 0.5f, 0.5f,

                                      -0.5f, -0.5f, 0.5f,

                                      //

                                      0.5f, -0.5f, -0.5f,

                                      0.5f, 0.5f, -0.5f,

                                      -0.5f, 0.5f, -0.5f,

                                      -0.5f, -0.5f, -0.5f};

static const uint indices_cube[] = {0, 1, 4, 5, 4, 1, 1, 2, 6, 6, 5, 1,
                                    2, 5, 6, 7, 6, 5, 5, 4, 7, 4, 5, 0,
                                    4, 5, 6, 6, 7, 4, 0, 5, 1, 1, 5, 2};

// float vertices[] = {
//     0.5f,  0.5f, 0.0f,  // top right
//     0.5f, -0.5f, 0.0f,  // bottom right
//    -0.5f, -0.5f, 0.0f,  // bottom left
//    -0.5f,  0.5f, 0.0f   // top left
//};
// unsigned int indices[] = {  // note that we start from 0!
//    0, 1, 3,  // first Triangle
//    1, 2, 3   // second Triangle
//};
#undef VERTEX_BBR
#undef VERTEX_BBL
#undef VERTEX_BTL
#undef VERTEX_BTR

#undef VERTEX_FBR
#undef VERTEX_FBL
#undef VERTEX_FTL
#undef VERTEX_FTR

/*******************************************************************************
 * OpenGL Events
 ******************************************************************************/

Viewer::Viewer() {
  std::cout << "poly " << std::endl;
  m_model.translate(0.0f, 0.0f, -15.0f);
}

void Viewer::initializeGL() {

  //   Initialize OpenGL Backend
  std::cout << "Viewer::init" << initializeOpenGLFunctions() << std::endl;

  connect(context(), SIGNAL(aboutToBeDestroyed()), this, SLOT(teardownGL()),
          Qt::DirectConnection);
  connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
  printVersionInformation();

  //  QOpenGLDebugLogger *logger = new QOpenGLDebugLogger(this);
  //  logger->initialize();

  /////////////////////////////////////////////////////////////////////////////

  const char *userName = std::getenv("USERNAME");
  if (userName != NULL)
    std::cout << userName << std::endl;

  //  QString fileName;
  //  if (std::string(userName) == "u17g43") {
  //    QString fileName = "/home/u17g43/test/cgnsmeshreader/Meshes/bunny.obj";
  //    fileName = "/home/u17g43/test/cgnsmeshreader/Meshes/suzanne.obj";
  //    //    fileName = "/home/u17g43/test/cgnsmeshreader/Meshes/cube.obj";
  //  } else if (std::string(userName) == "duvnjakk") {

  //    fileName =
  //        "/mnt/ubuntu_home/duvnjakk/workspace/mightymarten/Meshes/bunny.obj";
  //    //    fileName =
  //    //
  //    "/mnt/ubuntu_home/duvnjakk/workspace/cgnsMeshReader/Meshes/suzanne.obj";

  //    //    fileName = "E:\MM\Meshes\\";
  //    //    fileName =
  //    "/home/duvnjakk/workspace/mightymarten/Meshes/suzanne.obj"; fileName =
  //    "/home/duvnjakk/workspace/mightymarten/Meshes/suzanne.obj";

  //  } else
  //    std::cout << "Unknown user" << std::endl;

  //  fileName = "E:/MM/Meshes/suzanne.obj";
  /////////////////////////////////////////////////////////////////////////////////////////////
  //  OpenGL data
  //  ObjParser::parse(fileName, resMesh);
  //  createMVOs();

  bb::TopoGen::generateHex(bb::Vec3(0.5, 0.2, 0.8), 0.5);
  bb::TopoGen::generateHex(bb::Vec3(-0.5, -0.2, 0.8), 0.5);
  bb::Mesh meshko = bb::TopoGen::getMeshObj();
  bb::TopoGen::print();
  MeshViewObject *tmpMVO = new MeshViewObject(meshko);

  MVOs_.push_back(tmpMVO);
  //  initGLDataStrsucture();

  ////////////////////////////////////////////////////////////////////////////////////////////

  QOpenGLFramebufferObjectFormat fbof;
  fbof.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
  fbof.setInternalTextureFormat(GL_RGB32F);
  viewerFBO = new QOpenGLFramebufferObject(-this->size().width(),
                                           this->size().height(), fbof);

  // viewer modes
  ////////////////////////////////////////////////////////////////////////////////////////////
  viewerModeMap_.insert(std::make_pair(
      QString("DEFAULT"),
      new DefaultViewerMode(m_projection, m_camera, m_model, width_, height_,
                            selected_, colors_)));
  viewerModeMap_.insert(std::make_pair(
      QString("PICK"),
      new PickViewerMode(m_projection, m_camera, m_model, this->width_,
                         this->height_, selected_, colors_)));
  viewerModeMap_.insert(std::make_pair(
      QString("POLY"),
      new PolygonViewerMode(m_projection, m_camera, m_model, this->width_,
                            this->height_, selected_, colors_)));

  tmpViewerMode = viewerModeMap_[QString("DEFAULT")];
  tmpViewerMode->numOfIndices_ = 10000;
  tmpViewerMode->initViewerMode();
  std::cout << "SizeOF " << MVOs_.size() << std::endl;
  tmpViewerMode->vaoID = MVOs_[0]->getVAO();

  //  tmpViewerMode = viewerModeMap_[QString("PICK")];
  //  //  tmpViewerMode->numOfIndices_ = resMesh.getNumIndices();
  //  tmpViewerMode->setFramebufferObject(&viewerFBO);
  //  tmpViewerMode->initViewerMode();
  //  tmpViewerMode->vaoID = vao_;
  //  tmpViewerMode->colID = colBuff;

  //  tmpViewerMode = viewerModeMap_[QString("POLY")];
  //  //  tmpViewerMode->numOfIndices_ = resMesh.getNumIndices();
  //  tmpViewerMode->initViewerMode();
  //  tmpViewerMode->vaoID = vao_;
  //  tmpViewerMode->colID = colBuff;

  tmpViewerMode = viewerModeMap_[QString("DEFAULT")];

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

void Viewer::resizeGL(int width, int height) {

  m_projection.setToIdentity();
  m_projection.perspective(20.0f, width / float(height), 0.1f, 1000.0f);

  makeCurrent();
  QOpenGLFramebufferObjectFormat tmpFBOF = viewerFBO->format();
  delete viewerFBO;
  viewerFBO = new QOpenGLFramebufferObject(width, height, tmpFBOF);
  //  tmpViewerMode->resizeGL(width, height);

  this->width_ = this->width();
  this->height_ = this->height();

  return;
}

void Viewer::paintGL() {

  makeCurrent();
  tmpViewerMode->paintGL();

  return;
}

void Viewer::clearSelection() {
  tmpViewerMode->tmpMap.clear();
  tmpViewerMode->updateSelection();
}

void Viewer::activateViewerMode(QString mode) {
  std::cout << "clicked on mode : " << mode.toStdString() << std::endl;

  this->makeCurrent();
  if (mode == QString("POLY")) {
    this->setMouseTracking(true);
    this->setFocus();
  } else {
    this->setMouseTracking(false);
  }
  tmpViewerMode = viewerModeMap_[mode];

  return;
}

void Viewer::teardownGL() {
  // Actually destroy our OpenGL information
}

void Viewer::update() {
  // Update input
  Input::update();

  static const float transSpeed = 0.5f;
  static const float rotSpeed = 0.5f;

  // Camera Transformation
  if (Input::buttonPressed(Qt::LeftButton)) {
    m_model.rotate(-rotSpeed * Input::mouseDelta().x(), Camera3D::LocalUp);
    m_model.rotate(-rotSpeed * Input::mouseDelta().y(), m_model.right());
  }

  if (Input::buttonPressed(Qt::RightButton)) {
    m_model.translate(transSpeed * 0.05 * Input::mouseDelta().x() *
                      Camera3D::LocalRight);
    m_model.translate(transSpeed * -0.05 * Input::mouseDelta().y() *
                      Camera3D::LocalUp);
  }

  if (Input::buttonTriggered(Qt::LeftButton)) {
    //    pickedPoints_.push_back(
    //        QVector2D(Input::mousePosition().x(),
    //        Input::mousePosition().y()));
  }

  // Handle rotations
  //    m_camera.rotate(-rotSpeed * Input::mouseDelta().x(), Camera3D::LocalUp);
  //    m_camera.rotate(-rotSpeed * Input::mouseDelta().y(), m_camera.right());

  // Handle translations
  QVector3D translation;
  // Update instance information
  if (Input::getWheelQueue().getSize() > 0) {

    //    m_model.rotate(2.0f * (Input::getWheelQueue().front() /
    //                           std::abs(Input::getWheelQueue().front())),
    //                   QVector3D(0.4f, 0.3f, 0.3f));
    translation += m_camera.forward() *
                   (Input::getWheelQueue().front() /
                    std::abs(Input::getWheelQueue().front())) *
                   0.2;

    Input::getWheelQueue().dequeue();
  }

  if (Input::keyPressed(Qt::Key_W)) {
    translation += m_camera.forward();
  }
  if (Input::keyPressed(Qt::Key_S)) {
    translation -= m_camera.forward();
  }
  if (Input::keyPressed(Qt::Key_A)) {
    translation -= m_camera.right();
  }
  if (Input::keyPressed(Qt::Key_D)) {
    translation += m_camera.right();
  }
  if (Input::keyPressed(Qt::Key_Q)) {
    translation -= m_camera.up();
  }
  if (Input::keyPressed(Qt::Key_E)) {
    translation += m_camera.up();
  }
  m_camera.translate(transSpeed * translation);
  //  }
  // Schedule a redraw
  QOpenGLWidget::update();
}

void Viewer::keyPressEvent(QKeyEvent *event) {
  if (event->isAutoRepeat()) {
    event->ignore();
  } else {
    Input::registerKeyPress(event->key());
  }
  tmpViewerMode->keyPressEvent(event);
}

void Viewer::keyReleaseEvent(QKeyEvent *event) {
  if (event->isAutoRepeat()) {
    event->ignore();
  } else {
    Input::registerKeyRelease(event->key());
  }
}

void Viewer::mousePressEvent(QMouseEvent *event) {
  Input::registerMousePress(event->button());
  this->buttonDown = true;
}

void Viewer::mouseReleaseEvent(QMouseEvent *event) {
  Input::registerMouseRelease(event->button());
  this->buttonDown = false;

  std::cout << event->pos().x() << std::endl;
  //  this->capture(event->pos().x(), this->size().height() - event->pos().y());

  makeCurrent();
  tmpViewerMode->mouseReleaseEvent(event);
}

void Viewer::wheelEvent(QWheelEvent *wheel) {
  Input::registerMouseWheel(wheel->delta());
}

void Viewer::createMVOs() {
  bb::nVec<bb::Mesh> &meshes = FileManager::getImportedMeshes();
  for (uint i = 0; i < meshes.size(); i++) {

    MeshViewObject *tmpMVO = new MeshViewObject(meshes[i]);

    MVOs_.push_back(tmpMVO);
  }
}

void Viewer::mouseMoveEvent(QMouseEvent *event) {
  tmpViewerMode->mouseMoveEvent(event);
}

/*******************************************************************************
 * Private Helpers
 ******************************************************************************/

void Viewer::printVersionInformation() {
  QString glType;
  QString glVersion;
  QString glProfile;

  // Get Version Information
  glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
  glVersion = reinterpret_cast<const char *>(glGetString(GL_VERSION));

// Get Profile Information
#define CASE(c)                                                                \
  case QSurfaceFormat::c:                                                      \
    glProfile = #c;                                                            \
    break
  switch (format().profile()) {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
  }
#undef CASE

  // qPrintable() will print our QString w/o quotes around it.
  qDebug() << qPrintable(glType) << qPrintable(glVersion) << "("
           << qPrintable(glProfile) << ")";
}

void Viewer::initGLDataStructure() {

  //  glGenVertexArrays(1, &vao_);
  //  glBindVertexArray(vao_);

  //  glEnableVertexAttribArray(0);
  //  glEnableVertexAttribArray(1);
  //  glEnableVertexAttribArray(2);
  //  glGenBuffers(1, &vBuff);
  //  glBindBuffer(GL_ARRAY_BUFFER, vBuff);
  //  //  glBufferData(GL_ARRAY_BUFFER,
  //  //               sizeof(bb::Mesh::Vertex) * resMesh.getNumVertices(),
  //  //               resMesh.getVerticesData(), GL_STATIC_DRAW);
  //  glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, sizeof(bb::Mesh::Vertex),
  //  0); glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE,
  //  sizeof(bb::Mesh::Vertex),
  //                        (void *)(sizeof(bb::Vec3)));

  //  colors_.resize(resMesh.getNumVertices());
  //  for (uint i = 0; i < colors_.size(); i++) {
  //    colors_[i] = bb::Vec4(1.0f, 0.1f, 0.7f, 1.0f);
  //  }
  //  glGenBuffers(1, &colBuff);
  //  glBindBuffer(GL_ARRAY_BUFFER, colBuff);
  //  glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec4) * resMesh.getNumVertices(),
  //               colors_.first(), GL_DYNAMIC_DRAW);
  //  glVertexAttribPointer(2, 4, GL_DOUBLE, GL_FALSE, 0, 0);

  //  glGenBuffers(1, &iBuff);
  //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuff);
  //  //  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) *
  //  //  resMesh.getNumIndices(),
  //  //               resMesh.getIndices(), GL_STATIC_DRAW);

  //  glBindBuffer(GL_ARRAY_BUFFER, vBuff);
  //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuff);

  //  glBindVertexArray(0);
  //  glBindBuffer(GL_ARRAY_BUFFER, 0);
  //  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

Viewer::~Viewer() {}

void Viewer::passMeshes() {
  std::cout << FileManager::getImportedMeshes().size() << std::endl;

  createMVOs();
}

MeshViewObject::MeshViewObject(bb::Mesh &mesh) {
  // init GL funcs
  std::cout << "MeshViewObject::initializeOpenGLFunctions: "
            << initializeOpenGLFunctions() << std::endl;

  // initGL structure
  glGenVertexArrays(1, &vao);
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glGenBuffers(1, &vBuff);
  glGenBuffers(1, &colBuff);
  glGenBuffers(1, &iBuff);

  // save meshptr
  this->meshPtr_ = &mesh;

  // init GL data
  this->initGLData();

  LOG(std::cout << this << " -> MeshViewObject ctor " << std::endl;)
}

MeshViewObject::~MeshViewObject() {
  glDeleteVertexArrays(1, &vao);

  glDisableVertexAttribArray(0);
  glDisableVertexAttribArray(1);
  glDisableVertexAttribArray(2);

  glDeleteBuffers(1, &vBuff);
  glDeleteBuffers(1, &colBuff);
  glDeleteBuffers(1, &iBuff);

  LOG(std::cout << this << " -> MeshViewObject dtor " << std::endl;)
}

void MeshViewObject::initGLData() {

  glBindVertexArray(vao);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);

  // init vertex data
  glBindBuffer(GL_ARRAY_BUFFER, vBuff);
  bb::Mesh::Vertex vertices[meshPtr_->getNumVertices()];
  for (uint i = 0; i < meshPtr_->getNumVertices(); i++) {
    vertices[i].coord_ = meshPtr_->getVertices()[i];
    //    vertices[i].normal_ = meshPtr_->getNormals()[i];
  }

  std::cout << meshPtr_ << std::endl;
  glBufferData(GL_ARRAY_BUFFER,
               sizeof(bb::Mesh::Vertex) * meshPtr_->getNumVertices(), vertices,
               GL_STATIC_DRAW);
  glVertexAttribPointer(0, 3, GL_DOUBLE, GL_FALSE, sizeof(bb::Mesh::Vertex), 0);
  glVertexAttribPointer(1, 3, GL_DOUBLE, GL_FALSE, sizeof(bb::Mesh::Vertex),
                        (void *)(sizeof(bb::Vec3)));

  // init colors data
  bb::Vec4 colors[meshPtr_->getNumVertices()];
  for (uint i = 0; i < meshPtr_->getNumVertices(); i++) {
    colors[i] = bb::Vec4(1.0f, 0.1f, 0.7f, 1.0f);
  }
  glBindBuffer(GL_ARRAY_BUFFER, colBuff);
  glBufferData(GL_ARRAY_BUFFER, sizeof(bb::Vec4) * meshPtr_->getNumVertices(),
               colors, GL_DYNAMIC_DRAW);
  glVertexAttribPointer(2, 4, GL_DOUBLE, GL_FALSE, 0, 0);

  // init index element array data

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuff);
  bb::nVec<uint> indices;
  for (uint i = 0; i < meshPtr_->getCells().size(); i++) {
    const bb::PolyCell &tmpCell = meshPtr_->getCell(i);

    const bb::nVec<uint> &cellFaces = tmpCell.getCellPolyFaceIDs();
    const bb::nVec<bb::PolyFace> &meshPolyFaces = meshPtr_->getFaces();
    std::cout << " moram ic " << meshPtr_->getFaces() << std::endl;

    for (uint j = 0; j < cellFaces.size(); j++) {
      const bb::nVec<uint> &nodes =
          meshPolyFaces[cellFaces[j]].polyFaceNodes_();

      for (uint k = 0; k < nodes.size(); k++) {
        indices.push_back(nodes[k]);
      }
      indices.push_back(RESTART_INDEX);
      std::cout << std::endl;
    }
  }

  std::cout << "finalfantasy " << indices << std::endl;
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * indices.size(),
               indices.first(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, vBuff);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuff);

  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
