#include "centralwidget.h"
#include "filemanager.h"
#include "viewer.h"
#include <QApplication>
#include <QFileDialog>
#include <QMainWindow>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>
#include <chrono>

#include <thread>
int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  // Set OpenGL Version information
  // Note: This format must be set before show() is called.
  QSurfaceFormat format;
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setProfile(QSurfaceFormat::CompatibilityProfile);
  format.setVersion(3, 3);

  //  // Set the window up
  QMainWindow guiwindow;
  guiwindow.setWindowTitle(QString("MeshViewer"));

  CentralWidget centralWidget(&guiwindow);
  PlotWidget ploter;
  //  ploter.setData(test);
  //  guiwindow.setCentralWidget(&centralWidget);
  guiwindow.setCentralWidget(&ploter);
  guiwindow.resize(QSize(800, 700));
  guiwindow.show();

  bb::Mesh meshko;

  meshko.appendVertex(bb::Vec3(0.2, 0.2, 0));
  meshko.appendVertex(bb::Vec3(0.2, 0.5, 0));
  meshko.appendVertex(bb::Vec3(0.0, 0.6, 0));
  meshko.appendVertex(bb::Vec3(-0.2, 0.5, 0));
  meshko.appendVertex(bb::Vec3(-0.7, 0.0, 0));
  meshko.appendVertex(bb::Vec3(-0.6, -0.3, 0));

  meshko.appendVertex(bb::Vec3(-0.4, -0.6, 0));
  meshko.appendVertex(bb::Vec3(0.0, -0.5, 0));
  meshko.appendVertex(bb::Vec3(0.2, -0.4, 0));
  meshko.appendVertex(bb::Vec3(0.4, 0.0, 0));

  bb::PolyFace face1{0, 1, 2, 3, 4, 5};
  bb::PolyFace face2{6, 7, 8, 9};

  meshko.appendFace(face1);
  meshko.appendFace(face2);

  bb::PolyCell cell;
  cell.appendPolyFace(0);
  cell.appendPolyFace(1);
  meshko.appendCell(cell);

  bb::nVec<bb::Vec2> dejta({bb::Vec2(0.0, -0.5), bb::Vec2(1.0, 1.00)});
  ploter.plotData(dejta);
  //  ploter.plotData(meshko);

  bb::Mesh djole;
  bb::TopoGen::generateQuadRect(bb::Vec2(0.0, 0.0), bb::Vec2(20.0, 20.00), 4,
                                3);
  djole = bb::TopoGen::getMeshObj();

  ploter.plotData(djole);
  return app.exec();
}
