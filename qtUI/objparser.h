#ifndef OBJPARSER_H
#define OBJPARSER_H

#include "BB_Mesh.h"
#include <QString>

#include <map>

class ObjParser {
public:
  ObjParser() = delete;
  static void parse(QString const &filename, bb::Mesh &resultMesh);

  enum ENTITY { VERTEX, VERTEX_NORMAL, FACE, TEXTURE };

private:
  static void clearParserData();
  static void extractDataFromFile(const QString &filename);
  static void generateMeshData(bb::Mesh &resultMesh);

  static bb::Vec3 calculateVertexNormal(bb::PolyFace &tmpFace, uint nodeID);

  static bb::nVec<bb::Vec3> vertices_;
  static bb::nVec<bb::Vec3> vertNormals_;
  static bb::nVec<bb::PolyFace> faces_;

  static std::map<std::string, ObjParser::ENTITY> entityMap_;
};

#endif // OBJPARSER_H
