#ifndef VIEWER_H
#define VIEWER_H

#include "BB_Mesh.h"
#include "camera3d.h"
#include "transform3d.h"
#include "viewermode.h"
#include <QMatrix4x4>
#include <QOpenGLBuffer>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFunctions>
#include <QOpenGLFunctions_4_3_Compatibility>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLWidget>
#include <QVector2D>
#include <map>
#include <set>

class MeshViewObject : public QOpenGLFunctions_4_3_Core {
public:
  MeshViewObject(bb::Mesh &mesh);

  uint getVAO() { return this->vao; }

  void setFocus(bool focus) { this->focused = focus; }
  bool getFocus() { return this->focused; }

  ~MeshViewObject();

private:
  void initGLData();

  // GL handles
  GLuint vao;
  GLuint vBuff;
  GLuint iBuff;
  GLuint colBuff;

  // data structure
  bb::Mesh *meshPtr_;

  // UI
  bool focused = false;
};

class Viewer : public QOpenGLWidget, protected QOpenGLFunctions_4_3_Core {
  Q_OBJECT

  // OpenGL Events
public:
  Viewer();
  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();

  uint vao_;
  uint vBuff;
  uint iBuff;
  uint colBuff;

  uint width_;
  uint height_;

  QOpenGLFramebufferObject *viewerFBO;
  int u_modelToWorld;
  int u_worldToCamera;
  int u_cameraToView;
  int u_modelRotationMatrix;
  int u_color;

  ~Viewer();

public slots:
  void passMeshes();
  void activateViewerMode(QString mode);
  void clearSelection();

protected slots:
  void teardownGL();
  void update();

protected:
  void keyPressEvent(QKeyEvent *event);
  void keyReleaseEvent(QKeyEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *wheel);

private:
  void createMVOs();

private:
  std::map<QString, ViewerMode *> viewerModeMap_;
  ViewerMode *tmpViewerMode;

  bb::nVec<MeshViewObject *> MVOs_;

  bb::nVec<uint> selected_;
  bool buttonDown;

  // OpenGL State Information
  QMatrix4x4 m_projection;
  Camera3D m_camera;
  Transform3D m_model;

  bb::nVec<bb::Vec4> colors_;
  bb::Mesh resMesh;

  // Private Helpers
  void printVersionInformation();
  void initGLDataStructure();
};

#endif // VIEWER_H
