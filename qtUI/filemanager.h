#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "BB_Mesh.h"
#include <BB_STL.h>
#include <QFile>
#include <QObject>
#include <QWidget>

class FileManager : public QObject {
  Q_OBJECT
public:
  explicit FileManager(QObject *parent = nullptr);

  static bb::nVec<bb::Mesh> &getImportedMeshes();

private:
  QStringList getOpenFileNames();

private:
  static bb::nVec<bb::Mesh> meshes_;

  // signals and slots
signals:
  void importedMeshes();

public slots:
  void openFiles();
};

#endif // FILEMANAGER_H
