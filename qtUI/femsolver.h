#ifndef FEMSOLVER_H
#define FEMSOLVER_H

#include <BB_Mesh.h>
#include <QObject>
#include <memory>

class FEMsolver : public QObject {
  Q_OBJECT
public:
  explicit FEMsolver(QObject *parent = nullptr);
  void setMesh(bb::Mesh &mesh);

  void setNodeSelection(bb::NodeSelection &selection);

  void runSolver();

private:
  std::unique_ptr<bb::Mesh> meshPtr_;
  bb::nVec<bb::NodeSelection> selections_;

signals:

public slots:
};

#endif // FEMSOLVER_H
