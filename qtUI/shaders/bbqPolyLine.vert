#version 330
layout(location = 0) in vec2 inPos;

uniform vec4 projMatrix;

void main(void)
{
    gl_Position = vec4(inPos, -0.5, 1.0);
}
