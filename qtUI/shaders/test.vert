#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;

out vec4 vColor;

uniform mat4 modelToWorld;
uniform mat4 worldToCamera;
uniform mat4 cameraToView;
uniform mat3 modelRotation;


void main(void)
{
    mat4 transform = cameraToView * worldToCamera * modelToWorld ;
    gl_Position = transform * vec4(position, 1.0);

    vColor = vec4(1.0f, 0.0, 0.0, 1.0f);
}
