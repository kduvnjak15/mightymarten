#version 330
layout(location = 0) in vec3 position;
//layout(location = 1) in vec3 color;
layout(location = 1) in vec3 normal;

out vec4 vColor;
out vec3 vNormal;

out vec4 vNormalRotated;


out vec3 tmpNormal;
out vec4 dirVec;


uniform mat4 modelToWorld;
uniform mat4 worldToCamera;
uniform mat4 cameraToView;
uniform mat3 modelRotation;


void main(void)
{
    mat4 transform = cameraToView * worldToCamera * modelToWorld ;
    gl_Position = transform * vec4(position, 1.0);
}
