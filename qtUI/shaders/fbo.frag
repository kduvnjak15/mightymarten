#version 330
in vec4 vColor;
in vec3 vNormal;
in vec3 vNormalRotated;

out vec4 fColor;

uniform vec4 u_color;
uniform int numOfPrimitives;

void main(){

    fColor = vec4(gl_PrimitiveID/(numOfPrimitives*1.0f) , 0.0f, 0.0f , 1.0f);

}
