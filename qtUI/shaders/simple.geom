#version 330

layout(triangles) in;

layout(triangle_strip, max_vertices = 240) out;


in vec4 vColor[];
in vec3 vNormal[];

in vec4 vNormalRotated[];

in vec3 tmpNormal[];
in vec4 dirVec[];

in vec2 debug[];

in float factor[];

out vec4 vgColor;
out vec3 vgNormal;
out vec3 vgNormalRotated;

uniform int numOfPickedPoints;
uniform vec4 dots[6];

uniform int u_viewerWidth;
uniform int u_viewerHeight;

out float outID;

out vec2 dibag;
out vec2 dibagWH;
out vec2 dibagcenter;
vec2 primitiveCenter(){

    int width = u_viewerWidth;
    int height = u_viewerHeight;

    dibagWH = vec2(width, height);

    vec3 ndc0 = gl_in[0].gl_Position.xyz / gl_in[0].gl_Position.w;
    vec2 viewportCoord0 = ndc0.xy * 0.5 + 0.5;
    viewportCoord0.x *= width;
    viewportCoord0.y = 1 - viewportCoord0.y;
    viewportCoord0.y *= height;

    vec3 ndc1 = gl_in[1].gl_Position.xyz / gl_in[1].gl_Position.w;
    vec2 viewportCoord1 = ndc1.xy * 0.5 + 0.5;
    viewportCoord1.x *= width;
    viewportCoord1.y = 1 - viewportCoord1.y;
    viewportCoord1.y *= height;


    vec3 ndc2 = gl_in[2].gl_Position.xyz / gl_in[2].gl_Position.w;
    vec2 viewportCoord2 = ndc2.xy * 0.5 + 0.5;
    viewportCoord2.x *= width;
    viewportCoord2.y = 1 - viewportCoord2.y;
    viewportCoord2.y *= height;


    vec2 center = (viewportCoord0 + viewportCoord1 + viewportCoord2)/3.0f;

    return center;
}

bool primitiveSelected(){

    vec2 center = primitiveCenter();
    dibagcenter = center;
    float angle = 0;
    for (int i = 0; i < numOfPickedPoints; i++){
        vec2 p1 = dots[i].xy;
        vec2 p2 = dots[i].zw;

        vec2 dir1 =  p1-center;
        vec2 dir2 =  p2-center;
        dir1 = normalize(dir1);
        dir2 = normalize(dir2);

        angle += acos(dot(dir1, dir2));
    }

    return abs(angle - 2 *  3.14159265359) < 1e-1;

}

float primitiveSelectedfloat(){

    vec2 center = primitiveCenter();

    float angle = 0;
    for (int i = 0; i < numOfPickedPoints; i++){
        vec2 p1 = dots[i].xy;
        vec2 p2 = dots[i].zw;

        vec2 dir1 =  p1-center;
        vec2 dir2 =  p2-center;
        dir1 = normalize(dir1);
        dir2 = normalize(dir2);

        angle += acos(dot(dir1, dir2));
    }


    return angle;

}

void main(){


    gl_PrimitiveID = gl_PrimitiveIDIn;

    bool pass = primitiveSelected();

        gl_Position = gl_in[0].gl_Position;
        vgColor = vColor[0];
        gl_PrimitiveID = gl_PrimitiveIDIn;
        if (pass)
            outID = gl_PrimitiveID;
        else
            outID = -13;
        dibag = primitiveCenter();

    EmitVertex();

    pass = primitiveSelected();

        gl_Position = gl_in[1].gl_Position;
        vgColor = vColor[1];
        gl_PrimitiveID = gl_PrimitiveIDIn;
        if (pass)
            outID = gl_PrimitiveID;
        else
            outID = -13;
        dibag = primitiveCenter();
    EmitVertex();

    pass = primitiveSelected();

        gl_Position = gl_in[2].gl_Position;
        vgColor = vColor[2];
        gl_PrimitiveID = gl_PrimitiveIDIn;
        if (pass)
            outID = gl_PrimitiveID;
        else
            outID = -13;
        dibag = primitiveCenter();

    EmitVertex();


    EndPrimitive();
}
