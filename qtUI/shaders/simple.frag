#version 330
in vec4 vgColor;
in vec3 vNormal;
in vec3 vNormalRotated;

in float outID;

float M_PI =  3.1415926535897932384626433832795;
out vec4 fColor;

uniform vec4 u_color;


void main(){

    if (u_color.w < 0.001)
        fColor = vgColor;
    else
        fColor = u_color;

//    fColor = vgColor;
}
