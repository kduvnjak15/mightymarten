#version 330
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec4 colorPV;

out vec4 vColor;
out vec3 vNormal;

out vec4 vNormalRotated;

out vec3 tmpNormal;
out vec4 dirVec;

uniform mat4 modelToWorld;
uniform mat4 worldToCamera;
uniform mat4 cameraToView;
uniform mat3 modelRotation;

void main(void)
{
    vec4 sun_position = vec4(2, 2, 20, 1.0);
    float ambient = 0.2f;

    mat4 transform = cameraToView * worldToCamera * modelToWorld ;
    vNormal = modelRotation * normal;

    gl_Position = transform * vec4(position, 1.0);
    dirVec = sun_position - gl_Position;

    tmpNormal = vNormal;
    tmpNormal = normalize(tmpNormal);
    dirVec = normalize(dirVec);

    float factor = dot(tmpNormal, dirVec.xyz);

//     vColor =  (max(factor, 0) + ambient)*vec4(1, 0, 0, 1.0f) ;
    vColor =  (max(factor, ambient))*colorPV;
//    vColor =  (max(factor, ambient))*vec4(1.0,0.5, 1.0f, 1.0f) ;

}
