#version 330
layout(location = 0) in vec2 position;
//layout(location = 1) in vec4 color;

uniform mat4 projMatrix;


out mat4 debug;

void main(void)
{


    debug = projMatrix;
    gl_Position= projMatrix * vec4(position, 0.0, 1.00);
}
