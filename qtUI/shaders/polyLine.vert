#version 330
layout(location = 0) in vec2 position;
//layout(location = 1) in vec4 color;

uniform mat4 modelToWorld;
uniform mat4 worldToCamera;
uniform mat4 cameraToView;
uniform mat3 modelRotation;

uniform int pl_viewerWidth;
uniform int pl_viewerHeight;

void main(void)
{


    float kay = 2 * 3.14159624;

//    gl_Position = vec4(cos(kay /6 * idx), sin(kay/6 * idx), 0, 1);

//    mat4 transform = cameraToView * worldToCamera * modelToWorld ;


    int width = pl_viewerWidth;
    int height = pl_viewerHeight;
    vec2 inPos = vec2(position.x*2.0-width, height - position.y*2.0);
    inPos = vec2(inPos.x*1.0/width, inPos.y*1.0/height);
//    mat4 transform = cameraToView * worldToCamera * modelToWorld ;
//    gl_Position = transform * vec4(inPos,0.0, 1.0);

    gl_Position = vec4(inPos, 0.0, 1.0);
}
