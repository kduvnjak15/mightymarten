#include "objparser.h"
#include <BB_LinAlgebra.h>
#include <QDebug>
#include <QFile>
#include <assert.h>
#include <set>
#include <sstream>
#include <string>

std::map<std::string, ObjParser::ENTITY> ObjParser::entityMap_ = {
    {"v", ObjParser::VERTEX},
    {"vn", ObjParser::VERTEX_NORMAL},
    {"f", ObjParser::FACE},
    {"t", ObjParser::TEXTURE}};

bb::nVec<bb::Vec3> ObjParser::vertices_;
bb::nVec<bb::Vec3> ObjParser::vertNormals_;
bb::nVec<bb::PolyFace> ObjParser::faces_;

// ObjParser::ObjParser() {}

void ObjParser::parse(const QString &filename, bb::Mesh &resultMesh) {
  qDebug() << "this filename will be parsed " << filename;

  clearParserData();

  extractDataFromFile(filename);

  generateMeshData(resultMesh);

  std::cout << &resultMesh << std::endl;
  std::cout << resultMesh.getFaces() << std::endl;
  return;
}

// void ObjParser::initEntityMap() {
//  entityMap_.insert(std::pair<std::string, ENTITY>("v", VERTEX));
//  entityMap_.insert(std::pair<std::string, ENTITY>("vn", VERTEX_NORMAL));
//  entityMap_.insert(std::pair<std::string, ENTITY>("f", FACE));
//  entityMap_.insert(std::pair<std::string, ENTITY>("t", TEXTURE));
//}

void ObjParser::clearParserData() {
  vertices_.clear();
  vertNormals_.clear();
  faces_.clear();

  assert(vertices_.size() == 0);
  assert(vertNormals_.size() == 0);
  assert(faces_.size() == 0);

  return;
}

void ObjParser::extractDataFromFile(const QString &filename) {
  QFile tmpFile(filename);
  bool isOpen = tmpFile.open(QFile::OpenModeFlag::ReadOnly);

  while (!tmpFile.atEnd()) {
    std::string tmpLine = tmpFile.readLine().toStdString();
    std::string token;
    if (tmpLine.find(" ") != std::string::npos) {

      std::stringstream ss;
      ss << tmpLine;
      ss >> token;

      if (entityMap_.find(token) != entityMap_.end()) {
        ENTITY tmpEntity = entityMap_[token];
        switch (tmpEntity) {
        case VERTEX: {
          double x, y, z;
          ss >> x >> y >> z;
          qDebug() << "v> " << x << ", " << y << ", " << z;

          bb::Vec3 tmpVec(x, y, z);
          vertices_.push_back(bb::Vec3(x, y, z));
          break;
        }
        case VERTEX_NORMAL: {
          double x, y, z;
          ss >> x >> y >> z;
          qDebug() << "vn> " << x << ", " << y << ", " << z;

          bb::Vec3 tmpNormal(x, y, z);
          vertNormals_.push_back(bb::Vec3(x, y, z));
          break;
        }
        case TEXTURE:

          break;
        case FACE: {
          //          std::string stringData[3];
          //          ss >> stringData[0] >> stringData[1] >> stringData[2];
          std::string stringData;
          std::vector<std::string> tmpData;
          while (ss >> stringData) {
            tmpData.push_back(stringData);
            std::cout << "da ima " << stringData << ", ";
          }

          size_t dim = tmpData.size();
          std::string faceNodes[dim];
          std::string faceTex[dim];
          std::string faceNormals[dim];

          for (uint i = 0; i < dim; i++) {
            std::istringstream is1(tmpData[i]);
            std::getline(is1, faceNodes[i], '/');
            std::getline(is1, faceTex[i], '/');
            std::getline(is1, faceNormals[i], '/');
          }

          bb::PolyFace tmpFace;
          for (uint i = 0; i < dim; i++) {
            uint nodeID = std::strtoul(faceNodes[i].c_str(), 0, 10) - 1;
            int nodeNormalID = std::strtol(faceNormals[i].c_str(), 0, 10) - 1;
            std::cout << nodeID << ", " << nodeNormalID << std::endl;
            tmpFace.appendNode(nodeID, nodeNormalID);
          }

          std::cout << "tmpFace " << tmpFace << std::endl;
          faces_.push_back(tmpFace);

        } break;
        default:
          continue;
        }

        std::cout << std::endl;
      } else
        qDebug() << "nema to " << token.c_str();
    }

    std::cout << faces_ << std::endl;
  }

  return;
}

void ObjParser::generateMeshData(bb::Mesh &resultMesh) {

  resultMesh.setVertices(ObjParser::vertices_);
  resultMesh.setNormals(ObjParser::vertNormals_);
  std::cout << "objparser`1 " << ObjParser::faces_ << std::endl;
  std::cout << "objparser 2" << resultMesh.getFaces() << std::endl;
  resultMesh.setFaces(ObjParser::faces_);
  std::cout << "objparser34 " << ObjParser::faces_ << std::endl;
  std::cout << "objparser34" << resultMesh.getFaces() << std::endl;

  for (uint i = 0; i < faces_.size(); i++) {

    bb::PolyCell tmpCell;
    tmpCell.appendPolyFace(i);
    resultMesh.appendCell(tmpCell);
  }

  std::cout << resultMesh.getNumVertices() << std::endl;
  std::cout << resultMesh.getNumFaces() << std::endl;
  std::cout << resultMesh.getNumCells() << std::endl;

  std::cout << resultMesh.getVertices() << std::endl;
  std::cout << resultMesh.getFaces() << std::endl;

  return;
}

bb::Vec3 ObjParser::calculateVertexNormal(bb::PolyFace &tmpFace, uint nodeID) {
  bb::Vec3 v = vertices_[tmpFace.polyFaceNodes_()[nodeID]];
  bb::Vec3 v_before =
      vertices_[tmpFace.polyFaceNodes_()[(nodeID - 1) % tmpFace.dim()]];
  bb::Vec3 v_after =
      vertices_[tmpFace.polyFaceNodes_()[(nodeID + 1) % tmpFace.dim()]];

  bb::Vec3 v1 = v_before - v;
  bb::Vec3 v2 = v_after - v;

  bb::Vec3 vNormal = v1 ^ v2;
  vNormal.normalize();

  return vNormal;
}
