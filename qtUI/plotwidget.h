#ifndef PLOTWIDGET_H
#define PLOTWIDGET_H

#include <QBoxLayout>
#include <QDockWidget>
#include <QMainWindow>
#include <QMatrix4x4>
#include <QObject>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>
#include <QPainter>
#include <QWidget>
#include <camera3d.h>
#include <transform3d.h>

#include <BB_Base.h>
#include <BB_LinAlgebra.h>
#include <BB_Mesh.h>

class bbqGeomView : public QObject,
                    protected QOpenGLFunctions_4_3_Core,
                    public IObserver {

  Q_OBJECT
public:
  bbqGeomView();

  GLuint getVAO();

  virtual void render() = 0;

  const bb::Vec2 &getMinPt();
  const bb::Vec2 &getMaxPt();

  virtual ~bbqGeomView();

protected:
  bool glInitialized = false;
  GLuint VAO_;
  GLuint VBO_;
  GLuint shaderID_;
  QOpenGLShaderProgram shaderProgram_;
  GLuint u_projMatrix;
  virtual void createShaderProgram() = 0;
  virtual void initializeGLdata() = 0;
  virtual void resetGLdata() = 0;
  virtual void calcMinMax() = 0;

  bb::Vec2 max_{1, 1};
  bb::Vec2 min_{-1, -1};
};

class bbqPolyLine : public bbqGeomView {
  Q_OBJECT

public:
  bbqPolyLine();
  bbqPolyLine(bb::nVec<double> &data);
  bbqPolyLine(bb::nVec<bb::Vec2> &pts);
  virtual ~bbqPolyLine();

  void setData(bb::nVec<bb::Vec2> &pts);
  void setData(bb::nVec<double> &pts);

  void initializeGLdata();

protected:
  bbqPolyLine *linePtr_ = nullptr;
};

class bbqPolyLinePoints : public bbqPolyLine {
public:
  bbqPolyLinePoints(bb::nVec<bb::Vec2> &pts);

  virtual void render();
  virtual void update(std::string message);

  ~bbqPolyLinePoints();

private:
  void createShaderProgram();
  virtual void initializeGLdata();
  virtual void resetGLdata();
  virtual void calcMinMax();

  bb::nVec<bb::Vec2> *points_ = nullptr;
};

class bbqPolyLineData : public bbqPolyLine {

public:
  bbqPolyLineData(bb::nVec<double> &data);

  virtual void render();
  virtual void update(std::string message);

  virtual ~bbqPolyLineData();

private:
  virtual void createShaderProgram();
  virtual void initializeGLdata();
  virtual void resetGLdata();
  virtual void calcMinMax();

  bb::nVec<double> *data_ = nullptr;
};

class bbqMesh : public bbqGeomView {
  Q_OBJECT
public:
  bbqMesh(bb::Mesh &meshObj);

  ~bbqMesh();

  virtual void update(std::string message);

  virtual void render();
  virtual void createShaderProgram();
  virtual void initializeGLdata();
  virtual void resetGLdata();
  virtual void calcMinMax();

private:
  GLuint IBO_;

  bb::Mesh *mesh_;
};

class PlotWidget : public QOpenGLWidget, protected QOpenGLFunctions_4_3_Core {
  Q_OBJECT
public:
  PlotWidget(QOpenGLWidget *parent = nullptr);

  // OpenGL musthave
  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();

  virtual ~PlotWidget();

  bool checkGLInitialized();
  // data handling;
public slots:
  void plotData(bb::nVec<bb::Vec2> &points);
  void plotData(bb::nVec<double> &data);
  void plotData(bb::Mesh &meshObj);

private:
  int u_projMatrix;

  int u_modelToWorld;
  int u_worldToCamera;
  int u_cameraToView;
  int u_modelRotationMatrix;

  QMatrix4x4 projMatrix_;
  QOpenGLShaderProgram plotShader_;

  bool glInitialized = false;

  // plot func

protected slots:
  void teardownGL();
  void update();

protected:
  void keyPressEvent(QKeyEvent *event);
  void keyReleaseEvent(QKeyEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *wheel);
  bool buttonDown;

private:
  void appendAvatar(bbqGeomView *avatarPtr);
  void renderAxes();
  void renderText();
  void updateMinMax(const bb::Vec2 &tmpMin, const bb::Vec2 &tmpMax);
  void updateProjMatrix();

private:
  // OpenGL State Information
  QMatrix4x4 m_projection;
  Camera3D m_camera;
  Transform3D m_model;
  bb::Vec2 minPt_{1e300, 1e300};
  bb::Vec2 maxPt_{-1e300, -1e300};

  QPainter painter_{this};

  bb::nVec<bb::Vec2> axespts;
  bbqPolyLine *axes_;
  bb::nVec<bbqGeomView *> avatars;
};

#endif // PLOTWIDGET_H

class PlotDockWidget : public QDockWidget {
  Q_OBJECT
public:
  PlotDockWidget();

  void show() { this->widget_.repaint(); }

private:
  PlotWidget widget_;
};

class PlotWindow : public QMainWindow {
  Q_OBJECT

public:
  PlotWindow();

  void setData(bb::nVec<bb::Vec2> &pts);

  void show();

  ~PlotWindow();
  QBoxLayout *layout;

private:
  PlotWidget *plotWidget_;
  PlotWidget *plotWidget2_;
};
