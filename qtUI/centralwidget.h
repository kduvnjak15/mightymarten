#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QObject>
#include <QWidget>

#include <QBoxLayout>
#include <QPushButton>
#include <QSignalMapper>
#include <filemanager.h>
#include <plotwidget.h>
#include <viewer.h>

class CentralWidget : public QWidget {
  Q_OBJECT
public:
  explicit CentralWidget(QWidget *parent = nullptr);

  ~CentralWidget();

private:
  //  Viewer viewer_;

  void initializeCentralWidget();
  void connectObjects();

  QPushButton cancelBtn;
  QPushButton importBtn;

  QPushButton plotBtn;
  QPushButton solverBtn;

  QPushButton defaultSelectionBtn;
  QPushButton pickSelectionBtn;
  QPushButton polySelectionBtn;
  QPushButton clearBtn;

  // Set layout
  QHBoxLayout *hBoxLayoutButtons = new QHBoxLayout;
  QVBoxLayout *vBoxLayoutGeneralButtons = new QVBoxLayout;
  QVBoxLayout *vBoxLayoutSelectionButtons = new QVBoxLayout;
  QVBoxLayout *vBoxLayout = new QVBoxLayout;

  FileManager fileManager_;
  QSignalMapper *signalMapper_;
  //  Viewer viewer_;
  PlotWidget ploter_;

private:
  void initSignalMap();

public slots:
};

#endif // CENTRALWIDGET_H
