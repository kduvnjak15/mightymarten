 
syms ksi eta  a b 

syms x y
ksi = x/a
eta = y/b

ksi01=1/4.*(2-3*ksi+ksi^3)
ksi11=a/4.*(1-ksi-ksi^2+ksi^3)
ksi02=1/4.*(2+3*ksi-ksi^3)
ksi12=a/4.*(-1-ksi+ksi^2+ksi^3)

eta01=1/4.*(2-3*eta+eta^3)
eta11=b/4.*(1-eta-eta^2+eta^3)
eta02=1/4.*(2+3*eta-eta^3)
eta12=b/4.*(-1-eta+eta^2+eta^3)

N1=[ksi01.*eta01 ksi11.*eta01 ksi01.*eta11 ksi11.*eta11]
N2=[ksi02.*eta01 ksi12.*eta01 ksi02.*eta11 ksi12.*eta11]
N3=[ksi02.*eta02 ksi12.*eta02 ksi02.*eta12 ksi12.*eta12]
N4=[ksi01.*eta02 ksi11.*eta02 ksi01.*eta12 ksi11.*eta12]
% N1=[ksi01*eta01 ksi11*eta01 ksi01*eta11 ksi11*eta11];
% N2=[ksi02*eta01 ksi12*eta01 ksi02*eta11 ksi12*eta11];
% N3=[ksi02*eta02 ksi12*eta02 ksi02*eta12 ksi12*eta12];
% N4=[ksi01*eta02 ksi11*eta02 ksi01*eta12 ksi11*eta12];
N=[N1 N2 N3 N4]

B = - [diff(N1(1),'x','x')  dd_(N2,'x','x') dd_(N3,'x','x') dd_(N4,'x','x');
       dd_(N1,'y','y')  dd_(N2,'y','y') dd_(N3,'y','y') dd_(N4,'y','y');
      2*dd_(N1,'x','y')  2*dd_(N2,'x','y') 2*dd_(N3,'x','y') 2*dd_(N4,'x','y')]
   
Nxy = N

Bt = transpose(B)
%%

 

syms ksi eta

N(1)=1/4*(1-ksi)*(1-eta); 
N(2)=1/4*(1+ksi)*(1-eta); 
N(3)=1/4*(1+ksi)*(1+eta); 
N(4)=1/4*(1-ksi)*(1+eta); 
N1 = N(1)
N2 = N(2)
N3 = N(3)
N4 = N(4)

% B = - [dd_(N1,'x','x')  dd_(N2,'x','x') dd_(N3,'x','x') dd_(N4,'x','x');
%        dd_(N1,'y','y')  dd_(N2,'y','y') dd_(N3,'y','y') dd_(N4,'y','y');
%        dd_(N1,'x','y')  dd_(N2,'x','y') dd_(N3,'x','y') dd_(N4,'x','y')]
% 
syms xc1 xc2 xc3 xc4 yc1 yc2 yc3 yc4
xes = [xc1; xc2; xc3; xc4]
yes = [yc1; yc2; yc3; yc4]
coordmat = [xes zeros(4,1); zeros(4,1) yes] 

jjmat =[d_(N1,'ksi') d_(N2,'ksi') d_(N3,'ksi') d_(N4,'ksi') d_(N1,'ksi') d_(N2,'ksi') d_(N3,'ksi') d_(N4,'ksi');
        d_(N1,'eta') d_(N2,'eta') d_(N3,'eta') d_(N4,'eta') d_(N1,'eta') d_(N2,'eta') d_(N3,'eta') d_(N4,'eta')]

syms N1k N2k N3k N4k
syms N1e N2e N3e N4e

N1k = d_(N1,'ksi')
N2k = d_(N2,'ksi')
N3k = d_(N3,'ksi')
N4k = d_(N4,'ksi')
N1e = d_(N1,'eta')
N2e = d_(N2,'eta')
N3e = d_(N3,'eta')
N4e = d_(N4,'eta')

jjmat =[N1k N2k N3k N4k N1k N2k N3k N4k;
        N1e N2e N3e N4e N1e N2e N3e N4e]

jacobsmat = jjmat * coordmat

%%
 

% xc1 = -1
% xc2 = 2
% xc3 = 2
% xc4 = -2
% yc1 = -1
% yc2 = -2
% yc3 = 2
% yc4 = 2

xes = [xc1; xc2; xc3; xc4]
yes = [yc1; yc2; yc3; yc4]
coordmat = [xes zeros(4,1); zeros(4,1) yes] 
jacobsmat = jjmat * coordmat

det(jacobsmat)

%%

kern = Bt * B ;
fid = fopen('ny.txt','wt');

for i = 1:size(kern,1)
    for j = 1:size(kern,1)
        fprintf(fid, 'double K%d_%d (double a, double b, double x, double y) { return %s; } \n', i,j, ccode(kern(i,j)));
   
        functor = kern(i,j);
        
    end
end
fclose(fid);

fid3 = fopen('fs.txt', 'wt');

for i = 1 :size(Nxy(:))
    i,ccode(Nxy(i))
    fprintf(fid3, 'double N_%d  (double a, double b, double x, double y) { return %s; } \n', i,ccode(Nxy(i)));
end
fclose(fid3);