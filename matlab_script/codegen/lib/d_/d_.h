/*
 * d_.h
 *
 * Code generation for function 'd_'
 *
 */

#ifndef D__H
#define D__H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "d__types.h"

/* Function Declarations */
extern void d_(unsigned int arg, double var1, int ddxxret_size[2]);

#endif

/* End of code generation (d_.h) */
