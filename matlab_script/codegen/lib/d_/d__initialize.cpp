/*
 * d__initialize.cpp
 *
 * Code generation for function 'd__initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "d_.h"
#include "d__initialize.h"

/* Function Definitions */
void d__initialize()
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (d__initialize.cpp) */
