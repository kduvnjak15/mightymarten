/*
 * d__terminate.h
 *
 * Code generation for function 'd__terminate'
 *
 */

#ifndef D__TERMINATE_H
#define D__TERMINATE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "d__types.h"

/* Function Declarations */
extern void d__terminate();

#endif

/* End of code generation (d__terminate.h) */
