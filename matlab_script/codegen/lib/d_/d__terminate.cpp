/*
 * d__terminate.cpp
 *
 * Code generation for function 'd__terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "d_.h"
#include "d__terminate.h"

/* Function Definitions */
void d__terminate()
{
  /* (no terminate code required) */
}

/* End of code generation (d__terminate.cpp) */
