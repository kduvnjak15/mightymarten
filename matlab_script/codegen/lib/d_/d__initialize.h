/*
 * d__initialize.h
 *
 * Code generation for function 'd__initialize'
 *
 */

#ifndef D__INITIALIZE_H
#define D__INITIALIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "d__types.h"

/* Function Declarations */
extern void d__initialize();

#endif

/* End of code generation (d__initialize.h) */
