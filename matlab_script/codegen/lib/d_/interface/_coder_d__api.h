/*
 * _coder_d__api.h
 *
 * Code generation for function '_coder_d__api'
 *
 */

#ifndef _CODER_D__API_H
#define _CODER_D__API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_d__api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void d_(uint32_T arg, real_T var1, int32_T ddxxret_size[2]);
extern void d__api(const mxArray * const prhs[2], int32_T nlhs, const mxArray
                   *plhs[1]);
extern void d__atexit(void);
extern void d__initialize(void);
extern void d__terminate(void);
extern void d__xil_terminate(void);

#endif

/* End of code generation (_coder_d__api.h) */
