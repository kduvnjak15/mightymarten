/*
 * _coder_d__info.h
 *
 * Code generation for function 'd_'
 *
 */

#ifndef _CODER_D__INFO_H
#define _CODER_D__INFO_H
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo();
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties();

#endif
/* End of code generation (_coder_d__info.h) */
