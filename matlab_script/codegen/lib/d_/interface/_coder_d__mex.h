/*
 * _coder_d__mex.h
 *
 * Code generation for function '_coder_d__mex'
 *
 */

#ifndef _CODER_D__MEX_H
#define _CODER_D__MEX_H

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "_coder_d__api.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);
extern emlrtCTX mexFunctionCreateRootTLS(void);

#endif

/* End of code generation (_coder_d__mex.h) */
