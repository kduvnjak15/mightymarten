/*
 * main.cpp
 *
 * Code generation for function 'main'
 *
 */

/*************************************************************************/
/* This automatically generated example C main file shows how to call    */
/* entry-point functions that MATLAB Coder generated. You must customize */
/* this file for your application. Do not modify this file directly.     */
/* Instead, make a copy of this file, modify it, and integrate it into   */
/* your development environment.                                         */
/*                                                                       */
/* This file initializes entry-point function arguments to a default     */
/* size and value before calling the entry-point functions. It does      */
/* not store or use any values returned from the entry-point functions.  */
/* If necessary, it does pre-allocate memory for returned values.        */
/* You can use this file as a starting point for a main function that    */
/* you can deploy in your application.                                   */
/*                                                                       */
/* After you copy the file, and before you deploy it, you must make the  */
/* following changes:                                                    */
/* * For variable-size function arguments, change the example sizes to   */
/* the sizes that your application requires.                             */
/* * Change the example values of function arguments to the values that  */
/* your application requires.                                            */
/* * If the entry-point functions return values, store these values or   */
/* otherwise use them as required by your application.                   */
/*                                                                       */
/*************************************************************************/
/* Include files */
#include "rt_nonfinite.h"
#include "d_.h"
#include "main.h"
#include "d__terminate.h"
#include "d__initialize.h"

/* Function Declarations */
static double argInit_real_T();
static unsigned int argInit_uint32_T();
static void main_d_();

/* Function Definitions */
static double argInit_real_T()
{
  return 0.0;
}

static unsigned int argInit_uint32_T()
{
  return 0U;
}

static void main_d_()
{
  int unusedExpr[2];

  /* Initialize function 'd_' input arguments. */
  /* Call the entry-point 'd_'. */
  d_(argInit_uint32_T(), argInit_real_T(), unusedExpr);
}

int main(int, const char * const [])
{
  /* Initialize the application.
     You do not need to do this more than one time. */
  d__initialize();

  /* Invoke the entry-point functions.
     You can call entry-point functions multiple times. */
  main_d_();

  /* Terminate the application.
     You do not need to do this more than one time. */
  d__terminate();
  return 0;
}

/* End of code generation (main.cpp) */
