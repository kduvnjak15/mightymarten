#include <cgnslib.h>
#include <vector>

using std::vector;
struct cgnsBase
{
    int baseID;
    char baseName[CG_NAMES_LENGTH];
    int cellDim;
    int physDim;
};

struct cgnsZone
{
    int zoneID;
    char zoneName[CG_NAMES_LENGTH];
    int zoneSize[16];
    int index_dim;
    int zoneType;
};

struct cgnsSection
{
    int sectionID;
    char sectionName[CG_NAMES_LENGTH];
    ElementType_t elementType;
    int start;
    int end;
    int nbndry;
    int parentFlag;
    vector<int> elements;
    int parentData;

};





