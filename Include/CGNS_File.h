
#ifndef CGNS_FILE_H
#define CGNS_FILE_H

#define CG_NAMES_LENGTH 100
#include <vector>

#include <cgnslib.h>
#include "BB_Mesh.h"

class CGNS_File
{
public:
    CGNS_File(const char* filePath);

    void getCGNSInfo();

    void convert2BB_Mesh(BB_Mesh& dstMesh);

private:
    struct cgnsBase
    {
        int baseID;
        char baseName[CG_NAMES_LENGTH];
        int cellDim;
        int physDim;
    };

    struct cgnsZone
    {
        int zoneID;
        char zoneName[CG_NAMES_LENGTH];
        int zoneSize[16];
        int index_dim;
        int zoneType;
        vector<BB_Vert3> vertices;
    };

    struct cgnsSection
    {
        int sectionID;
        char sectionName[CG_NAMES_LENGTH];
        CG_ElementType_t elementType;
        int start;
        int end;
        int nbndry;
        int parentFlag;
        vector<int> elements;
        int parentData;

    };

    int fileType_;
    int fileHandle_;
    int err_;

    std::vector<cgnsBase> baseList_;
    std::vector<cgnsZone> zoneList_;
    std::vector<cgnsSection> sectionList_;

    void extractBases();
    void extractZones(int baseID);
    void extractCoordinates(int baseID, int zoneID);
    void extractSections(int baseID, int zoneID);

};

#endif // CGNS_FILE_H
