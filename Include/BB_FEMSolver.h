#ifndef BB_FEMSOLVER_H
#define BB_FEMSOLVER_H

#include "BB_Mesh.h"

class BB_FEMSolverDesc {
public:
  BB_FEMSolverDesc();

  std::shared_ptr<bb::Mesh> meshPtr_;
};

class BB_FEMSolver {
public:
  BB_FEMSolver();

  void setMesh(const bb::Mesh *meshPtr);

  void defineStiffMatrix();

  void defineSourceVector();

  void detectContraintNodes();

  void defineBNDconditions();

  void runAssembler();

  void runSolver();

private:
  std::shared_ptr<bb::Mesh> meshObj;
  bb::SquareMatrix globalStiffMatrix_;
  bb::dVec globalSourceVector_;
  bb::dVec resultVec_;

  bb::dVec constraintVector_;
  double loadQ_;
  double elasticity_;

  struct todoList {
    bool meshDefined = false;
    bool bndDefined = false;
    bool stiffMatrixDefined = false;
    bool sourceVectorDefined = false;
    bool constrDefined = false;
  } checkList;
};

#endif // BB_FEMSOLVER_H
