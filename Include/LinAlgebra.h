//#pragma once
//#include <cuda_runtime.h>
//#include <device_launch_parameters.h>
//#include <iostream>
//#include <vector>
//#include <initializer_list>
//#include <memory>

////#define DEBUG
//#ifdef DEBUG
//    #define LOG(e) e
//#else
//    #define LOG(e)
//#endif

//#define LOGNOW(e) e

//#include "BB_DataStructure.h"

//using namespace std;
//class A
//{
//public:
//    A(){
//        LOG(cout << "A Ctor size" << sizeof (a) + sizeof(b)<< ", " << this << endl;)
//    }
//    A(const A& copyObj){
//        cout << "A copy Ctor " << this << endl;
//    }
//    A(const A&& copyObj){
//        cout << "A move Ctor " << this << endl;
//    }
//    A operator=(const A& copyObj){
//        cout << "A copy assignment Ctor " << this << endl;
//        return *this;
//    }
//    A operator=(const A&& moveObj){
//        cout << "A move assignment Ctor " << this << endl;
//        return *this;
//    }

//    bool operator<(const A& obj) const{
//        return true;
//    }

//    int a = 11;
//    char b = 'c';
//    ~A(){
//        cout << "DTOR" << this << endl;
//    }

//    friend ostream& operator<<(ostream& os, const A& obj){
//        os << &obj;
//        return os;
//    }
//};

//class DispObj;

//class dVec : public bb::nVec<double>
//{
//public:

//    //////////////////////////////////////////////////
//    ////  constructors
//    //////////////////////////////////////////////////
//    dVec();
//    dVec(uint size);
//    dVec(uint size, uint capacity);

//    void operator=(uint&& dim) = delete;
//    dVec(const dVec& copyObj);
//    dVec& operator=(const dVec& copyObj);

//    dVec(dVec&& moveObj);
//    void operator=(dVec&& moveObj);

//    dVec(std::initializer_list<double>&& initlist);
//    void operator=(std::initializer_list<double>& initlist);

//    dVec(const DispObj& obj);
//    dVec& operator=(const DispObj& obj);

//    ~dVec();

//    const double* data() const {return data_;}

//    double length();
//    void weight();
//    //////////////////////////////////////////////////
//    ////  operators
//    //////////////////////////////////////////////////
//    dVec operator+(const dVec& operand);
//    dVec operator-(const dVec& operand);
//    dVec& operator+=(const dVec& operand);
//    dVec& operator-=(const dVec& operand);

//    double operator*(const dVec& operand);
//    dVec operator*(const double scalar);
//    dVec& operator*=(const double scalar);
//    friend dVec operator*(const double scalar, dVec& operand);
//    dVec operator/(const double scalar);
//    bool operator==(const dVec& obj);
//    dVec operator!();

//protected:

//};

//class Vec2 : public dVec
//{
//public:
//    Vec2();
//    Vec2(double x, double y = 0.0);

//    double& x;
//    double& y;
//};

//class Vec3 : public dVec
//{
//public:
//    Vec3();
//    Vec3(double x, double y = 0.0, double z = 0.0);

//    double& x;
//    double& y;
//    double& z;
//};

//class Vec4 : public dVec
//{
//public:
//    Vec4();
//    Vec4(double x, double y = 0.0, double z = 0.0, double w = 0.0);

//    double& x;
//    double& y;
//    double& z;
//    double& w;
//};


//class DispObj{
//public:
//    double& operator[](uint colIndex){
//        return *(posPtr_+ colIndex*stride_);
//    }

//    uint num_;
//    uint stride_;
//    double* posPtr_;
//};


//class Matrix{
//public:

//    /////////////////////////////////////////////////////////////////////////
//    //  Matrix ctors

//    Matrix();
//    Matrix(uint row, uint col);

//    Matrix(const Matrix& copyObj);
//    Matrix(Matrix&& moveObj);

//    Matrix& operator=(const Matrix& copyObj);
//    Matrix& operator=(Matrix&& moveObj);

//    Matrix(std::initializer_list<initializer_list<double> >&& obj);
//    Matrix operator=(std::initializer_list<initializer_list<double> >&& obj);
//    Matrix(std::initializer_list<dVec> &&obj);
//    Matrix operator=(std::initializer_list<dVec>&& obj);

//    ~Matrix();
//    /////////////////////////////////////////////////////////////////////////

//    const double* data() const { return dataVec_.data(); }

//    void randomize();
//    void zeros();

//    uint rows() const;
//    uint cols() const;
//    uint numOfElems() const {return row_ * col_;}

//    /////////////////////////////////////////////////////////////////////////
//    // input/output functions
//    friend ostream& operator<<(ostream& os, const Matrix& obj){


//        os << "[";

//        for (uint i = 0; i < obj.rows(); i++){
//            for (uint j = 0; j < obj.cols(); j++){

//                if (obj.transpose_)
//                os << obj[i][j] << " ";
//            }
//            if (i == obj.rows() -1)
//                os << "]";
//            else
//                os << endl;
//        }
//        return os;
//    }

//    DispObj& operator[](uint row) const ;
//    DispObj& operator()(uint col) const ;
//    /////////////////////////////////////////////////////////////////////////


//    /////////////////////////////////////////////////////////////////////////
//    // operators

//    Matrix operator*(const Matrix& obj);
//    Matrix operator+(const Matrix& obj);
//    Matrix& operator+=(const Matrix& obj);
//    Matrix operator-(const Matrix& obj);
//    Matrix& operator-=(const Matrix& obj);
//    Matrix operator*(const double scalar) const ;
//    Matrix operator/(const double scalar) const ;
//    friend Matrix operator*(const double scalar, Matrix& obj) {return obj * scalar;}
//    bool operator==(const Matrix& obj);
//    Matrix operator~();

//    /////////////////////////////////////////////////////////////////////////



//    bool transposed() {return transpose_;}
//private:
//    /////////////////////////////////////////////////////////////////////////
//    ///    members

//    DispObj* dispObj_ = nullptr;

//    uint row_;
//    uint col_;
//    dVec dataVec_;

//    bool transpose_ = false;
//};

//class SquareMatrix : public Matrix
//{
//public:
//    SquareMatrix();
//    SquareMatrix(uint size);

//    SquareMatrix(const SquareMatrix& obj);
//    SquareMatrix& operator=(const SquareMatrix& obj);

//    SquareMatrix(SquareMatrix&& moveObj);
//    SquareMatrix& operator=(SquareMatrix&& obj);

//    SquareMatrix(std::initializer_list<initializer_list<double> >&& obj);
//    SquareMatrix operator=(std::initializer_list<initializer_list<double> >&& obj);
//    SquareMatrix(std::initializer_list<dVec> &&obj);
//    SquareMatrix operator=(std::initializer_list<dVec>&& obj);
//};


////class SymMatrix : public SquareMatrix
////{
////public:

////    SymMatrix();
////    SymMatrix(uint size);

////    friend ostream& operator<<(ostream& os, const SymMatrix& obj){
////        os << endl << "[";
////        for (uint i = 0; i < obj.rows(); i++){
////            for (uint j = 0; j < obj.cols(); j++){
////                os << obj.dataVec_[i*obj.cols() + j] << " ";
////            }
////            if (i == obj.rows() -1)
////                os << "]";
////            else
////                os << endl;
////        }
////        return os;
////    }
////};

