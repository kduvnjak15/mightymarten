#pragma once

#include <iostream>
#include <math.h>

#include "BB_Base.h"

#include "BB_DataStructure.h"
//#include "BB_LinAlgebra.h"

#define RESTART_INDEX 0xFFFF

using namespace std;

namespace bb {

class Face {
public:
  Face() {
    nodes[0] = 0;
    nodes[1] = 0;
    nodes[2] = 0;
  }
  Face(uint node1, uint node2, uint node3, uint stride = 0) {
    nodes[0] = node1 + stride;
    nodes[1] = node2 + stride;
    nodes[2] = node3 + stride;
  }

  void setNodes(uint node1, uint node2, uint node3, uint stride = 0) {
    nodes[0] = node1;
    nodes[1] = node2;
    nodes[2] = node3;
  }
  void setTex(double coordX, double coordY) {}

  void setNormals(uint normalID1, uint normalID2, uint normalID3) {
    normals[0] = normalID1;
    normals[1] = normalID2;
    normals[2] = normalID3;
  }

  uint nodes[3];
  uint normals[3];

  friend ostream &operator<<(ostream &os, Face &obj) {
    os << obj.nodes[0] << "/" << obj.normals[0] << " " << obj.nodes[1] << "/"
       << obj.normals[1] << " " << obj.nodes[2] << "/" << std::endl;
    return os;
  }

private:
};

class PolyFace {
public:
  PolyFace() {

    LOG(std::cout << this << " -> PolyFace default ctor" << std::endl;)
  };

  void operator=(PolyFace &copyObj) {
    LOG(std::cout << this << " -> PolyFace copy assignment operator"
                  << std::endl;)

    this->nodes_ = copyObj.nodes_;
    this->nodeNormals_ = copyObj.nodeNormals_;
    return;
  }

  PolyFace(bb::nVec<uint> &nodesList) {
    // da ima
    nodes_ = nodesList;
  }

  PolyFace(std::initializer_list<uint> &initList) { nodes_ = initList; }
  PolyFace(std::initializer_list<uint> &&initList) { nodes_ = initList; }

  void appendNode(uint node, int normal) {
    nodes_.push_back(node);
    nodeNormals_.push_back(normal);
    return;
  }

  void popNode(uint node) {
    std::cout << "TO BE IMPLEMENTED " << std::endl;

    return;
  }

  uint dim() { return nodes_.size(); }

  const bb::nVec<uint> &polyFaceNodes_() const { return nodes_; }

  const bb::nVec<int> &polyFaceNormals_() { return nodeNormals_; }

  ~PolyFace() {
    //    nodes_.clear();
    //    nodeNormals_.clear();
  }

  friend std::ostream &operator<<(std::ostream &os, const PolyFace &obj) {
    os << "polyface nodes: " << obj.nodes_;

    return os;
  }

  const uint operator[](uint index) const { return nodes_[index]; }
  uint operator[](uint index) { return nodes_[index]; }

  bb::nVec<int> nodeNormals_;

private:
  bb::nVec<uint> nodes_;
};

class PolyCell {
public:
  PolyCell();

  PolyCell(bb::nVec<uint> &polyfaces);
  PolyCell(std::initializer_list<uint> &&polyFaces);

  void appendPolyFace(uint polyfaceID);
  const bb::nVec<uint> &getCellPolyFaceIDs() const { return polyFaces_; }
  ~PolyCell();

private:
  bb::nVec<uint> polyFaces_;
};

class NodeSelection {
public:
  NodeSelection() = delete;
  NodeSelection(std::string name);
  NodeSelection(std::string name, bb::nVec<uint> &nodes);
  NodeSelection(std::string name, bb::nVec<uint> &&nodes);
  NodeSelection(std::string name, std::initializer_list<uint> &nodes);
  NodeSelection(std::string name, std::initializer_list<uint> &&nodes);

private:
  bb::nVec<uint> nodes_;
  const std::string name_;
};

class Mesh : public ISubject {
public:
  class Vertex;

  Mesh();
  Mesh(const Mesh &meshobj);
  //  ~Mesh() { throw std::runtime_error("NEMA NEMA"); }

  uint appendVertex(Vec3 vertex);
  void setVertices(bb::nVec<Vec3> &vertices);
  uint appendNormal(Vec3 &normal);
  void setNormals(bb::nVec<Vec3> &normals);
  uint appendFace(PolyFace &face);
  void setFaces(const bb::nVec<PolyFace> &faces);
  uint appendCell(PolyCell &cell);

  const uint getNumVertices() const { return vertices_.size(); }
  const uint getNumFaces() const { return faces_.size(); }
  const uint getNumCells() const { return cells_.size(); }

  const bb::nVec<Vec3> &getVertices();
  const bb::nVec<Vec3> &getNormals();
  const bb::nVec<PolyFace> &getFaces();
  const bb::nVec<PolyCell> &getCells();
  const bb::PolyCell &getCell(uint index);
  const bb::PolyFace &getFace(uint index);
  const bb::Vec3 &getVertex(uint index);

  const bb::Vec3 getMaxPt() const;
  const bb::Vec3 getMinPt() const;

  void clean();

  void updateMinMaxPt(Vec3 &vertex);

  friend std::ostream &operator<<(std::ostream &os, const Mesh &meshobj) {
    os << "Mesh " << &meshobj << " has : " << meshobj.getNumCells() << "cells;"
       << std::endl
       << meshobj.getNumFaces() << " faces; \n"
       << meshobj.getNumVertices() << " vertices; \n";

    return os;
  }

  class Vertex {
  public:
    Vertex() {}

    Vertex(const bb::Vec3 &coord, const bb::Vec3 &normal)
        : coord_(coord), normal_(normal) {}

    friend std::ostream &operator<<(ostream &os, Vertex &obj) {
      os << "!" << obj.coord_ << "~" << obj.normal_ << std::endl;
      return os;
    }

    bb::Vec3 coord_;
    bb::Vec3 normal_;
  };

private:
  bb::nVec<Vec3> vertices_;
  bb::nVec<Vec3> vertNormals_;
  bb::nVec<PolyFace> faces_;
  bb::nVec<PolyCell> cells_;

  bb::Vec3 maxPt_;
  bb::Vec3 minPt_;
};

class TopoGen {
public:
  TopoGen() = delete;

  static void generateHex(bb::Vec3 minPt, bb::Vec3 maxPt);
  static void generateHex(bb::Vec3 center, double length);
  static void generateHexGeneral(bb::nVec<Vec3> &eightCorners);
  static void generateQuadRect(Vec2 minPt, Vec2 maxPt, uint discrY,
                               uint discrX);

  static void print();

  static Mesh &getMeshObj();

  static void reset();

  ~TopoGen();

private:
  static bb::Mesh resultMesh_;
};
} // namespace bb
