#pragma once
#include <ostream>

class BB_RefCounter {
public: 
	//ctor
	BB_RefCounter() : counter_(0) {}
	BB_RefCounter(const BB_RefCounter&) = delete; 
	BB_RefCounter& operator=(const BB_RefCounter&) = delete;
	//dtor
	~BB_RefCounter() {

	}

	void reset() {
		counter_ = 0;
	}

	unsigned int get() { return counter_; }
	void operator++() { counter_++; }
	void operator++(int) { counter_++; }
        void operator--() { counter_--; }
        void operator--(int) { counter_--; }

	friend std::ostream& operator<<(std::ostream& os, const BB_RefCounter& counter) {
		os << "Counter value " << counter.counter_ << std::endl;
	}

private: 
	unsigned int counter_;
};


template <typename T> 
class BB_SmartPointer
{
public: 
	//ctor
	explicit BB_SmartPointer(T* ptr = nullptr) {
		ptr_ = ptr;
		cnt_ = new BB_RefCounter();
		if (ptr) {
			cnt_->operator++();
		}
	}

	// copy-ctor
	BB_SmartPointer(const BB_SmartPointer& obj) {
		ptr_ = obj.ptr_;
		cnt_ = obj.cnt_;
		cnt_->operator++();
	}

	unsigned int use_count() {
		return cnt_->get();
	}

	// get pointer
	T* get() {
		return ptr_;
	}

	T& operator*() {
		return *ptr_;
	}

	T* operator->() {
		return ptr_;
	}

	~BB_SmartPointer() {
		cnt_->operator--();
		if (cnt_->get() == 0) {
			delete cnt_;
			delete ptr_;
		}
	}

	friend std::ostream& operator<<(std::ostream& os, BB_SmartPointer<T>& sp) {
		os << "Address pointed " << sp.get() << " with refcount " << sp.use_count() << std::endl;

		return os;
	}

private: 

        BB_RefCounter* cnt_ = nullptr;
        T* ptr_ = nullptr;

};

template<typename T>
using BB_SP = BB_SmartPointer<T>;
