#pragma once

#include "VulkanAPI.h"
#include "WindowWin32.h"


class APP
{
public: 

	APP() = default;

	void run();

private: 

	void init();

	VulkanAPI vkApi; 
	WindowWin32 window;
};

