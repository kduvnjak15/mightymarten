#pragma once

#include "BB_LinAlgebra.h"
#include <functional>
using namespace bb;

SquareMatrix rect16fem2(bb::Vec2 pt1, bb::Vec2 pt2, bb::Vec2 pt3, bb::Vec2 pt4,
                        uint integrationPts = 4);

class rect16fem {
public:
  rect16fem() = delete;
  rect16fem(const rect16fem &obj) = delete;

  static SymMatrix getLocalStiffnessMatrix(bb::Vec2 minPt, bb::Vec2 maxPt,
                                           uint integrationPts = 4);

  static bb::dVec getSourceVector(bb::Vec2 minPt, bb::Vec2 maxPt,
                                  uint integrationPts = 4);

private:
  //  static void initializeFunctors();

  static bb::nVec<
      bb::nVec<std::function<double(double, double, double, double)>>>
      stiffMatElementFunctors_;

  static bb::nVec<std::function<double(double, double, double, double)>>
      sourceVectorElementFunctors_;
};
