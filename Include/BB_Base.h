
#pragma once

typedef unsigned int uint;

//#define DEBUG
#ifdef DEBUG
#define LOG(e) e
#else
#define LOG(e)
#endif

#define LOGNOW(e) e

// class Atest{
// public:
//    Atest(){
//        std::cout << "test class A ctor" << this << std::endl;
//    }

//    ~Atest(){
//        std::cout << "test class A dtor" << this << std::endl;
//    }

//    friend std::ostream& operator<<(std::ostream& os, Atest& obj){
//        std::cout << obj << ", " ;
//    }
//};

class BB_Base {
public:
  BB_Base();
  ~BB_Base();
};

#include <iostream>

#include <map>
#include <string>
#include <vector>

using namespace std;

class IObserver {

public:
  IObserver() { LOG(std::cout << "observer ctor " << this << std::endl;) }

  virtual void update(std::string message) = 0;

  virtual ~IObserver() { std::cout << "observer dtor " << this << std::endl; }
};

class ISubject {
public:
  ISubject() {
    LOG(std::cout << "This is ISubject ctor " << this << std::endl;)
  }

  virtual void subscribe(std::string message, IObserver *observer) {
    if (observerMap_.find(message) == observerMap_.end()) {

      observerList_ *newObserverList = new observerList_;
      observerMap_.insert({message, *newObserverList});
      observerMap_[message].push_back(observer);

      std::cout << "ObserverObj: " << observer
                << " subscribed to topic : " << message << std::endl;
    } else {

      observerMap_[message].push_back(observer);
    }
  }

  virtual void unsubscribe(std::string message, IObserver *observer) {
    if (observerMap_.find(message) == observerMap_.end()) {
      std::cout << "There is no message found" << std::endl;
    } else {
      for (auto obs = observerMap_[message].begin();
           obs != observerMap_[message].end(); obs++) {
        if (*obs == observer) {

          observerMap_[message].erase(obs);
        }
        std::cout << "ObserverObj: " << observer
                  << " unsubscribed to topic : " << message << std::endl;
      }
    }
  }

  virtual void notify(std::string message) {
    for (auto subs : observerMap_[message]) {
      subs->update(message);
    }
  }

  virtual ~ISubject() {
    LOG(std::cout << "This is virtual ISubject dtor " << this << std::endl;)
  }

private:
  typedef std::vector<IObserver *> observerList_;
  typedef std::map<std::string, observerList_> ObserverMap;
  ObserverMap observerMap_;
};

class raketa : public IObserver {
public:
  raketa() {
    std::cout << "This is raketa default ctor: " << this << std::endl;
  }

  void update(std::string msg) {
    std::cout << "raketa::update " << msg << std::endl;
  }

  ~raketa() {
    std::cout << "This is raketa default dtor: " << this << std::endl;
  }
};

class radar : public ISubject {
public:
  radar() { std::cout << "This is radar default ctor: " << this << std::endl; }

  ~radar() { std::cout << "This is radar default dtor: " << this << std::endl; }
};


namespace bb {

};