#pragma once
#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>

#include "BB_Base.h"

#include "BB_Algorithms.h"
#include "BB_LinAlgebra.h"
#include "BB_STL.h"

#define ALLOCATION_FACTOR 1.5

#define MAXNODES 10
#define LAMBDA -1

namespace bb {

enum nTreeDim { BINARY = 2, TERNARY = 3, QUAD = 4, OCTREE = 8 };

typedef nTreeDim treeDim;

template <typename T, treeDim dim = OCTREE> class node {
public:
  node(T val, node *parent) {
    this->val_ = val;
    parent_ = parent;
  }

  virtual node *insertNode(T val) {

    if (val < val_) {
      if (!left_child) {
        left_child = new node(val, this);
      } else {
        left_child->insertNode(val);
      }
    } else {
      if (!right_child) {
        right_child = new node(val, this);
      } else {
        right_child->insertNode(val);
      }
    }
  }

  uint getNodeHeight() const {
    if (!this)
      return 0;

    uint lHeight = this->left_child->getNodeHeight();
    uint rHeight = this->right_child->getNodeHeight();

    // use the larger one
    if (lHeight > rHeight)
      return (lHeight + 1);
    else
      return (rHeight + 1);
  }

  node *left_child = nullptr;
  node *right_child = nullptr;
  node *parent_ = nullptr;

  uint level_ = 0;
  uint pos_ = 0;
  uint stride_ = 0;

  T val() const { return val_; }

protected:
  node() { std::cout << this << "Tree ctor" << std::endl; }

private:
  T val_;
};

template <typename T> class Tree : public node<T> {
public:
  Tree<T>() : node<T>() {}

  node<T> *insertNode(T val) {
    if (!root_)
      root_ = new node<T>(val, nullptr);
    else
      root_->insertNode(val);
  }

  friend std::ostream &operator<<(std::ostream &os, Tree<T> &tree) {

    tree.traverse();

    return os;
  }

  friend class node<T>;

  void traverse() {
    if (!root_)
      return;

    breadthFirst(root_);
  }

  void initDispMap() {
    uint dispMapSize = std::pow(2, this->getTreeDepth() - 1);

    dispMap_.resize(dispMapSize);

    uint width = std::pow(2, this->getTreeDepth()) - 1;

    for (uint i = 0; i < dispMap_.size(); i++) {

      dispMap_[i].resize(width, std::string(format, ' '));
    }
    return;
  }

  void fillDots(node<T> *tmpNode, uint tmpRow) {

    if (tmpNode->left_child) {
      uint rowCnt = tmpRow + 1;
      for (uint p = tmpNode->pos_ - 1; p > tmpNode->left_child->pos_; p--) {
        std::stringstream sss;
        sss << std::setw(format - 1) << "/";

        dispMap_[rowCnt][p] = sss.str();
        rowCnt++;
      }
    }
    if (tmpNode->right_child) {
      uint rowCnt = tmpRow + 1;
      for (uint p = tmpNode->pos_ + 2; p <= tmpNode->right_child->pos_; p++) {
        std::stringstream sss;
        sss << std::setw(format - 1) << "\\";

        dispMap_[rowCnt][p] = sss.str();
        rowCnt++;
      }
    }
  }

  void flush(uint userformat = 3) {
    if (userformat < 2)
      userformat = 2;
    this->format = userformat;

    this->breadthFirst(this->root_);

    initDispMap();
    uint row = 0;
    for (uint i = 0; i < bfsList_.size(); i++) {

      std::stringstream sss;
      sss << "[" << std::setw(format - 2) << bfsList_[i]->val() << "]";

      row = pow(2, this->getTreeDepth() - 1) -
            pow(2, this->getTreeDepth() - bfsList_[i]->level_);
      dispMap_[row][bfsList_[i]->pos_] = sss.str();

      fillDots(bfsList_[i], row);
    }

    for (uint i = 0; i < dispMap_.size(); i++) {
      for (auto entry : dispMap_[i]) {

        std::cout << entry;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  uint getTreeDepth() const { return root_->getNodeHeight(); }

  const node<T> *getRoot() { return root_; }

private:
  void preorderTraverse(const node<T> *tmpNode) {

    if (!tmpNode)
      return;

    std::cout << tmpNode->val() << std::endl;
    preorderTraverse(tmpNode->left_child);
    preorderTraverse(tmpNode->right_child);
  }

  void inorderTraverse(const node<T> *tmpNode) const {
    if (!tmpNode)
      return;

    inorderTraverse(tmpNode->left_child);
    std::cout << tmpNode->val() << std::endl;
    inorderTraverse(tmpNode->right_child);
  }

  void postorderTraverse(const node<T> *tmpNode) const {
    if (!tmpNode)
      return;

    postorderTraverse(tmpNode->left_child);
    postorderTraverse(tmpNode->right_child);
    std::cout << tmpNode->val() << std::endl;
  }

  void updateChildrenLevelAndPos(node<T> *tmpNode) {
    uint stride = (std::pow(2, (this->getTreeDepth() - tmpNode->level_)) - 1);

    if (tmpNode->left_child) {
      tmpNode->left_child->level_ = tmpNode->level_ + 1;
      tmpNode->left_child->stride_ = tmpNode->stride_ / 2;
      tmpNode->left_child->pos_ = tmpNode->pos_ - (tmpNode->stride_ / 2 + 1);
    }
    if (tmpNode->right_child) {
      tmpNode->right_child->level_ = tmpNode->level_ + 1;
      tmpNode->right_child->stride_ = tmpNode->stride_ / 2;
      tmpNode->right_child->pos_ = tmpNode->pos_ + (tmpNode->stride_ / 2 + 1);
    }
    return;
  }

  void breadthFirst(node<T> *tmpNode, uint level = 0, uint pos = 0) {
    if (!tmpNode)
      return;

    if (tmpNode == root_) {
      breadthFirst_.push(root_);
      tmpNode->level_ = 1;
      tmpNode->pos_ = (std::pow(2, tmpNode->getNodeHeight()) - 1) / 2;
      tmpNode->stride_ = tmpNode->pos_;
    }
    updateChildrenLevelAndPos(tmpNode);

    bfsList_.push_back(tmpNode);
    std::cout << tmpNode->val() << std::endl;

    if (tmpNode->left_child)
      breadthFirst_.push(tmpNode->left_child);
    if (tmpNode->right_child)
      breadthFirst_.push(tmpNode->right_child);
    breadthFirst_.pop();

    breadthFirst(breadthFirst_.front());
  }

  node<T> *root_ = nullptr;
  std::vector<std::vector<std::string>> dispMap_;
  std::queue<node<T> *> breadthFirst_;
  std::vector<node<T> *> bfsList_;

  uint format = 4;
};

class Point2D {
public:
  double x_, y_;

  uint operator<(const Point2D &val) {
    if (val.x_ < this->x_)
      if (val.y_ < this->y_)
        return 2;
      else
        return 1;
    else if (val.y_ < this->y_)
      return 3;
    else
      return 0;
  }
};

template <typename T, treeDim DIM> class nNode {
public:
  nNode<T, DIM>() {}
  nNode<T, DIM>(T &val, nNode *parent) {
    this->val_ = val;
    this->parent_ = parent;
  }

  virtual uint cmpFunc(T val) { return this->val_ < val; }

  virtual nNode *insertNode(T val) {

    uint pos = this->cmpFunc(val);
    if (!this->children_[pos])
      this->children_[pos] = new nNode<T, DIM>(val, this);
    else
      this->children_[pos]->insertNode(val);
  }

  T val() const { return val_; }

  uint getNodeHeight() {
    if (!this)
      return 0;

    uint heights[DIM];
    uint max = 0;
    for (uint i = 0; i < DIM; i++) {
      heights[i] = this->children_[i]->getNodeHeight();
      if (heights[i] > max) {
        max = heights[i];
      }
    }
    return max + 1;
  }

  nNode<T, DIM> *getChild(uint pos) { return *(this->children_ + pos); }

protected:
  virtual nNode<T, DIM> *operator[](uint pos) {
    return *(this->children_ + pos);
  }

  nNode<T, DIM> *children_[DIM];
  T val_;
  nNode<T, DIM> *parent_ = nullptr;
};

template <typename T, treeDim DIM> class nTree {
public:
  nTree() {}

  nNode<T, DIM> *insertNode(T val) {
    if (!root_) {
      root_ = new nNode<T, DIM>(val, nullptr);
    } else {
      root_->insertNode(val);
    }
  }

  uint getTreeDepth() {
    if (this->root_)
      return this->root_->getNodeHeight();
    else
      return 0;
  }

  nNode<T, DIM> *getRoot() { return root_; }

  void traverse(void (*traverseFunc)(nNode<T, DIM> *) = nullptr) {
    postorderTraverse(this->root_, traverseFunc);
  }

  ~nTree() { postorderTraverse(this->getRoot(), nTree<T, DIM>::dtorFunc); }

protected:
  void preorderTraverse(nNode<T, DIM> *tmpNode,
                        void (*traverseFunc)(nNode<T, DIM> *) = nullptr) {
    if (!tmpNode)
      return;

    if (traverseFunc) {
      traverseFunc(tmpNode);
    }

    for (uint i = 0; i < this->dimension_; i++) {
      preorderTraverse(tmpNode->getChild(i), traverseFunc);
    }
  }

  void
  postorderTraverse(nNode<T, DIM> *tmpNode,
                    void (*traverseFunc)(nNode<T, DIM> *argument) = nullptr) {
    if (!tmpNode)
      return;

    for (uint i = 0; i < this->dimension_; i++) {
      postorderTraverse(tmpNode->getChild(i), traverseFunc);
    }

    if (traverseFunc) {
      traverseFunc(tmpNode);
    }
  }

protected:
  nNode<T, DIM> *root_ = nullptr;
  uint dimension_ = DIM;

private:
  static void dtorFunc(nNode<T, DIM> *tmpNode) { delete tmpNode; }
};

typedef nTree<double, BINARY> binaryTree;

class oNode : public nNode<Vec3, OCTREE> {
public:
  oNode() {}
  oNode(Vec3 val, oNode *parent_) : nNode<Vec3, OCTREE>(val, parent_) {}

  virtual uint cmpFunc(oNode &node) {
    Vec3 dirVec = node.val() - center_;
    if (dirVec.z() >= 0) {
      if (dirVec.y() >= 0) {
        if (dirVec.x() >= 0)
          return 0;
        else
          return 1;
      } else {
        if (dirVec.x() >= 0)
          return 3;
        else
          return 2;
      }
    } else {
      if (dirVec.y() >= 0) {
        if (dirVec.x() >= 0)
          return 4;
        else
          return 5;
      } else {
        if (dirVec.x() >= 0)
          return 7;
        else
          return 6;
      }
    }
  }

  virtual oNode *insertNode(oNode &node) {

    uint pos = this->cmpFunc(node);
    if (!this->children_[pos])
      this->children_[pos] = new oNode(node.val(), this);

    else
      this->children_[pos]->insertNode(node.val());
  }

  Vec3 getCenter() { return center_; }
  Vec3 getLengt_() { return length_; }

protected:
  Vec3 center_;
  Vec3 length_;
};

class BBox {
public:
  BBox(Vec3 minPt, Vec3 maxPt) : minPt_(minPt), maxPt_(maxPt) {}

  BBox operator+(Vec3 &offset) {
    BBox tmp(*this);
    tmp.minPt_ = this->minPt_ + offset;
    tmp.maxPt_ = this->maxPt_ + offset;

    return tmp;
  }

  BBox operator[](uint pos) {
    bb::Vec3 center = (maxPt_ + minPt_) / 2;
    bb::Vec3 length = (maxPt_ - minPt_) / 2;

    BBox octant(center, center + length);
    Vec3 offset;

    if (pos == 0) {
    } else if (pos == 1) {
      offset.x() = -fabs(length.x());
    } else if (pos == 2) {
      offset.x() = -fabs(length.x());
      offset.y() = -fabs(length.y());
    } else if (pos == 3) {
      offset.y() = -fabs(length.y());
    } else if (pos == 4) {
      offset.z() = -fabs(length.z());
    } else if (pos == 5) {
      offset.x() = -fabs(length.x());
      offset.z() = -fabs(length.z());
    } else if (pos == 6) {
      offset.x() = -fabs(length.x());
      offset.y() = -fabs(length.y());
      offset.z() = -fabs(length.z());
    } else if (pos == 7) {
      offset.y() = -fabs(length.y());
      offset.z() = -fabs(length.z());
    }

    octant = octant + offset;

    return octant;
  }

  Vec3 minPt_;
  Vec3 maxPt_;
};

class octGeoNode {
public:
  octGeoNode() {}

  uint operator<(const octGeoNode &obj) { return this->center_ < obj.center_; }

  bb::nVec<bb::Vec3> vertices_;
  Vec3 center_;
  Vec3 minPt_;
  Vec3 maxPt_;
};

class octNode {
public:
  octNode(BBox bbox, octNode *parent, uint maxDepth)
      : bbox_(bbox), parent_(parent), maxDepth_(maxDepth) {
    this->center_ = (bbox.maxPt_ + bbox.minPt_) / 2;

    //        uint depth = this->getRootDist();

    //        if (depth < maxDepth)
    //            spawnChildren();
  }

  void insertVertex(Vec3 vertex) { flushVertex(vertex); }

  octNode *findVertex(Vec3 &vertex) {
    uint pos = this->cmpFunc(vertex);
    if (this->children_[pos])
      return this->children_[pos]->findVertex(vertex);

    for (auto vert = this->vertices_.begin(); vert != this->vertices_.end();
         vert++) {
      if ((vertex - vert->val_).length() < tolerance_)
        return this;
    }

    return nullptr;
  }

  void spawnChildren() {
    for (uint i = 0; i < OCTREE; i++) {
      spawnChild(i);
    }
  }

  bool isEmpty() {
    bool res = false;
    for (uint i = 0; i < OCTREE; i++) {
      if (this->children_[i])
        res |= this->children_[i]->isEmpty();
    }

    return res;
  }

  uint getNumVertices() {
    uint res = this->vertices_.size();
    for (uint i = 0; i < OCTREE; i++) {
      if (this->children_[i])
        res += this->children_[i]->getNumVertices();
    }

    return res;
  }

private:
  uint cmpFunc(Vec3 &vertex) {
    Vec3 dirVec = vertex - center_;
    if (dirVec.z() >= 0) {
      if (dirVec.y() >= 0) {
        if (dirVec.x() >= 0)
          return 0;
        else
          return 1;
      } else {
        if (dirVec.x() >= 0)
          return 3;
        else
          return 2;
      }
    } else {
      if (dirVec.y() >= 0) {
        if (dirVec.x() >= 0)
          return 4;
        else
          return 5;
      } else {
        if (dirVec.x() >= 0)
          return 7;
        else
          return 6;
      }
    }

    return 99;
  }

  void flushVertex(Vec3 vertex) {
    uint pos = this->cmpFunc(vertex);

    if (this->children_[pos]) {
      this->children_[pos]->insertVertex(vertex);

    } else {
      uint depth = this->getRootDist();
      if (depth < maxDepth_) {

        spawnChild(pos);
        this->children_[pos]->insertVertex(vertex);
      } else {
        vertices_.pushBack(vertex);
      }
    }

    return;
  }

  void spawnChild(uint childID) {

    this->children_[childID] = new octNode(bbox_[childID], this, maxDepth_);
  }

  uint getRootDist() {
    if (!this->parent_)
      return 1;
    else
      return parent_->getRootDist() + 1;
  }

  BBox bbox_;
  Vec3 center_;
  octNode *children_[OCTREE];
  octNode *parent_;
  uint maxDepth_;
  List<Vec3> vertices_;
  double tolerance_ = 1e-13;
};

class octree {
public:
  octree(Vec3 minPt, Vec3 maxPt, uint maxDepth)
      : minPt_(minPt), maxPt_(maxPt), maxDepth_(maxDepth) {}

  void insertVertex(Vec3 vertex) {
    if (root_)
      root_->insertVertex(vertex);
    else {
      root_ = new octNode(BBox(minPt_, maxPt_), nullptr, maxDepth_);
      root_->insertVertex(vertex);
    }
  }

  bool isEmpty() {
    if (!root_)
      return true;

    return root_->isEmpty();
  }

  uint getNumVertices() {
    if (!root_)
      return 0;

    return root_->getNumVertices();
  }

  bool findVertex(Vec3 &vertex) { return root_->findVertex(vertex); }

private:
  octNode *root_ = nullptr;
  Vec3 minPt_ = Vec3(0, 0, 0);
  Vec3 maxPt_ = Vec3(0, 0, 0);
  uint maxDepth_;
};
} // namespace bb
