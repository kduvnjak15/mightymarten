#pragma once

#include "BB_Algorithms.h"
#include <BB_Base.h>
#include <iomanip>
#include <iostream>
#include <memory>
#include <new>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <utility>
//#define DEBUG
#ifdef DEBUG
#define LOG(e) e
#else
#define LOG(e)
#endif

#define LOGNOW(e) e

#define ALLOCATION_FACTOR 1.5

namespace bb {

template <typename T> class Stack {
protected:
  class StackNode {
  public:
    StackNode(T val) : val_(val) {}

    T &getVal() { return val_; }
    const T &getVal() const { return val_; }
    StackNode *next_;

  protected:
    T val_;
  };

public:
  Stack() {}

  StackNode *push(T val) {

    StackNode *tmp = new StackNode(val);
    tmp->next_ = head_;
    head_ = tmp;
    size_++;
    return tmp;
  }

  void pop() {
    if (!head_)
      throw std::runtime_error("Stack is empty");

    StackNode *tmpNode = head_->next_;
    delete head_;
    head_ = tmpNode;

    size_--;
    return;
  }

  const T &top() const {

    if (!head_)
      throw std::runtime_error("Stack is empty");
    return head_->getVal();
  }

  T &top() {
    if (!head_)
      throw std::runtime_error("Stack is empty");
    return head_->getVal();
  }

  uint getSize() { return size_; }

  ~Stack() {
    while (head_) {
      StackNode *tmp = head_->next_;

      delete head_;
      size_--;
      head_ = tmp;
    }
  }

  bool empty() { return !head_; }

  friend std::ostream &operator<<(std::ostream &os, Stack &obj) {
    Stack<T> tmp;
    while (!obj.empty()) {
      tmp.push(obj.top());
      os << obj.top() << ", ";
      obj.pop();
    }

    while (!tmp.empty()) {
      obj.push(tmp.top());
      tmp.pop();
    }

    os << std::endl;
    return os;
  }

protected:
  StackNode *head_ = nullptr;
  uint size_ = 0;
};

template <typename T> class Queue {
private:
  class QueueNode {
  public:
    QueueNode(T val) : val_(val) {}
    QueueNode *next_ = nullptr;

    T &getVal() { return val_; }

  private:
    T val_;
  };

public:
  Queue() {}

  void enqueue(T val) {

    QueueNode *tmp = back_;

    back_ = new QueueNode(val);
    if (tmp)
      tmp->next_ = back_;

    if (!front_)
      front_ = back_;

    size_++;
    return;
  }

  void dequeue() {
    if (this->empty()) {
      //      throw std::runtime_error("QUEUE IS EMPTY");
      return;
    }

    QueueNode *tmp = front_;
    front_ = front_->next_;

    if (!front_)
      back_ = front_;

    delete tmp;

    size_--;
    return;
  }

  T &front() { return front_->getVal(); }
  T &back() { return back_->getVal(); }

  bool empty() { return !front_; }

  uint getSize() { return size_; }

  friend std::ostream &operator<<(std::ostream &os, Queue<T> &obj) {
    Queue<T> tmp;

    while (!obj.empty()) {
      tmp.enqueue(obj.front());

      os << obj.front() << ", ";
      obj.dequeue();
    }

    while (!tmp.empty()) {
      obj.enqueue(tmp.front());
      tmp.dequeue();
    }

    return os;
  }

protected:
  uint size_ = 0;
  QueueNode *front_ = nullptr;
  QueueNode *back_ = nullptr;
};

template <class T> class List {
  class node;

public:
  List() {
    head_.next_ = tailPtr_;
    head_.prev_ = nullptr;

    tail_.next_ = nullptr;
    tail_.prev_ = headPtr_;
  }

  ~List() {
    node *nit = this->begin().toNodePtr();
    while (nit != this->end().toNodePtr()) {
      node *nextPtr = nit->next_;

      delete nit;
      nit = nextPtr;
    }
  }

  class iter : public std::iterator<std::input_iterator_tag, const uint8_t> {
  public:
    typedef iter _Unchecked_type;
    iter() {}

    iter(node *currPtr) : current_(currPtr) {}

    bool operator==(const iter &tmpIt) const {
      return this->current_ == tmpIt.current_;
    }

    bool operator!=(const iter &tmpIt) const {
      return this->current_ != tmpIt.current_;
    }

    explicit operator bool() const { return current_ != nullptr; }

    virtual iter &operator++() {
      this->current_ = this->current_->next_;
      return *this;
    }

    virtual iter &operator++(int unused_param) {
      this->current_ = this->current_->next_;
      return *this;
    }

    virtual iter &operator--() {
      this->current_ = this->current_->prev_;
      return *this;
    }

    virtual iter &operator--(int unused_param) {
      this->current_ = this->current_->prev_;
      return *this;
    }

    T &operator*() { return current_->val_; }

    node *operator->() { return current_; }

    node *toNodePtr() { return current_; }

  protected:
    node *current_ = nullptr;
  };

  class reverse_iter : public iter {

    typedef reverse_iter _Unchecked_type;

  public:
    reverse_iter(node *currPtr) : iter(currPtr) {}
    virtual iter &operator++() {
      this->current_ = this->current_->prev_;
      return *this;
    }
    virtual iter &operator++(int unused_param) {
      this->current_ = this->current_->prev_;
      return *this;
    }

    virtual iter &operator--() {
      this->current_ = this->current_->next_;
      return *this;
    }
    virtual iter &operator--(int unused_param) {
      this->current_ = this->current_->next_;
      return *this;
    }
  };

  void pushBack(T &val) {
    node *newNode = new node(val, tail_.prev_, tailPtr_);

    tail_.prev_->next_ = newNode;
    tail_.prev_ = newNode;

    size_++;
  }

  void pushBack(T &&val) {
    node *newNode = new node(std::forward<T>(val), tailPtr_->prev_, tailPtr_);

    tail_.prev_->next_ = newNode;
    tail_.prev_ = newNode;

    size_++;
  }

  void pushFront(T &val) {
    node *newNode = new node(val, headPtr_, headPtr_->next_);

    head_.next_->prev_ = newNode;
    head_.next_ = newNode;

    size_++;
  }

  void pushFront(T &&val) {
    node *newNode = new node(val, headPtr_, headPtr_->next_);

    head_.next_->prev_ = newNode;
    head_.next_ = newNode;

    size_++;
  }

  void insertNode(iter nodeIter, T val) {

    node *newNode =
        new node(val, nodeIter.toNodePtr()->prev_, nodeIter.toNodePtr());

    nodeIter->prev_->next_ = newNode;
    nodeIter->prev_ = newNode;

    size_++;
  }

  void popFront() {

    if (headPtr_->next_ != tailPtr_) {

      node *tmpNode = headPtr_->next_;
      headPtr_->next_->next_->prev_ = headPtr_;
      headPtr_->next_ = headPtr_->next_->next_;

      delete tmpNode;
      size_--;
    } else {
      std::cout << "List is empty " << this << std::endl;
    }
  }

  void popBack() {
    if (tailPtr_->prev_ != headPtr_) {

      node *tmpNode = tailPtr_->prev_;
      tailPtr_->prev_->prev_->next_ = tailPtr_;
      tailPtr_->prev_ = tailPtr_->prev_->prev_;

      delete tmpNode;
      size_--;
    } else {
      std::cout << "List is empty " << this << std::endl;
    }
  }

  void popNode(node *nodePtr) {
    if (!nodePtr)
      return;

    node *tmpPrev = nodePtr->prev_;
    node *tmpNext = nodePtr->next_;

    if (tmpPrev)
      tmpPrev->next_ = tmpNext;
    else
      head_ = tmpNext;

    if (tmpNext)
      tmpNext->prev_ = tmpPrev;
    else
      tail_ = tmpPrev;

    delete nodePtr;

    size_--;
  }

  iter find(const iter start, const iter end, T &&val) const {
    iter lit = start;
    while (lit != end) {
      if ((*lit) == val)
        return lit;

      lit++;
    }
    return this->end();
  }

  iter find(T &val) const {
    return this->find(this->begin(), this->end(), std::forward<T>(val));
  }

  iter find(T &&val) const {
    return this->find(this->begin(), this->end(), std::forward<T>(val));
  }

  void reverse() {
    std::swap(headPtr_, tailPtr_);
    node *nit = this->head_.next_;

    std::swap(head_.next_, tail_.prev_);
    std::swap(head_.prev_, tail_.next_);

    while (nit != this->headPtr_) {
      node *nextPtr = nit->next_;

      std::swap(nit->next_, nit->prev_);

      nit = nextPtr;
    }
  }

  void swapElements(iter it1, iter it2) {

    if (it1 == this->end() || it2 == this->end())
      throw std::runtime_error("ERROR, element not in list; end reached!");

    std::swap(it1.toNodePtr()->prev_->next_, it2.toNodePtr()->prev_->next_);
    std::swap(it1.toNodePtr()->next_->prev_, it2.toNodePtr()->next_->prev_);
    std::swap(it1.toNodePtr()->next_, it2.toNodePtr()->next_);
    std::swap(it1.toNodePtr()->prev_, it2.toNodePtr()->prev_);
  }

  iter begin() { return iter(head_.next_); }
  reverse_iter rbegin() { return reverse_iter(tailPtr_->prev_); }
  iter end() { return iter(tailPtr_); }
  reverse_iter rend() { return reverse_iter(headPtr_); }

  const iter begin() const { return iter(head_.next_); }
  const reverse_iter rbegin() const { return reverse_iter(tailPtr_->prev_); }
  const iter end() const { return iter(tailPtr_); }
  const reverse_iter rend() const { return reverse_iter(headPtr_); }

  uint size() const { return size_; }

  friend std::ostream &operator<<(std::ostream &os, const List &listObj) {
    if (listObj.size() == 0) {
      os << "<empty list " << &listObj << ">";
    } else {
      os << listObj.size_ << ": ";
      List<T>::iter nodePtr = listObj.begin();
      while (nodePtr != listObj.end()) {
        os << nodePtr->val_ << ", ";
        nodePtr++;
      }
    }

    return os;
  }

  T &front() { return headPtr_->next_->val_; }
  T &back() { return tailPtr_->prev_->val_; }
  const T &front() const { return headPtr_->next_->val_; }
  const T &back() const { return tailPtr_->prev_->val_; }

  //// todo
  void merge();
  void sort();

  bool operator==(List<T> &compareList);
  bool operator!=(List<T> &compareList);

private:
  class node {
  public:
    node() {}
    node(T &&val, node *prev, node *next)
        : val_(val), prev_(prev), next_(next) {
      //      std::cout << "node by rvalue " << std::endl;
    }

    node(T &val, node *prev, node *next) : val_(val), prev_(prev), next_(next) {
      //      std::cout << "node by reference value " << std::endl;
    }

    T val_;
    node *prev_ = nullptr;
    node *next_ = nullptr;
  };

  uint size_ = 0;

  node *headPtr_ = &head_;
  node *tailPtr_ = &tail_;
  node head_;
  node tail_;
};

template <class T> class nVec : public ISubject {
public:
  nVec<T>() {
    this->data_ = nullptr;
    LOG(std::cout << this << " -> nVec<T> default ctor" << std::endl;)
  }

  nVec<T>(uint size) : nVec<T>(size, (size + 1) * ALLOCATION_FACTOR) {
    LOG(std::cout << this << " -> nVec<T>::parameter ctor(size) " << size
                  << std::endl;)

    //    this->capacity_ = (size + 1) * ALLOCATION_FACTOR;
    //    nVec<T>(size, capacity_);

    LOG(std::cout << this << " -> nVec<T>::parameter ctor(size) end " << size
                  << std::endl;)
  }

  nVec<T>(uint size, uint capacity)
      : size_(size), capacity_(capacity), data_(nullptr) {
    LOG(std::cout << this << " -> nVec<T>::parameter ctor(size,capacity) "
                  << size << std::endl;)
    this->allocate();
    LOG(std::cout << this << " -> nVec<T>::parameter ctor(size,capacity) end "
                  << size << std::endl;)
  }

  nVec<T>(const nVec<T> &obj) : data_(nullptr) {
    LOG(std::cout << this << " -> nVec<T> copy ctor " << std::endl;)
    this->size_ = obj.size_;
    this->capacity_ = obj.capacity_;

    this->data_ = static_cast<T *>(operator new(sizeof(T) * capacity_));
    for (uint i = 0; i < size_; i++) {
      new (this->data_ + i) T{obj[i]};
    }

    LOG(std::cout << this << " -> nVec<T> copy ctor end " << size_
                  << std::endl;)
  }

  nVec<T>(nVec<T> &&obj) : data_(nullptr) {
    LOG(std::cout << this << " -> nVec<T> move ctor " << std::endl;)
    this->size_ = obj.size_;
    this->capacity_ = obj.capacity_;
    this->data_ = obj.data_;

    obj.size_ = 0;
    obj.capacity_ = 0;
    obj.data_ = nullptr;

    LOG(std::cout << this << " -> nVec<T> move ctor end " << size_
                  << std::endl;)
  }

  nVec<T>(std::initializer_list<T> &&initList) : nVec<T>(initList.size()) {
    LOG(std::cout << this << " -> initlist ctor " << std::endl;)

    //    this->data_ = static_cast<T *>(operator new(sizeof(T) * capacity_));
   // std::cout << sizeof(this->data_) << std::endl;
    for (uint i = 0; i < size_; i++) {
      new (this->data_ + i) T{*(initList.begin() + i)};
    }
    LOG(std::cout << this << " -> initlist ctor end" << std::endl;)
  }

  void operator=(nVec<T> &&moveObj) {
    LOG(std::cout << this << " -> move assignment operator" << std::endl;)

    this->size_ = moveObj.size_;
    this->capacity_ = moveObj.capacity_;
    this->data_ = moveObj.data_;

    moveObj.size_ = 0;
    moveObj.capacity_ = 0;
    moveObj.data_ = nullptr;
    LOG(std::cout << this << " -> move assignment operator end" << std::endl;)
    return;
  }

  void operator=(nVec<T> &copyObj) {
    LOG(std::cout << this << " -> copy assignment operator" << std::endl;)

    this->clear();
    this->dealocate();

    this->size_ = copyObj.size_;
    this->capacity_ = copyObj.capacity_;

    this->data_ = static_cast<T *>(operator new(sizeof(T) * capacity_));
    for (uint i = 0; i < size_; i++) {
      new (this->data_ + i) T{copyObj[i]};
    }

    LOG(std::cout << this << " -> copy assignment operator end" << std::endl;)

    return;
  }

  void operator=(const nVec<T> &copyObj) {
    LOG(std::cout << this << " -> copy assignment operator" << std::endl;)

    this->clear();
    this->dealocate();

    this->size_ = copyObj.size_;
    this->capacity_ = copyObj.capacity_;

    this->data_ = static_cast<T *>(operator new(sizeof(T) * capacity_));
    for (uint i = 0; i < size_; i++) {
      new (this->data_ + i) T{copyObj[i]};
    }

    LOG(std::cout << this << " -> copy assignment operator end" << std::endl;)

    return;
  }

  nVec<T> &operator=(std::initializer_list<T> &&initList) {
    LOG(std::cout << this << " -> initializer list&& assignment operator "
                  << std::endl;)

    operator=(initList);

    LOG(std::cout << this << " -> initializer list&& assignment operator end "
                  << std::endl;)
    return *this;
  }

  nVec<T> &operator=(std::initializer_list<T> &initList) {
    LOG(std::cout << this << " -> initializer list assignment operator "
                  << std::endl;)
    this->clear();
    this->size_ = initList.size();
    this->capacity_ = (this->size_ + 1) * ALLOCATION_FACTOR;
    this->allocate();

    for (uint i = 0; i < initList.size(); i++) {
      new (this->data_ + i) T{*(initList.begin() + i)};
    }

    LOG(std::cout << this << " -> initializer list assignment operator end "
                  << std::endl;)

    return *this;
  }

  T *first() const { return data_; }
  T *begin() const { return data_; }
  T *last() const { return data_ + size_ - 1; }
  T *end() const { return data_ + size_ - 1; }

  void push_back(T &&obj) {

    if (size_ == capacity_) {
      this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
      reallocate();
    }

    new (data_ + size_) T{std::move(obj)};
    this->size_++;

    this->notify("update");
  }

  void push_back(T &obj) {

    if (size_ == capacity_) {
      this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
      reallocate();
    }

    new (data_ + size_) T{obj};
    this->size_++;
    this->notify("update");
  }

  void push_back(const T &obj) {

    if (size_ == capacity_) {
      this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
      reallocate();
    }

    *(data_ + size_) = obj;
    this->size_++;
  }

  void pop_back() {
    if (size_ == 0)
      return;

    (data_ + size_ - 1)->~T();
    size_--;
  }

  void clear() {

    for (uint i = 0; i < size_; i++) {
      (this->data_ + i)->~T();
    }

    //    operator delete(this->data_);

    size_ = 0;

    return;
  }

  void resize(uint size) {

    //    this->size_ = size; THIS HAS SOME BUG WITH RESIZEING TO SMALLER
    this->capacity_ = (size + 1) * ALLOCATION_FACTOR;
    T *newData = static_cast<T *>(operator new(capacity_ * sizeof(T)));
    for (uint i = 0; i < size_; i++) {
      new (newData + i) T{*(this->data_ + i)};
      (this->data_ + i)->~T();
    }
    operator delete(this->data_);

    this->data_ = newData;

    this->size_ = size;

    return;
  }

  void insert(uint pos, T &obj) {
    //      insert(pos, std::move(obj));

    std::runtime_error("NOT IMPLEMENTED");
  }

  void insert(uint pos, T &&obj) {
    if (pos >= size_)
      throw std::runtime_error("Pos is bigger than nVec size");

    //    if (size_ + 1 >= capacity_) {
    //      this->capacity_ = static_cast<uint>((size_ + 1) *
    //      ALLOCATION_FACTOR); reallocate();
    //    }

    //    if (size_ > 0) {
    //      for (uint i = size_; i > pos; i--) {
    //        *(data_ + i) = *(data_ + i - 1);
    //      }
    //    }
    //    *(data_ + pos) = std::move(obj);

    //    this->size_++;
    std::runtime_error("NOT IMPLEMENTED; SHOULD BE REVISITED ");
  }

  void remove(uint pos) {
    if (pos >= size_)
      throw std::runtime_error("Pos is bigger than nVec size");

    for (uint i = pos + 1; i < size_; i++) {
      *(this->first() + i - 1) = *(this->first() + i);
    }
    this->size_--;
  }

  void randomize() {
    for (T *pos = data_; pos < this->data_ + this->size_; pos++) {
      *pos = rand() % 20;
    }
  }

  nVec<T> &sort() {
    SortingAlgorithm<T>::getSortingAlgorithm().bubbleSort(this->data_,
                                                          this->size_);

    return *this;
  }

  friend std::ostream &operator<<(std::ostream &os, const nVec<T> &obj) {
    if (obj.size() == 0) {
      os << "< " << &obj << " is empty >" << std::endl;
    } else {
      os << "< ";
      for (uint i = 0; i < obj.size_ - 1; ++i) {
        os << *(obj.data_ + i) << ", ";
      }
      os << *(obj.data_ + obj.size_ - 1);
      os << " >";
    }
    return os;
  }

  T &operator[](uint pos) {
    if (pos < this->size_)
      return *(this->data_ + pos);
  }

  const T &operator[](uint pos) const {
    if (pos < this->size_)
      return *(this->data_ + pos);
  }

  T &at(uint pos) {
    if (pos > this->size_ - 1)
      throw std::runtime_error("nVec<T>.at() Bigger index than size");

    return *(data_ + pos);
  }

  T &at(uint pos) const {
    if (pos > this->size_ - 1)
      throw std::runtime_error("nVec<T>.at() Bigger index than size");

    return *(data_ + pos);
  }

  void init(T &&initVal) {
    for (uint i = 0; i < this->size_; i++) {
      *(this->data_ + i) = initVal;
    }
  }

  const uint size() const { return size_; }

  virtual ~nVec() {

    for (uint i = 0; i < size_; i++) {
      (this->data_ + i)->~T();
    }

    operator delete(this->data_);
    this->data_ = nullptr;

    LOG(std::cout << this << " -> nVec<T> ~dtor  " << std::endl;)
  }

protected:
  T *data_ = nullptr;
  uint size_ = 0;
  uint capacity_ = 0;

protected:
  void allocate() {
    LOG(std::cout << this << " -> allocate size " << this->size_
                  << ", capacity " << this->capacity_ << std::endl;)

    operator delete(this->data_);

    this->data_ = static_cast<T *>(operator new(sizeof(T) * capacity_));
    for (uint i = 0; i < size_; i++) {
      new (this->data_ + i) T{};
    }

    return;
  }

  void reallocate() {
    LOG(std::cout << this << " -> reallocate size " << this->size_
                  << ", capacity " << this->capacity_ << std::endl;)
    T *newData = static_cast<T *>(operator new(capacity_ * sizeof(T)));
    for (uint i = 0; i < size_; i++) {
      new (newData + i) T{std::move(*(this->data_ + i))};
      //      (this->data_ + i)->~T();
    }

    operator delete(this->data_);
    this->data_ = newData;
  }

  void dealocate() {
    operator delete(this->data_);
    this->data_ = nullptr;
    this->capacity_ = 0;
  }

private:
};
} // namespace bb
