﻿
#pragma once
#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string.h>
//#define DEBUG
#ifdef DEBUG
#define LOG(e) e
#else
#define LOG(e)
#endif

#define LOGNOW(e) e

#define ALLOCATION_FACTOR 1.5

#include "BB_Algorithms.h"
#include "BB_STL.h"

using std::cout;
using std::endl;

class A {
public:
  A() {
    this->a = rand() % 100;
    LOGNOW(std::cout << "A Ctor size" << sizeof(a) + sizeof(b) << ", " << this
                     << endl;)
  }

  A(const A &copyObj) {
    this->a = copyObj.a;
    LOG(std::cout << "A copy Ctor " << this << std::endl;)
  }

  A(const A &&moveObj) {
    this->a = moveObj.a;
    LOG(cout << "A move Ctor " << this << std::endl;)
  }
  A operator=(const A &copyObj) {
    this->a = copyObj.a;
    LOG(cout << "A copy assignment Ctor " << this << std::endl;)
    return *this;
  }
  A operator=(const A &&moveObj) {
    this->a = moveObj.a;
    LOG(cout << "A move assignment Ctor " << this << std::endl;)
    return *this;
  }

  bool operator<(const A &obj) const {
    LOG(cout << "rose " << this->a << " <> " << obj.a << std::endl;)
    return this->a < obj.a;
  }

  bool operator>(const A &obj) const {
    LOG(cout << "rose " << this->a << " <> " << obj.a << std::endl;)
    return this->a > obj.a;
  }

  int a = 11;
  char b = 'c';
  ~A() { LOG(cout << "DTOR" << this << std::endl;) }

  friend std::ostream &operator<<(std::ostream &os, const A &obj) {
    os << obj.a;
    return os;
  }
};

namespace bb {

class dVec {
public:
  //////////////////////////////////////////////////
  ////  constructors
  //////////////////////////////////////////////////
  dVec();
  dVec(uint dim);
  dVec(const dVec &copyObj);
  dVec &operator=(const dVec &copyObj);

  dVec(dVec &&moveObj);
  void operator=(dVec &&moveObj);
  void operator=(uint &&dim) = delete;

  double &operator[](uint pos);
  const double &operator[](uint pos) const;
  bb::nVec<double> &data();
  const bb::nVec<double> &data() const;

  const uint size() const;

  friend std::ostream &operator<<(std::ostream &os, const dVec &obj) {
    if (obj.size() == 0) {
      os << "[ " << &obj << " is empty >" << std::endl;
    } else {
      os << "[ ";
      for (uint i = 0; i < obj.size() - 1; ++i) {
        os << *(obj.data().first() + i) << ", ";
      }

      os << *(obj.data().first() + obj.size() - 1);
      os << " ]";
    }
    return os;
  }

  dVec(std::initializer_list<double> &initList);
  dVec(std::initializer_list<double> &&initList);
  void operator=(std::initializer_list<double> &initlist);

  virtual ~dVec();

  void push_back(double elem);

  const double length() const;

  //////////////////////////////////////////////////
  ////  operators
  //////////////////////////////////////////////////
  dVec operator+(const dVec &operand) const;
  dVec operator-(const dVec &operand) const;
  dVec &operator+=(const dVec &operand);
  dVec &operator-=(const dVec &operand);
  double operator*(const dVec &operand) const;
  dVec operator*(const double scalar);
  friend dVec operator*(double scalar, bb::dVec &operand) {
    return operand * scalar;
  }
  dVec &operator*=(const double scalar);
  dVec operator/(const double scalar);
  dVec &operator/=(const double scalar);

  bool operator==(const dVec &obj);
  dVec operator!();
  void operator~();

  dVec getNormalized() const;
  void normalize();

protected:
  bb::nVec<double> data_;
  bool transposed = false;
}; // namespace bb

// bb::dVec bb::operator*(double scalar, bb::dVec &operand);
// bb::dVec bb::operator*(const double scalar, const bb::dVec &operand);

template <typename T, uint N> class VecN {
public:
  VecN() {

    for (uint i = 0; i < N; i++) {
      *(this->data_ + i) = 0;
    }

    LOG(std::cout << this << " -> Vec" << N << " default ctor" << std::endl;)
  }

  VecN(const VecN &copyObj) {
    memcpy(this->data_, copyObj.data(), N * sizeof(T));
  }

  VecN &operator=(const VecN &copyObj) {

    //    std::cout << "copy assignment  " << copyObj << std::endl;
    memcpy(this->data_, copyObj.data(), N * sizeof(T));
    return *this;
  }

  const T &operator[](uint pos) const {
    if (pos > N - 1)
      throw std::runtime_error(
          std::string("bb::Vec3 has only " + std::to_string(N) + " coords "));

    return *(this->data_ + pos);
  }

  T &operator[](uint pos) {
    if (pos > N - 1)
      throw std::runtime_error(
          std::string("bb::Vec3 has only " + std::to_string(N) + " coords "));

    return *(this->data_ + pos);
  }

  double operator*(const VecN &obj) const {
    double sum = 0;
    for (uint i = 0; i < N; i++) {
      sum += *(this->data_ + i) * *(obj.data_ + i);
    }
    return sum;
  }

  VecN operator+(const VecN &obj) const {
    VecN tmp(*this);
    for (uint i = 0; i < N; i++) {
      *(tmp.data_ + i) += *(obj.data_ + i);
    }
    return tmp;
  }

  VecN operator-(const VecN &obj) const {
    VecN tmp(*this);
    for (uint i = 0; i < N; i++) {
      *(tmp.data_ + i) -= *(obj.data_ + i);
    }
    return tmp;
  }

  VecN operator*(const double scalar) const {
    VecN tmp(*this);
    for (uint i = 0; i < N; i++) {
      *(tmp.data_ + i) *= scalar;
    }
    return tmp;
  }

  VecN operator/(const double scalar) const {
    VecN tmp(*this);
    for (uint i = 0; i < N; i++) {
      *(tmp.data_ + i) /= scalar;
    }
    return tmp;
  }

  double length() const {
    double sum = 0;
    for (uint i = 0; i < N; i++) {
      sum += *(this->data_ + i) * (*(this->data_ + i));
    }
    return std::sqrt(sum);
  }

  VecN normalized() const {
    VecN tmp(*this);

    return tmp / tmp.length();
  }

  void normalize() const {
    *this / this->length();
    return;
  }

  void zeros() { memset(this->data_, 0, sizeof(this->data_)); }
  void init(T val) {
    for (uint i = 0; i < N; i++) {
      *(this->data_ + i) = val;
    }
  }
  void ones() { this->init(1); }

  double *data() { return data_; }
  const double *data() const { return data_; }
  uint dim() const { return N; }

  friend VecN operator*(double scalar, VecN &obj) { return obj * scalar; }

  friend std::ostream &operator<<(std::ostream &os, const VecN<T, N> &obj) {
    os << "[ ";
    for (uint i = 0; i < obj.dim() - 1; ++i) {
      os << *(obj.data() + i) << ", ";
    }
    os << *(obj.data_ + obj.dim() - 1);
    os << " ]";

    return os;
  }

  friend std::ostream &operator<<(std::ostream &os, VecN<T, N> &&obj) {
    os << obj;
    return os;
  }

  double dot(const VecN &obj) const { return obj * (*this); }

protected:
  double data_[N];
};

class Vec2 : public VecN<double, 2> {
public:
  Vec2() : VecN<double, 2>() {
    LOG(std::cout << this << " -> Vec2 default ctor" << std::endl;)
  }

  Vec2(double x, double y = 0.0) {
    *(this->data_) = x;
    *(this->data_ + 1) = y;
    LOG(std::cout << this << " -> (x, y) parameter ctor" << std::endl;)
  }

  Vec2(const VecN *copyPtr) : VecN(*copyPtr) {}
  Vec2(const VecN &copyObj) : VecN(copyObj) {}
  Vec2(const Vec2 *copyPtr) : VecN(*copyPtr) {}
  Vec2(const Vec2 &copyObj) : VecN(copyObj) {}

  Vec2 normalized() const {
    Vec2 tmp(*this);
    return tmp / tmp.length();
  }

  const double &x() const { return *data_; }
  const double &y() const { return *(data_ + 1); }
  double &x() { return *data_; }
  double &y() { return *(data_ + 1); }

  uint operator<(const Vec2 &obj) { return 3; }

  const bb::Vec2 operator*(const double scalar) const;
  const bb::Vec2 operator*(const uint scalar) const;
  friend bb::Vec2 operator*(const double scalar, const bb::Vec2 &vecObj) {
    bb::Vec2 tmpVec(vecObj);
    tmpVec.x() *= scalar;
    tmpVec.y() *= scalar;

    return std::move(tmpVec);
  }
  friend bb::Vec2 operator*(const uint scalar, const bb::Vec2 &vecObj) {
    bb::Vec2 tmpVec(vecObj);
    tmpVec.x() *= scalar;
    tmpVec.y() *= scalar;

    return std::move(tmpVec);
  }

  //  ~Vec2() { std::cout << "VEC@DTOR" << std::endl; }
};

class Vec3 : public VecN<double, 3> {
public:
  Vec3() : VecN<double, 3>() {
    LOG(std::cout << this << " -> Vec3 default ctor" << std::endl;)
  }

  Vec3(const VecN *copyPtr) : VecN(*copyPtr) {}
  Vec3(const VecN &copyObj) : VecN(copyObj) {}
  Vec3(const Vec3 *copyPtr) : VecN(*copyPtr) {}
  Vec3(const Vec3 &copyObj) : VecN(copyObj) {}

  Vec3(double x, double y = 0.0, double z = 0.0) {
    *(this->data_) = x;
    *(this->data_ + 1) = y;
    *(this->data_ + 2) = z;

    LOG(std::cout << this << " -> (x, y, z) parameter ctor"
                  << *(this->data_ + 2) << std::endl;)
  }

  Vec3 operator+(const Vec3 &obj) {
    Vec3 tmp(*this);
    tmp.x() += obj.x();
    tmp.y() += obj.y();
    tmp.z() += obj.z();

    return tmp;
  }

  const double &x() const { return *data_; }
  const double &y() const { return *(data_ + 1); }
  const double &z() const { return *(data_ + 2); }

  double &x() { return *data_; }
  double &y() { return *(data_ + 1); }
  double &z() { return *(data_ + 2); }
  bb::Vec2 xy() const { return bb::Vec2(this->x(), this->y()); }

  Vec3 normalized() const {
    Vec3 tmp(*this);
    return tmp / tmp.length();
  }

  //// class specific
  Vec3 cross(const Vec3 &obj) const {
    Vec3 tmp(*(this->data_ + 1) * obj[2] - (*(this->data_ + 2)) * obj[1],
             *(this->data_ + 2) * obj[0] - (*this->data_) * obj[2],
             *(this->data_) * obj[1] - (*(this->data_ + 1)) * obj[0]);
    return tmp;
  }

  friend Vec3 operator^(const Vec3 &first, const Vec3 &second) {
    return first.cross(second);
  }

  uint operator<(const Vec3 &obj) const { return 1; }

  Vec3 &operator=(const Vec3 &obj) {

    memcpy(this->data_, obj.data_, 3 * sizeof(double));

    return *this;
  }
};

class Vec4 : public VecN<double, 4> {
public:
  Vec4() : VecN<double, 4>() {
    LOG(std::cout << this << " -> Vec3 default ctor" << std::endl;)
  }

  Vec4(const VecN *copyPtr) : VecN(*copyPtr) {}
  Vec4(const VecN &copyObj) : VecN(copyObj) {}
  Vec4(const Vec4 *copyPtr) : VecN(*copyPtr) {}
  Vec4(const Vec4 &copyObj) : VecN(copyObj) {}

  Vec4(double x, double y = 0.0, double z = 0.0, double w = 0.0) {
    *(this->data_) = x;
    *(this->data_ + 1) = y;
    *(this->data_ + 2) = z;
    *(this->data_ + 3) = w;

    LOG(std::cout << this << " -> (x, y, z, w) parameter ctor" << std::endl;)
  }

  const double &x() const { return *data_; }
  const double &y() const { return *(data_ + 1); }
  const double &z() const { return *(data_ + 2); }
  const double &w() const { return *(data_ + 3); }

  double &x() { return *data_; }
  double &y() { return *(data_ + 1); }
  double &z() { return *(data_ + 2); }
  double &w() { return *(data_ + 3); }

  Vec4 normalized() const {
    Vec4 tmp(*this);
    return tmp / tmp.length();
  }

  friend Vec4 operator*(double scalar, Vec4 &obj) { return obj * scalar; }

  uint operator<(const Vec3 &obj) const { return 1; }
};

class DispObj {
public:
  //  DispObj() = default;
  DispObj() = delete;
  DispObj(double *posPtr, uint num, uint stride = 1);

  DispObj(const DispObj &obj);

  DispObj(const dVec &obj);
  DispObj operator=(const dVec &obj);

  double &operator[](uint colIndex);

  friend std::ostream &operator<<(std::ostream &os, const DispObj &obj) {
    if (obj.num_ == 0) {
      os << "[ " << &obj << " is empty >" << std::endl;
    } else {
      os << "[ ";
      for (int i = 0; i < obj.num_ - 1; ++i) {
        os << *(obj.posPtr_ + i * obj.stride_) << ", ";
      }
      os << *(obj.posPtr_ + (obj.num_ - 1) * obj.stride_);
      os << " ]";
    }
    return os;
  }

  uint num_;
  uint stride_;
  double *posPtr_;
};

class SymDispObj : public DispObj {
public:
  SymDispObj() = delete;
  SymDispObj(double *posPtr, uint num, uint idx = 0);

  friend std::ostream &operator<<(std::ostream &os, const SymDispObj &obj) {
    if (obj.num_ == 0) {
      os << "[ " << &obj << " is empty >" << std::endl;
    } else {
      os << "[ ";
      for (uint i = 0; i < obj.num_; i++) {

        uint row = obj.stride_;
        uint col = i;
        if (col < row)
          std::swap(row, col);

        //          if (j >= i) {
        //            os << *(obj.posPtr_ + i * obj.num_ + j) << ", ";
        //          } else {
        //            os << *(obj.posPtr_ + j * obj.num_ + i) << ", ";
        //          }
        //        uint pivot = obj.stride_ * obj.num_ -
        //                     (obj.stride_ * (obj.stride_ - 1) / 2) + (col -
        //                     row);
        uint pivot = row * obj.num_ - (row * (row - 1) / 2) + (col - row);

        os << *(obj.posPtr_ + pivot);
        if (i < obj.num_ - 1)
          os << ", ";

        //              os << *(obj.posPtr_ + i*obj.stride_) << ", ";
      }
      //          for (int i = 0; i < obj.num_ - 1; ++i) {
      //            os << *(obj.posPtr_ + i*obj.stride_) << ", ";
      //          }
      //          os << *(obj.posPtr_ + obj.num_* obj.stride_- 1);
      os << " ]";
    }
    return os;
  }

  double &operator[](uint colIndex) {

    uint row = this->stride_;
    uint col = colIndex;

    if (col < row)
      std::swap(row, col);

    uint pos = row * this->num_ - (row * (row - 1) / 2) + (col - row);

    return *(posPtr_ + pos);
  }
};

//__global__ void matrixMultiplication(double *d_a, double *d_b, double *d_res)
//{

//  printf("this is supposed to be matrixmultiplication");
//}
class SymMatrix;

class Matrix {
public:
  /////////////////////////////////////////////////////////////////////////
  //  Matrix ctors

  Matrix();
  Matrix(uint row, uint col);
  Matrix(const Matrix &copyObj);
  Matrix(const SymMatrix &copyObj);
  Matrix &operator=(const Matrix &copyObj);

  Matrix(Matrix &&moveObj);

  Matrix &operator=(Matrix &&moveObj);

  Matrix(std::initializer_list<std::initializer_list<double>> &&obj);
  Matrix operator=(std::initializer_list<std::initializer_list<double>> &&obj);
  Matrix(std::initializer_list<dVec> &&obj);
  Matrix operator=(std::initializer_list<dVec> &&obj);

  ~Matrix();
  /////////////////////////////////////////////////////////////////////////

  const bb::dVec &data() const;
  bb::dVec &data();

  void randomize();

  void zeros();
  void ones();

  const uint rows() const;
  const uint cols() const;
  const uint numOfElems() const;
  double determinant() const;

  /////////////////////////////////////////////////////////////////////////
  // input/output functions
  friend std::ostream &operator<<(std::ostream &os, const Matrix &obj) {
    os << "[";

    if (obj.row_ < 1) {
      os << &obj << "is empty ]" << std::endl;
      return os;
    }
    for (uint i = 0; i < obj.row_; i++) {
      for (uint j = 0; j < obj.col_; j++) {
        os << *(obj.data().data().first() + i * obj.col_ + j) << " ";
      }
      if (i == obj.rows() - 1)
        os << "]";
      else
        os << std::endl;
    }
    return os;
  }

  DispObj &operator[](uint index);
  DispObj &operator()(uint col);
  DispObj operator[](uint index) const;
  DispObj operator()(uint col) const;

  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  // operators

  Matrix operator*(const Matrix &obj);

  Matrix operator+(const Matrix &sumand) const;
  Matrix &operator+=(const Matrix &sumand);
  Matrix operator-(const Matrix &sumand);
  Matrix &operator-=(const Matrix &sumand);
  Matrix operator*(const double scalar) const;
  Matrix operator/(const double scalar) const;
  dVec operator*(const bb::dVec &vecObj) const;
  friend Matrix operator*(const double scalar, const Matrix &obj);

  Matrix operator~();
  bool operator==(const Matrix &obj);
  static Matrix zeros(uint rows, uint cols);
  static Matrix ones(uint rows, uint cols);
  Matrix slice(uint startX, uint startY, uint stopX, uint stopY);
  dVec sliceRow(uint row, uint startX, int stopX = -1);
  dVec sliceCol(uint col, uint startY, int stopY = -1);

  static void removeRow(Matrix &matObj, uint row);
  static void removeCol(Matrix &matObj, uint col);

  //  Matrix &embedd(uint startX, uint startY, bb::Matrix &matObj) {
  //    uint dimy = this->row_;
  //    uint dimx = this->col_;

  //    if (dimy < (matObj.row_ + startY)) {
  //      dimy = matObj.row_ + startY;
  //    }
  //    if (dimx < (matObj.col_ + startX)) {
  //      dimx = matObj.col_ + startX;
  //    }

  //    Matrix temp(dimy, dimx);
  //    temp.zeros();

  //    for (uint i = 0; i < this->row_; i++) {
  //      for (uint j = 0; j < this->col_; j++) {
  //        temp[i][j] = (*this)[i][j];
  //      }
  //    }

  //    for (uint i = 0; i < matObj.rows(); i++) {
  //      for (uint j = 0; j < matObj.cols(); j++) {
  //        temp[i + startY][j + startX] = matObj[i][j];
  //      }
  //    }

  //    *this = temp;

  //    return *this;
  //  }

  Matrix &embedd(uint startX, uint startY, bb::Matrix &matObj);
  void setSize(uint rows, uint cols);

  /////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////
  ///    members
protected:
  DispObj *dispObj_;
  dVec dataVec_;

  uint row_;
  uint col_;
}; // namespace bb

class SquareMatrix : public Matrix {
public:
  SquareMatrix();
  SquareMatrix(uint size);
  SquareMatrix(const Matrix &obj);
  SquareMatrix(const SquareMatrix &obj);
  SquareMatrix(const SymMatrix &symObj);
  SquareMatrix &operator=(const SquareMatrix &obj);
  SquareMatrix &operator=(const Matrix &obj);
  SquareMatrix(SquareMatrix &&moveObj);

  SquareMatrix &operator=(SquareMatrix &&obj);
  SquareMatrix(std::initializer_list<std::initializer_list<double>> &&obj);
  SquareMatrix &
  operator=(std::initializer_list<std::initializer_list<double>> &&obj);
  SquareMatrix(std::initializer_list<dVec> &&obj) : Matrix(std::move(obj)) {}

  SquareMatrix &operator=(std::initializer_list<dVec> &&obj);

  SquareMatrix operator*(const SquareMatrix &obj) const;

  SquareMatrix operator*(const double scalar) const;
  //  friend SquareMatrix operator*(const double scalar, const SquareMatrix
  //  &obj);
  void eye();

  static SquareMatrix eye(uint size);
  static SquareMatrix eye(const SquareMatrix &obj);
  double determinant() const;
  SquareMatrix inverse() const;

  virtual double trace() const;
  static double trace(const SquareMatrix &obj);
  void LUdecomposition(const SquareMatrix &a, const SquareMatrix &l,
                       const SquareMatrix &u) const;
  double determinant2() const;
};

class SymMatrix : public SquareMatrix {
public:
  SymMatrix();
  SymMatrix(uint size);
  SymMatrix(const Matrix &obj);
  SymMatrix(const SquareMatrix &obj);
  SymMatrix(const SymMatrix &obj);
  Matrix &operator=(const Matrix &obj);
  SquareMatrix &operator=(const SquareMatrix &obj);
  SymMatrix &operator=(const SymMatrix &obj);
  SymMatrix(SymMatrix &&moveObj);
  SymMatrix &operator=(SymMatrix &&obj);

  friend std::ostream &operator<<(std::ostream &os, const SymMatrix &obj) {
    if (obj.rows() == 0) {
      os << "[ " << &obj << " is empty >" << std::endl;
    } else if (obj.rows() == 1) {
      os << obj.dataVec_ << std::endl;
      os << "[ " << *obj.data().data().first() << " ]" << std::endl;
    } else {
      //          os << obj.dataVec_ << std::endl;
      os << "[ ";

      for (uint row = 0; row < obj.rows(); row++) {
        for (uint col = 0; col < obj.cols(); col++) {
          os << std::setw(8) << obj[row][col] << ", ";
        }
        std::cout << std::endl;
      }

      //      for (uint j = 0; j < obj.cols(); j++) {
      //        os << *(obj.data().data().first() + j) << ", ";
      //        //              os << j << ", ";
      //      }
      //      os << std::endl << "  ";
      //      for (uint i = 1; i < obj.rows() - 1; i++) {
      //        for (uint j = 0; j < obj.cols(); j++) {

      //          uint ii = i;
      //          uint jj = j;
      //          if (j < i) {
      //            std::swap(ii, jj);
      //          } else {
      //          }

      //          os << *(obj.data().data().first() +
      //                  (ii * obj.cols() - (ii * ii + ii) / 2 + jj))
      //             << ", ";
      //        }
      //        os << " " << std::endl << "  ";
      //      }

      //      for (uint j = 0; j < obj.cols() - 1; j++) {
      //        uint i = obj.rows() - 1;
      //        if (j >= i) {
      //          os << *(obj.data().data().first() + i * obj.cols() -
      //                  (((i - 1) * (i - 1) + (i - 1)) / 2) + (j - i))
      //             << ", ";
      //        } else {
      //          os << *(obj.data().data().first() + j * obj.cols() -
      //                  (((j - 1) * (j - 1) + (j - 1)) / 2) + (i - j))
      //             << ", ";
      //        }
      //      }

      //      os << *(obj.dataVec_.data().last());

      os << " ]";
    }
    return os;
  }

  SymMatrix operator+(const SymMatrix &obj);
  SymMatrix operator-(const SymMatrix &obj);
  SymMatrix operator*(const SymMatrix &obj);
  SymMatrix operator/(const SymMatrix &obj);
  SymMatrix operator*(const double scalar);

  SymDispObj operator[](uint index) const;
  SymMatrix operator~();
  double determinant();
};

class VecDisp {
public:
  virtual std::ostream &disp(std::ostream &os) const = 0;

  virtual double &operator[](uint index) = 0;

  virtual void setDataPtr(double *ptr);
  virtual void setRowNum(uint row) { rows_ = row; }
  virtual void setColNum(uint col) { cols_ = col; }
  virtual void setIndex(uint index) { index_ = index; }

  virtual ~VecDisp() {}

protected:
  double *dataPtr_ = nullptr;
  uint rows_;
  uint cols_;
  uint index_;
};

class nonTransposedDisp : public VecDisp {
public:
  nonTransposedDisp(uint rows, uint cols, double *dataPtr) {
    rows_ = rows;
    cols_ = cols;
    dataPtr_ = dataPtr;
  }

  double &operator[](uint index) { return *(this->dataPtr_ + index); }

  std::ostream &disp(std::ostream &os) const {

    os << "< ";
    for (uint i = 0; i < this->cols_; i++) {
      os << *(this->dataPtr_ + i) << ", ";
    }
    os << " >" << std::endl;

    return os;
  }
};

class TransposedDisp : public VecDisp {
public:
  TransposedDisp(uint rows, uint cols, double *dataPtr) {
    rows_ = rows;
    cols_ = cols;
    dataPtr_ = dataPtr;
  }

  double &operator[](uint index) { return *(this->dataPtr_ + index); }

  std::ostream &disp(std::ostream &os) const {

    os << "transp< ";
    for (uint i = 0; i < this->rows_; i++) {
      os << *(this->dataPtr_ + i * this->cols_) << ", ";
    }
    os << " >" << std::endl;

    return os;
  }
};

class Mat2x2 : public SquareMatrix {
public:
  Mat2x2();
  Mat2x2(const Matrix &obj);
  Mat2x2(SymMatrix &obj);
};

class Quaternion;

class Mat3x3 : public SquareMatrix {
public:
  Mat3x3();
  Mat3x3(const Matrix &obj);
  Mat3x3(SymMatrix &obj);
  Vec3 operator*(const Vec3 &vecObj);
  Mat3x3 operator*(const Mat3x3 &matObj);
  Quaternion quaternion();
  static Mat3x3 rotMatAxis(double phi, const Vec3 &axisvec);
};

class Mat4x4 : public SquareMatrix {
public:
  Mat4x4();
  Mat4x4(const Matrix &obj) : SquareMatrix(4) {
    if (obj.cols() == 4 && obj.rows() == 4) {
      memcpy(const_cast<double *>(this->data().data().first()),
             obj.data().data().first(), obj.numOfElems() * sizeof(double));
    } else {
      throw std::runtime_error("Incompatible matrix with Mat2x2");
    }
  }

  Mat4x4(SymMatrix &obj) : SquareMatrix(4) {
    for (uint i = 0; i < 4; i++) {
      for (uint j = 0; j < 4; j++) {
        (*this)[i][j] = obj[i][j];
      }
    }
  }
};

class Quaternion {
public:
  Quaternion();
  Quaternion(double w, double x = 0, double y = 0, double z = 0);
  Quaternion(double phi, const Vec3 &axis);
  Quaternion(const Quaternion &copyObj);
  Quaternion operator=(const Quaternion &copyObj);
  Quaternion(std::initializer_list<double> &initList);
  void operator=(std::initializer_list<double> &initList);
  friend Quaternion operator*(double scalar, Quaternion &obj);
  Quaternion operator+(const Quaternion &obj);
  Quaternion operator-(const Quaternion &obj);
  Quaternion operator*(const double scalar);
  Quaternion operator/(const double scalar);
  Quaternion operator*(const Quaternion &obj);

  // conjugate
  Quaternion operator~();
  double norm();
  Quaternion unit();
  Quaternion inverse();
  Mat3x3 rotMatrix();

  Vec3 rotateVec3(Vec3 &obj);

public:
  double &x_;
  double &y_;
  double &z_;
  double &w_;

  friend std::ostream &operator<<(std::ostream &os, const Quaternion &obj) {
    os << " q[" << obj.data_[0] << ", " << obj.data_[1] << ", " << obj.data_[2]
       << ", " << obj.data_[3] << "]" << std::endl;

    return os;
  }

private:
  double data_[4];
};

} // namespace bb
