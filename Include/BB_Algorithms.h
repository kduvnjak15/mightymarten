#pragma once

#include <iostream>
#include <stdio.h>

#include "BB_Base.h"

namespace bb {

template <typename T> class SortingAlgorithm {

  enum sortType { BUBBLE, SELECTION, INSERTION, MERGE, QUICK };

public:
  /////////////////////////////////////////////////////////////////////
  ///  ctor
  static SortingAlgorithm &getSortingAlgorithm() {

    static SortingAlgorithm instance_;

    return instance_;
  }

  SortingAlgorithm(const SortingAlgorithm &obj) = delete;
  void operator=(const SortingAlgorithm &obj) = delete;
  //////////////////////////////////////////////////////////////////////

  void bubbleSort(T *firstPtr, uint numOfElems) {
    std::cout << "Using bb::bubbleSort " << std::endl;

    for (int i = 0; i < numOfElems - 1; i++) {
      for (int j = 0; j < numOfElems - i - 1; j++) {
        if (*(firstPtr + j) > *(firstPtr + j + 1))
          std::swap(*(firstPtr + j), *(firstPtr + j + 1));
      }
    }
    std::cout << "Using bb::bubbleSort...end " << std::endl;
  }

  void insertionSort(T *firstPtr, uint numOfElems, bool = false) {
    std::cout << "Using bb::insertionSort " << std::endl;

    std::cout << "Using bb::insertionSort...end " << std::endl;
  }

  //////////////////////////////////////////////////////////////////////

private:
  SortingAlgorithm() {
    std::cout << "SortingAlgorithm ctor singleton" << std::endl;
  }
};
}
