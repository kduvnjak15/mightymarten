#pragma once
#include <iostream> 


class BB_Window
{
public: 


#ifdef _WIN32
	virtual void* getInstanceHandle() = 0;
	virtual uint32_t* getWindowHandle() = 0;
#elif __linux__
	virtual void* getConnectionPtr() = 0;
	virtual uint32_t getWindowID() = 0;
#endif

	virtual void init() = 0;
	virtual void run() = 0;
};

