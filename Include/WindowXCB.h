#pragma once
#ifdef __linux__
#include <xcb/xcb.h
#include "BB_Window.h"

class WindowXCB : public BB_Window
{
public:
    WindowXcb();

    void run();

    xcb_connection_t* getConnPtr();
    xcb_window_t getWndID();

    ~WindowXcb();

private:

    bool init();


    void create_window();

    bool createContext();

private:
    xcb_connection_t* connPtr;
    const xcb_setup_t* setupPtr;
    xcb_screen_t* screenPtr;

    xcb_window_t windowID;
    xcb_window_t parentID;
    uint32_t rootVisual;

    xcb_gcontext_t contextID;
    xcb_drawable_t drawable;

    xcb_window_class_t classType;

    uint32_t propName;
    uint32_t propval[2];

    //
    uint16_t xPos;
    uint16_t yPos;
    uint16_t screenWidth;
    uint16_t screenHeight;
    uint16_t windowWidth;
    uint16_t windowHeight;

    xcb_atom_t wmProtocols;
    xcb_atom_t wmDeleteWin;
};

#endif