#pragma once
#include <vulkan/vulkan.h>
#include <vector>

class VulkanAPI
{
public:

    VulkanAPI(void* connectionPtr = nullptr, uint32_t windowID = 0);

    void reinitialize(void* cP, uint32_t wndID);

    ~VulkanAPI();

private:

    void init();

    void createPhysicalDevice();

    void createDevice();

    void createSurface();

    void detectQueue();

    void getSurfaceFormats();

    void run();
private:

    bool isInitialized_;

    void* connPtr_;
    uint32_t windowID_;

    std::vector<const char*> enabledExtensions_;

    VkInstance instance_;
    VkPhysicalDevice physDevice_;
    VkDevice device_;
    VkSurfaceKHR surface_;

    uint32_t graphicsQueue;
    uint32_t computeQueue;

    VkFormat colorFormat;
    VkColorSpaceKHR colorSpace;
};


