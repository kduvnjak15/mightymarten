#include "stdafx.h"

#include "BB_Mesh.h"

///////////////////////////////////////////////////////////////////////////
/////////                        BB_nVec

////////////////////////////////////////////////////////////////////////////
/////////                        BB_Cell

////////////////////////////////////////////////////////////////////////////
/////////                        BB_Mesh

bb::Mesh::Mesh() {}

bb::Mesh::Mesh(const bb::Mesh &meshobj) {
  this->cells_ = meshobj.cells_;
  this->faces_ = meshobj.faces_;
  this->vertices_ = meshobj.vertices_;
  this->vertNormals_ = meshobj.vertNormals_;

  this->maxPt_ = meshobj.maxPt_;
  this->minPt_ = meshobj.minPt_;

  std::cout << "calling copy ctor " << this->getFaces() << std::endl;
}

uint bb::Mesh::appendVertex(Vec3 vertex) {
  vertices_.push_back(vertex);

  updateMinMaxPt(vertex);

  return vertices_.size() - 1;
}

void bb::Mesh::setVertices(bb::nVec<bb::Vec3> &vertices) {
  this->vertices_.clear();
  this->vertices_ = vertices;
  return;
}

uint bb::Mesh::appendNormal(Vec3 &normal) {
  vertNormals_.push_back(normal);
  return vertices_.size() - 1;
}

void bb::Mesh::setNormals(bb::nVec<bb::Vec3> &normals) {
  this->vertNormals_.clear();
  this->vertNormals_ = normals;
  return;
}

uint bb::Mesh::appendFace(bb::PolyFace &face) {
  faces_.push_back(face);
  return faces_.size() - 1;
}

void bb::Mesh::setFaces(const bb::nVec<bb::PolyFace> &faces) {
  this->faces_.clear();
  this->faces_ = faces;
  return;
}

uint bb::Mesh::appendCell(bb::PolyCell &cell) {
  cells_.push_back(cell);
  return cells_.size() - 1;
}

const bb::nVec<bb::Vec3> &bb::Mesh::getVertices() { return vertices_; }

const bb::nVec<bb::Vec3> &bb::Mesh::getNormals() { return vertNormals_; }

const bb::nVec<bb::PolyFace> &bb::Mesh::getFaces() { return faces_; }

const bb::nVec<bb::PolyCell> &bb::Mesh::getCells() { return cells_; }

const bb::PolyCell &bb::Mesh::getCell(uint index) {
  return this->cells_[index];
}

const bb::PolyFace &bb::Mesh::getFace(uint index) {
  return this->faces_[index];
}

const bb::Vec3 &bb::Mesh::getVertex(uint index) {
  return this->vertices_[index];
}

const bb::Vec3 bb::Mesh::getMaxPt() const { return this->maxPt_; }

const bb::Vec3 bb::Mesh::getMinPt() const { return this->minPt_; }

void bb::Mesh::clean() {
  this->vertices_.clear();
  this->vertNormals_.clear();
  this->faces_.clear();
  this->cells_.clear();
}

void bb::Mesh::updateMinMaxPt(Vec3 &vertex) {

  if (vertex.x() > this->maxPt_.x())
    this->maxPt_.x() = vertex.x();
  if (vertex.y() > this->maxPt_.y())
    this->maxPt_.y() = vertex.y();
  if (vertex.z() > this->maxPt_.z())
    this->maxPt_.z() = vertex.z();

  if (vertex.x() < this->minPt_.x())
    this->minPt_.x() = vertex.x();
  if (vertex.y() < this->minPt_.y())
    this->minPt_.y() = vertex.y();
  if (vertex.z() < this->minPt_.z())
    this->minPt_.z() = vertex.z();

  return;
}

bb::PolyCell::PolyCell() {
  LOG(std::cout << this << "-> Cell dtor" << std::endl;)
}

bb::PolyCell::PolyCell(bb::nVec<uint> &polyfaces) {
  this->polyFaces_ = polyfaces;
}

bb::PolyCell::PolyCell(std::initializer_list<uint> &&polyFaces) {

  this->polyFaces_ = polyFaces;
}

void bb::PolyCell::appendPolyFace(uint polyfaceID) {
  polyFaces_.push_back(polyfaceID);
}

bb::PolyCell::~PolyCell() {
  //  this->polyFaces_.clear();
  LOG(std::cout << this << "-> Cell dtor" << std::endl;)
}

// bb::TopoGen::TopoGen() {}
bb::Mesh bb::TopoGen::resultMesh_;

void bb::TopoGen::generateHex(bb::Vec3 minPt, bb::Vec3 maxPt) {
  if (minPt.x() >= maxPt.x() || minPt.y() >= maxPt.y() ||
      minPt.z() >= maxPt.z())
    std::cout << "CATASTROPHIC ERROR: Hex has invalid definition" << std::endl;

  std::cout << "maxpt min pt " << maxPt << ", " << minPt << std::endl;
  bb::Vec3 center = (maxPt + minPt) / 2.0;

  bb::Vec3 dir = (maxPt - minPt) / 2.0;
  bb::Vec3 xoffset(dir.x(), 0, 0);
  bb::Vec3 yoffset(0, dir.y(), 0);
  bb::Vec3 zoffset(0, 0, dir.z());

  bb::nVec<bb::Vec3> corners;
  // lower side
  corners.push_back(minPt);

  bb::Vec3 tmpCoord;
  dir = (maxPt - minPt) / 2.0;
  dir.x() = -dir.x();
  dir.z() = -dir.z();
  tmpCoord = center + dir;
  corners.push_back(tmpCoord);

  dir = (maxPt - minPt) * 0.5;
  dir.z() = -dir.z();
  tmpCoord = center + dir;
  corners.push_back(tmpCoord);

  dir = (maxPt - minPt) / 2.0;
  dir.y() = -dir.y();
  dir.z() = -dir.z();
  tmpCoord = center + dir;
  corners.push_back(tmpCoord);

  // upper side
  dir = (maxPt - minPt) / 2.0;
  dir.x() = -dir.x();
  dir.y() = -dir.y();
  corners.push_back(center + dir);

  dir = (maxPt - minPt) / 2.0;
  dir.x() = -dir.x();
  corners.push_back(center + dir);

  dir = (maxPt - minPt) / 2.0;
  corners.push_back(maxPt);

  dir = (maxPt - minPt) / 2.0;
  dir.y() = -dir.y();
  corners.push_back(center + dir);
  std::cout << "Corners" << std::endl;
  //  std::cout << corners << std::endl;
  generateHexGeneral(corners);
}

void bb::TopoGen::generateHex(bb::Vec3 center, double length) {
  length *= 0.5;
  bb::Vec3 minPt = center - bb::Vec3(length, length, length);
  bb::Vec3 maxPt = center + bb::Vec3(length, length, length);
  std::cout << minPt << std::endl;
  std::cout << maxPt << std::endl;

  TopoGen::generateHex(minPt, maxPt);
}

void bb::TopoGen::generateHexGeneral(bb::nVec<bb::Vec3> &eightCorners) {
  if (eightCorners.size() != 8)
    std::cout << "CATASTROPHIC ERROR: hex should have eight corners"
              << std::endl;

  for (uint i = 0; i < 8; i++) {
    resultMesh_.appendVertex(eightCorners[i]);
  }

  // define aliases
  uint zero = resultMesh_.getNumVertices() - 8;
  uint one = resultMesh_.getNumVertices() - 7;
  uint two = resultMesh_.getNumVertices() - 6;
  uint three = resultMesh_.getNumVertices() - 5;
  uint four = resultMesh_.getNumVertices() - 4;
  uint five = resultMesh_.getNumVertices() - 3;
  uint six = resultMesh_.getNumVertices() - 2;
  uint seven = resultMesh_.getNumVertices() - 1;

  // define faces
  bb::PolyFace tmpFace1({zero, one, five, four});
  resultMesh_.appendFace(tmpFace1);
  bb::PolyFace tmpFace2({one, two, six, five});
  resultMesh_.appendFace(tmpFace2);
  bb::PolyFace tmpFace3({two, three, seven, six});
  resultMesh_.appendFace(tmpFace3);
  bb::PolyFace tmpFace4({three, zero, four, seven});
  resultMesh_.appendFace(tmpFace4);
  bb::PolyFace tmpFace5({six, seven, four, five});
  resultMesh_.appendFace(tmpFace5);
  bb::PolyFace tmpFace6({zero, three, two, one});
  resultMesh_.appendFace(tmpFace6);

  // define cells
  bb::PolyCell tmpCell;
  uint numOfFaces = resultMesh_.getNumFaces();
  tmpCell.appendPolyFace(numOfFaces - 1);
  tmpCell.appendPolyFace(numOfFaces - 2);
  tmpCell.appendPolyFace(numOfFaces - 3);
  tmpCell.appendPolyFace(numOfFaces - 4);
  tmpCell.appendPolyFace(numOfFaces - 5);
  tmpCell.appendPolyFace(numOfFaces - 6);
  resultMesh_.appendCell(tmpCell);

  return;
}

void bb::TopoGen::generateQuadRect(bb::Vec2 minPt, bb::Vec2 maxPt, uint discrY,
                                   uint discrX) {

  const double DX = maxPt.x() - minPt.x();
  const double DY = maxPt.y() - minPt.y();
  const double dx = DX / discrX;
  const double dy = DY / discrY;
  const bb::Vec2 xDir(1.0, 0.0);
  const bb::Vec2 yDir(0.0, 1.0);

  for (uint i = 0; i <= discrY; i++) {
    for (uint j = 0; j <= discrX; j++) {
      bb::Vec2 tmpVec = minPt + j * (xDir * dx) + i * (yDir * dy);

      bb::Vec3 tmpVec3(tmpVec.x(), tmpVec.y(), 0.0);
      resultMesh_.appendVertex(tmpVec3);
    }
  }

  for (uint i = 0; i < discrY; i++) {
    for (uint j = 0; j < discrX; j++) {
      uint pos = (i * (discrX + 1)) + j;

      bb::PolyFace tmpFace({pos, pos + 1, pos + discrX + 2, pos + discrX + 1});

      uint faceID = resultMesh_.appendFace(tmpFace);
      bb::PolyCell tmpCell{faceID};

      resultMesh_.appendCell(tmpCell);
    }
  }
  return;
}

void bb::TopoGen::print() {
  std::cout << "Print" << std::endl;
  for (uint i = 0; i < 8; i++) {
    std::cout << resultMesh_.getVertices()[i] << std::endl;
  }
}

bb::Mesh &bb::TopoGen::getMeshObj() { return resultMesh_; }

void bb::TopoGen::reset() { resultMesh_.clean(); }

bb::TopoGen::~TopoGen() {}

bb::NodeSelection::NodeSelection(string name) : name_(name) {}

bb::NodeSelection::NodeSelection(string name, bb::nVec<uint> &nodes)
    : name_(name), nodes_(nodes) {}

bb::NodeSelection::NodeSelection(string name, bb::nVec<uint> &&nodes)
    : name_(name), nodes_(nodes) {}

bb::NodeSelection::NodeSelection(string name,
                                 std::initializer_list<uint> &nodes)
    : name_(name) {
  this->nodes_ = nodes;
}

bb::NodeSelection::NodeSelection(string name,
                                 std::initializer_list<uint> &&nodes)
    : name_(name) {
  this->nodes_ = nodes;
}
