
#include "CGNS_File.h"
#include "cgnslib.h"

#include <iostream>
#include <stdexcept>

using namespace std;

//#define DEBUG_MODE

#ifdef DEBUG_MODE
    #define DEBUG std::cout << "In " << __FILE__<< " , at " << __LINE__ << "         : " << cg_get_error() << std::endl;;
#else
    #define DEBUG
#endif


CGNS_File::CGNS_File(const char* filePath)
{
    cg_is_cgns(filePath, &fileType_); DEBUG

    if ((fileType_ != CG_FILE_ADF && fileType_ != CG_FILE_HDF5) || fileType_ == CG_FILE_NONE) {
        throw std::runtime_error("Invalid file path or type!");

    }

    cg_open(filePath, CG_MODE_READ, &fileHandle_); DEBUG

    extractBases();

    cg_close(fileHandle_);
}

void CGNS_File::getCGNSInfo(){
    float fileVersion;
    cg_version(fileHandle_, &fileVersion); DEBUG

    int precision;
    cg_precision(fileHandle_, &precision); DEBUG

    SimulationType_t simType;
    cg_simulation_type_read(fileHandle_, 1, &simType);

    int ndescriptors;
    cg_ndescriptors(&ndescriptors);

    cout << "Displaying CGNS File info: " << endl;
    cout << "File version : " << fileVersion << endl;
    cout << "File precision : " << precision << "bit" << endl;
    cout << "File type: " << (fileType_ == 1 ? "ADF": (fileType_ == 2 ? "HDF" : "" ) )<< endl;
    cout << "Simulation type : " << simType << endl;

    cout << "Num of descriptors : " << ndescriptors << endl;
}

void CGNS_File::convert2BB_Mesh(BB_Mesh& dstMesh){

    for (const cgnsZone& zone : zoneList_)
    {
        for (int i = 0; i < zone.vertices.size(); i++){

            BB_Vert3 tempV(zone.vertices[i].x_, zone.vertices[i].y_, zone.vertices[i].z_);
            dstMesh.appendVertex(tempV);
        }
    }

    for (const cgnsSection& section : sectionList_){
        for (int i = 0; i < section.elements.size(); i += 5){
            BB_Cell tmp;
            if (section.elements[i] == 10){

//                tmp.facets[0] = BB_Triangle(section.elements[i+1],
//                                            section.elements[i+2],
//                                            section.elements[i+3]);
                int n1 = section.elements[i+1];
                int n2 = section.elements[i+2];
                int n3 = section.elements[i+3];
                int n4 = section.elements[i+4];


                tmp.nodes[0] = section.elements[i+1];
                tmp.nodes[1] = section.elements[i+2];
                tmp.nodes[2] = section.elements[i+3];
                tmp.nodes[3] = section.elements[i+4];
            }
            else
            {
//                throw std::runtime_error("Not a tetra");
                cout << "Move to next section "<< endl;
                break;
            }
            dstMesh.appendCell(tmp);
        }
    }
}

void CGNS_File::extractBases(){

    int nbases;
    cg_nbases(fileHandle_, &nbases); DEBUG

    baseList_.reserve(nbases);

    std::cout << nbases << std::endl;

    for (int baseID = 1; baseID <= nbases ; baseID++){
        cgnsBase tmpBase;
        tmpBase.baseID = baseID;

        cg_base_read(fileHandle_, tmpBase.baseID, tmpBase.baseName, &tmpBase.cellDim,  &tmpBase.physDim ); DEBUG
        baseList_.push_back(tmpBase);

        extractZones(baseID);
    }
}
#include <cstring>
void CGNS_File::extractZones(int baseID){

    int nzones;
    cg_nzones(fileHandle_, baseID, &nzones);

    for (int zoneID = 1; zoneID <= nzones; zoneID++){
        cgnsZone tmpZone;
        tmpZone.zoneID = zoneID;

        cg_zone_read(fileHandle_, baseID, zoneID, tmpZone.zoneName, tmpZone.zoneSize); DEBUG
        cg_index_dim(fileHandle_, baseID, zoneID, &tmpZone.index_dim); DEBUG

        cout << tmpZone.index_dim << endl;
        zoneList_.push_back(tmpZone);

        extractCoordinates(baseID, zoneID);
        extractSections(baseID, zoneID);
    }
}

void CGNS_File::extractSections(int baseID, int zoneID){

    int nsections;
    cg_nsections(fileHandle_, baseID, zoneID, &nsections);

    for (int sectionID = 1; sectionID <= nsections; sectionID++){
        cgnsSection tmpSection;
        tmpSection.sectionID = sectionID;

        cg_section_read(fileHandle_, baseID, zoneID, sectionID,
                        tmpSection.sectionName,
                        &tmpSection.elementType,
                        &tmpSection.start,
                        &tmpSection.end,
                        &tmpSection.nbndry,
                        &tmpSection.parentFlag);

        int elementDataSize;
        cg_ElementDataSize(fileHandle_, baseID, zoneID, sectionID, &elementDataSize);
        tmpSection.elements.resize(elementDataSize);
        cg_elements_read(fileHandle_, baseID, zoneID, sectionID,
                         tmpSection.elements.data(),
                         NULL);
        cout << "stop" << endl;

        sectionList_.push_back(tmpSection);

    }

}

void CGNS_File::extractCoordinates(int baseID, int zoneID){
    int ngrids;
    cg_ngrids(fileHandle_, baseID, zoneID, &ngrids); DEBUG
    cout << "Ngrids " <<ngrids << endl;

    char gridName[CG_NAMES_LENGTH];
    cg_grid_read(fileHandle_, baseID, zoneID, 1, gridName);
    cout << "gridName: " << gridName << endl;

    int ncoords;
    cg_ncoords(fileHandle_, baseID, zoneID, &ncoords);
    cout << "ncoords: " << ncoords << endl;

    int rmin[] = {1, 1, 1};
    int rmax[] = {zoneList_[zoneID-1].zoneSize[0], zoneList_[zoneID-1].zoneSize[0], zoneList_[zoneID-1].zoneSize[0]};


    CGNS_ENUMV(ZoneType_t) zoneType;
    cg_zone_type(fileHandle_, baseID, zoneID, &zoneType);

    int node_size;
    if (zoneType == CGNS_ENUMV(Unstructured)){
        node_size = zoneList_[zoneID-1].zoneSize[0];
    }
    else if (zoneType == CGNS_ENUMV(Structured))
    {
        for (int i = 0; i < 3; i++)
            rmax[i] = zoneList_[zoneID-1].zoneSize[i];

        node_size = rmax[0]*rmax[1]*rmax[2];
    }

    vector<double> xcoord(node_size), ycoord(node_size), zcoord(node_size);

    CGNS_ENUMV(DataType_t) dtt;
    char coordName[CG_NAMES_LENGTH];
    cg_coord_info(fileHandle_, baseID, zoneID, 1, &dtt, coordName );
//    cout << coordName << ", " << rmin[0] << ", " << rmax[0] <<", " <<  zoneList_[zoneID-1].index_dim << endl;

    cg_coord_read(fileHandle_, baseID, zoneID, "CoordinateX", CGNS_ENUMV(RealDouble), rmin, rmax, xcoord.data() ); DEBUG
    cg_coord_read(fileHandle_, baseID, zoneID, "CoordinateY", CGNS_ENUMV(RealDouble), rmin, rmax, ycoord.data() ); DEBUG
    cg_coord_read(fileHandle_, baseID, zoneID, "CoordinateZ", CGNS_ENUMV(RealDouble), rmin, rmax, zcoord.data() ); DEBUG

    for (int j = 0; j < node_size; j++){
        zoneList_[zoneID-1].vertices.push_back(BB_Vert3(xcoord[j], ycoord[j], zcoord[j]));
    }


}




