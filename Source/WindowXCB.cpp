#ifdef __linux__


#include "WindowXCB.h"

WindowXCB::WindowXcb()
{

    bool initialized = init();

    if (initialized)
    {
        create_window();
    }

    bool createContext();

    // close button
    xcb_intern_atom_cookie_t wmDeleteCookie = xcb_intern_atom(connPtr, 0, strlen("WM_DELETE_WINDOW"), "WM_DELETE_WINDOW");

    xcb_intern_atom_cookie_t wmProtocolsCookie = xcb_intern_atom(connPtr, 0, strlen("WM_PROTOCOLS"), "WM_PROTOCOLS");

    xcb_intern_atom_reply_t* wmDeleteReply = xcb_intern_atom_reply(connPtr, wmDeleteCookie, NULL);

    xcb_intern_atom_reply_t* wmProtocolsReply = xcb_intern_atom_reply(connPtr, wmProtocolsCookie, NULL);

    wmProtocols = wmProtocolsReply->atom;
    wmDeleteWin = wmDeleteReply->atom;

    xcb_change_property(connPtr, XCB_PROP_MODE_REPLACE, windowID, wmProtocolsReply->atom, 4, 32, 1, &wmDeleteReply->atom);

    //
    xcb_map_window(connPtr, windowID);
    xcb_flush(connPtr);
}

inline void WindowXCB::run()
{
    bool running = true;
    xcb_generic_event_t* event;
    xcb_client_message_event_t* cm;

    uint64_t frame = 0;
    while (running)
    {
        frame++;
        event = xcb_wait_for_event(connPtr);

        switch (event->response_type & ~0x80) {
        case XCB_CLIENT_MESSAGE: {
            cm = (xcb_client_message_event_t*)event;

            if (cm->data.data32[0] == wmDeleteWin) {
                running = false;
                break;
            }
        }
        case XCB_KEY_PRESS:
            std::cout << "frame " << frame << ":" << "KEY PRESS" << std::endl;
            break;
        case XCB_BUTTON_PRESS:
            std::cout << "frame " << frame << ":" << "BUTTON PRESS" << std::endl;
            if (((xcb_button_press_event_t*)event)->detail == XCB_BUTTON_INDEX_2)
                running = false;
            break;
        case XCB_MOTION_NOTIFY:

            std::cout << "frame " << frame << ":" << "Mouse move" << std::endl;

            break;
        case XCB_EXPOSE:
            std::cout << "frame " << frame << ":" << "Exposure " << std::endl;
            break;

        default:
            break;
        }


    }

    free(event);
}

inline xcb_connection_t* WindowXCB::getConnPtr() { return connPtr; }

inline xcb_window_t WindowXCB::getWndID() { return windowID; }

inline WindowXCB::~WindowXcb()
{
    xcb_disconnect(connPtr);
}

inline bool WindowXCB::init()
{
    connPtr = xcb_connect(NULL, NULL);
    if (xcb_connection_has_error(connPtr)) {
        printf("Error openning display! Exiting XCBWindow ctor!\n");
        exit(1);
    }

    setupPtr = xcb_get_setup(connPtr);
    screenPtr = xcb_setup_roots_iterator(setupPtr).data;

    windowID = xcb_generate_id(connPtr);
    parentID = screenPtr->root;
    rootVisual = screenPtr->root_visual;

    classType = xcb_window_class_t::XCB_WINDOW_CLASS_INPUT_OUTPUT;

    propName = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    propval[0] = screenPtr->black_pixel;
    propval[1] = XCB_EVENT_MASK_BUTTON_MOTION |
        XCB_EVENT_MASK_BUTTON_PRESS |
        XCB_EVENT_MASK_POINTER_MOTION |
        XCB_EVENT_MASK_KEY_PRESS |
        XCB_EVENT_MASK_KEYMAP_STATE |
        XCB_EVENT_MASK_NO_EVENT |

        XCB_EVENT_MASK_EXPOSURE;

    screenWidth = screenPtr->width_in_pixels;
    screenHeight = screenPtr->height_in_pixels;

    windowWidth = screenWidth / 2;
    windowHeight = screenHeight / 2;

    xPos = screenWidth / 5;
    yPos = screenHeight / 5;


    return true;
}

inline void WindowXCB::create_window()
{
    xcb_create_window(connPtr,
        screenPtr->root_depth,
        windowID,
        parentID,
        xPos,
        yPos,
        windowWidth,
        windowHeight,
        1,
        classType,
        rootVisual,
        propName,
        &propval
    );

    return;
}

inline bool WindowXCB::createContext() {

    uint32_t valueMask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;

    uint32_t value_list[2];
    value_list[0] = screenPtr->black_pixel;
    value_list[1] = 0;

    xcb_create_gc(connPtr, contextID, drawable, valueMask, value_list);

}

#endif // __linux__