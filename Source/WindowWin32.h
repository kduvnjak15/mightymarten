#pragma once
#include <Windows.h>
#include <vector>
#include <windef.h>
#include "BB_Window.h"
//
//BOOL WINAPI AllocConsole(void);
//

class WindowWin32 : public BB_Window
{
public: 
	WindowWin32();

    void run();
    void init();

#ifdef _WIN32
    void* getInstanceHandle() { return &hinstance_; }
    uint32_t* getWindowHandle() { return reinterpret_cast<uint32_t*>(&wndHandle_); }
#else 
    virtual void* getConnectionPtr() { return hinstance_; }
    virtual uint32_t getWindowID() {
        return 0;
    }

#endif
	virtual ~WindowWin32();

private: 
    void registerClass();
    void createWindow();

private:
    HINSTANCE hinstance_;
    HWND wndHandle_;

   /* const xcb_setup_t* setupPtr;
    xcb_screen_t* screenPtr;

    xcb_window_t parentID;
    uint32_t rootVisual;

    xcb_gcontext_t contextID;
    xcb_drawable_t drawable;

    xcb_window_class_t classType;*/

    uint32_t propName;
    uint32_t propval[2];

    //
    uint16_t xPos;
    uint16_t yPos;
    uint16_t screenWidth;
    uint16_t screenHeight;
    uint16_t windowWidth;
    uint16_t windowHeight;
};

