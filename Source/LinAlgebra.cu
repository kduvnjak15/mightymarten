 #pragma once
#include <chrono>
#include <math.h>
#include <stdexcept>
using namespace std;
using namespace std::chrono;

#include "LinAlgebra.h"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#define ALLOCATION_FACTOR 1.5
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <algorithm>

using namespace std;

__global__ void disp(double* res){
    uint rowNum = blockDim.x;
    uint WIDTH  = blockDim.y;
    uint colNum = blockDim.z;
    uint row = threadIdx.x;
    uint k = threadIdx.y;
    uint col = threadIdx.z;
//    printf("res %f \n", res[colNum*row + col]);

}
__global__ void matrixMultiplication(double *a, double *b, double *res) {

//  printf("rowNum %d width %d; colNum %d, th.x=%d, th.y=%d, th.z= %d \n", blockDim.x, blockDim.y, blockDim.z, threadIdx.x, threadIdx.y, threadIdx.z);

  uint rowNum = blockDim.x;
  uint WIDTH  = blockDim.y;
  uint colNum = blockDim.z;
  uint row = threadIdx.x;
  uint k = threadIdx.y;
  uint col = threadIdx.z;

//  printf("row=%d, k=%d, col= %d id = %f \n", threadIdx.x, threadIdx.y, threadIdx.z, a[row * WIDTH + k] * b[rowNum * k + col]);

//  res[colNum * row + col ] += a[row * WIDTH + k] * b[rowNum * k + col];
//  res[colNum * row + col ] = a[row * WIDTH + k] * b[rowNum * k + col];
  res[row * (WIDTH * colNum) + k + col * WIDTH]  = a[row * WIDTH + k] * b[colNum * k + col];
//  printf("res %f \n", res[colNum*row + col]);


}

template <class T>
BB_nVec<T>::BB_nVec() : size_(0), capacity_(0), data_(nullptr) {
  LOG(cout << this << " -> BB_nVec<T> default ctor" << endl;)
}

template <class T>
BB_nVec<T>::BB_nVec(uint size) : size_(size), data_(nullptr) {
  this->capacity_ = (size + 1) * ALLOCATION_FACTOR;
  LOG(cout << this << " -> BB_nVec<T>::parameter ctor(size) " << size << endl;)
  this->allocate();
  LOG(cout << this << " -> BB_nVec<T>::parameter ctor(size) end " << size << endl;)
}

template <class T>
BB_nVec<T>::BB_nVec(uint size, uint capacity)
    : size_(size), capacity_(capacity), data_(nullptr) {
  LOG(cout << this << " -> BB_nVec<T>::parameter ctor(size,capacity) " << size << endl;)
  this->allocate();
  LOG(cout << this << " -> BB_nVec<T>::parameter ctor(size,capacity) end " << size << endl;)
}

template <class T> BB_nVec<T>::BB_nVec(const BB_nVec<T> &obj) {
  LOG(cout << this << " -> BB_nVec<T> copy ctor " << endl;)
  this->size_ = obj.size_;
  this->capacity_ = obj.capacity_;
  this->data_ = nullptr;

  this->allocate();
  for (uint i = 0; i < this->size_; ++i) {
    *(this->data_ + i) = obj[i];
  }
  LOG(cout << this << " -> BB_nVec<T> copy ctor end " << size_ << endl;)
}

template <class T> BB_nVec<T>::BB_nVec(BB_nVec<T> &&obj) {
  LOG(cout << this << " -> BB_nVec<T> move ctor " << endl;)
  this->size_ = obj.size_;
  this->capacity_ = obj.capacity_;
  this->data_ = obj.data_;

  obj.size_ = 0;
  obj.capacity_ = 0;
  obj.data_ = nullptr;

  LOG(cout << this << " -> BB_nVec<T> move ctor end " << size_ << endl;)
}

template <class T> BB_nVec<T>::~BB_nVec() {
  for (int i = 0; i < size_; i++) {
    (data_ + i)->~T();
  }
  size_ = 0;
  capacity_ = 0;

  if (this->data_ != nullptr)
    free(this->data_);
  LOG(cout << this << " -> BB_nVec<T> ~dtor  " << endl;)
}

template <class T>
BB_nVec<T>::BB_nVec(std::initializer_list<T> &initList)
    : BB_nVec<T>(initList.size()) {
  LOG(cout << this << " -> initlist ctor " << endl;)
  uint i = 0;
  for (auto it = initList.begin(); it != initList.end(); it++) {
    *(data_ + i) = *it;
    i++;
  }
  LOG(cout << this << " -> initlist ctor end" << endl;)

  std::move(*this);
}

template <class T> BB_nVec<T> BB_nVec<T>::operator=(BB_nVec<T> &&moveObj) {
  LOG(cout << this << " -> move assignment operator" << endl;)
}

template <class T> BB_nVec<T> BB_nVec<T>::operator=(BB_nVec<T> &copyObj) {
  LOG(cout << this << " -> copy assignment operator" << endl;)
}

template <class T>
BB_nVec<T> &BB_nVec<T>::operator=(std::initializer_list<T> initList) {
  LOG(cout << this << " -> initializer list assignment operator " << endl;)
  this->size_ = initList.size();
  this->capacity_ = (this->size_ + 1) * ALLOCATION_FACTOR;
  this->allocate();

  uint i = 0;
  for (auto it = initList.begin(); it != initList.end(); it++) {
    *(data_ + i) = *it;
    i++;
  }
  LOG(cout << this << " -> initializer list assignment operator end " << endl;)
}

/// Brief description.
/** Detailed description. */
template <class T> uint BB_nVec<T>::size() const { return size_; }

template <class T> T &BB_nVec<T>::at(uint pos) const {
  if (pos > this->size_ - 1)
    throw std::runtime_error("BB_nVec<T>.at() Bigger index than size");

  return *(data_ + pos);
}

template <class T> void BB_nVec<T>::init(T &&initVal) {
  for (int i = 0; i < this->size_; i++) {
    this->at(i) = initVal;
  }
}

template <class T> T &BB_nVec<T>::operator[](uint pos) const {
  return *(data_ + pos);
}

template <class T> T *BB_nVec<T>::first() { return (data_); }

template <class T> T *BB_nVec<T>::last() { return (data_ + size_ - 1); }

template <class T> void BB_nVec<T>::push_back(T &obj) {
  if (size_ == capacity_){
    this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
    reallocate();
  }

  new (data_ + size_) T();
  *(data_ + size_) = obj;
  this->size_++;
  //    cout << "classic" << endl;
}

template <class T> void BB_nVec<T>::push_back(T &&obj) {
  if (size_ == capacity_) {
    this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
    reallocate();
  }

  new (data_ + size_) T();
  *(data_ + size_) = std::move(obj);
  this->size_++;
  //    cout << "rvalueref" << endl;
}

template <class T> void BB_nVec<T>::clear() {
  if (size_ == 0)
    return;

  while (size_ > 0) {
    this->pop_back();
  }
}

template <class T> void BB_nVec<T>::resize(uint size) {
  this->size_ = size;
  this->capacity_ = (this->size_ + 1) * ALLOCATION_FACTOR;
  this->allocate();
}

template <class T> void BB_nVec<T>::pop_back() {
  if (size_ == 0)
    return;

  (data_ + size_ - 1)->~T();
  size_--;
}

template <class T> void BB_nVec<T>::allocate() {

  LOG(cout << this << " -> allocate size " << this->size_ << ", capacity "
       << this->capacity_ << endl;)

  if (this->data_ != nullptr)
    free(this->data_);

  this->data_ = static_cast<T *>(malloc(this->capacity_ * sizeof(T)));
  for (int i = 0; i < this->size_; i++) {
    new (this->data_ + i) T();
  }
}

template <class T> void BB_nVec<T>::reallocate() {
  LOG(cout << this << " -> reallocate size " << this->size_ << ", capacity "
       << this->capacity_ << endl;)
//  this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
  T *newData = static_cast<T *>(malloc(capacity_ * sizeof(T)));

  for (int i = 0; i < size_; i++) {
    new (newData + i) T();
    *(newData + i) = *(this->data_ + i);
  }

  for (int i = 0; i < size_; i++) {
    (this->data_ + i)->~T();
  }

  if (this->data_)
    free(this->data_);

  data_ = newData;
}

template <class T> void BB_nVec<T>::insert(uint pos, T &&obj) {

  if (size_ + 1 >= capacity_){
    this->capacity_ = static_cast<uint>((size_ + 1) * ALLOCATION_FACTOR);
    reallocate();
  }

  if (size_ > 0) {
    for (uint i = size_; i >= pos; i--) {
      *(data_ + i) = *(data_ + i - 1);
    }
  }
  *(data_ + pos) = std::move(obj);

  this->size_++;
}

template<class T>
void BB_nVec<T>::sort(){
    std::sort(this->data_, this->data_ + this->size_);
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

BB_dVec::BB_dVec() : BB_nVec<double>() {
  LOG(cout << this << " -> BB_dVec default ctor " << endl;)
}

BB_dVec::BB_dVec(uint size) : BB_nVec<double>(size) {
  LOG(cout << this << " -> BB_dVec param ctor " << endl;)
}

BB_dVec::BB_dVec(uint size, uint capacity) : BB_nVec<double>(size, capacity) {
  LOG(cout << this << " -> BB_dVec param ctor " << endl;)
}

BB_dVec::BB_dVec(const BB_dVec &copyObj) : BB_nVec<double>(copyObj) {

  LOG(cout << this << " -> BB_dVec copyCtor" << endl;)
}

/// Brief description.
/** Detailed description. */
BB_dVec& BB_dVec::operator=(const BB_dVec &copyObj) {

  if (this == &copyObj) return *this;

  if (this->size_ != copyObj.size() || this->capacity_ != this->size_){
      this->size_ = copyObj.size();
      this->capacity_ = this->size_;

      this->reallocate();
  }

  for (uint i = 0; i < this->size(); i++) {
    (*this)[i] = copyObj[i];
  }

  LOG(cout << this << " -> BB_dVec copy assignment operator" << endl;)
  return *this;
}

BB_dVec::BB_dVec(BB_dVec &&moveObj) : BB_nVec<double>(std::move(moveObj)) {
  LOG(cout << this << " -> BB_dVec move ctor" << endl;)
}

void BB_dVec::operator=(BB_dVec &&moveObj) {
  LOG(cout << this << " -> BB_dVec move assignment operator on " << this << " for "
       << &moveObj << endl;)
  this->data_ = moveObj.data_;
  this->size_ = moveObj.size();
  this->capacity_ = moveObj.capacity_;

  moveObj.data_ = nullptr;
  moveObj.size_ = 0;
  moveObj.capacity_ = 0;
}

BB_dVec::BB_dVec(std::initializer_list<double> &&initList)
    : BB_nVec<double>(initList) {
  LOG(cout << this << " -> BB_dVec initList size " << this->size() << endl;)
}

void BB_dVec::operator=(std::initializer_list<double> &initlist) {
  LOG(cout << "initlist assign " << endl;)
  this->resize(initlist.size());
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] = *(initlist.begin() + i);
  }
}


BB_dVec::BB_dVec(const VecDisp& obj):BB_dVec(obj.num_){
    LOGNOW(cout << this << " -> BB_dVec (const RowObj)");
    std::copy(obj.posPtr_,obj.posPtr_+obj.num_,this->data_);
}


BB_dVec& BB_dVec::operator=(const VecDisp& obj){
    LOGNOW(cout << "BB_dVec::operator=(const RowObj)");

    if (this->size() != obj.num_) throw std::runtime_error("Incompatible BB_dVec and RowObj");
    cout << this->data_ << endl;

    std::copy(obj.posPtr_, obj.posPtr_ + obj.num_, this->data_);

    return *this;
}


BB_dVec::~BB_dVec() {
  if (this->data_ != nullptr)
    free(this->data_);
  this->data_ = nullptr;
  LOG(cout << this << " -> BB_dVec default ~dtor " << endl;)
}

BB_dVec BB_dVec::operator+(const BB_dVec &operand) {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  cout << "operator + start for " << this << endl;
  BB_dVec tmpVec(*this);
  cout << "operator + temp " << &tmpVec << endl;
  cout << "operator+ : " << this << " + " << &tmpVec << endl;
  for (int i = 0; i < tmpVec.size(); i++) {
    tmpVec[i] += operand[i];
  }

  return tmpVec;
}

BB_dVec BB_dVec::operator-(const BB_dVec &operand) {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  cout << "operator - start for " << this << endl;
  BB_dVec tmpVec(*this);
  cout << "operator - temp " << &tmpVec << endl;
  cout << "operator- : " << this << " - " << &tmpVec << endl;
  for (int i = 0; i < tmpVec.size(); i++) {
    tmpVec[i] -= operand[i];
  }

  return std::move(tmpVec);
}

BB_dVec &BB_dVec::operator+=(const BB_dVec &operand) {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  LOG(cout << this << " -> BB_dVec::operator+= " << endl;)
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] += operand[i];
  }
  LOG(cout << this << " -> BB_dVec::operator+= end" << endl;)
  return (*this);
}

BB_dVec &BB_dVec::operator-=(const BB_dVec &operand) {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  LOG(cout << this << " -> BB_dVec::operator-= " << endl;)
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] -= operand[i];
  }
  LOG(cout << this << " -> BB_dVec::operator-= end" << endl;)
  return (*this);
}

double BB_dVec::operator*(const BB_dVec &operand) {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  LOG(cout << "dotProduct " << this << " * " << &operand << endl;)
  double dotProduct = 0.0;
  for (uint i = 0; i < this->size(); i++) {
    dotProduct += (*this)[i] * operand[i];
  }
  return dotProduct;
}

BB_dVec BB_dVec::operator*(const double scalar) {
  BB_dVec tmp(*this);
  for (int i = 0; i < this->size(); i++) {
    tmp[i] *= scalar;
  }
  return std::move(tmp);
}

BB_dVec& BB_dVec::operator*=(const double scalar) {

  for (int i = 0; i < this->size(); i++) {
    (*this)[i] *= scalar;
  }
  return *this;
}

BB_dVec operator*(const double scalar, BB_dVec &operand) {

  BB_dVec tmp(operand);
  for (int i = 0; i < operand.size(); i++) {
    tmp[i] *= scalar;
  }
  return std::move(tmp);
}

BB_dVec BB_dVec::operator/(const double scalar) {
  BB_dVec tmp(*this);
  for (int i = 0; i < this->size(); i++) {
    tmp[i] /= scalar;
  }
  return std::move(tmp);
}

BB_dVec operator/(double scalar, BB_dVec &operand) {
  BB_dVec tmp(operand);
  for (int i = 0; i < operand.size(); i++) {
    tmp[i] /= scalar;
  }
  return std::move(tmp);
}

bool BB_dVec::operator==(const BB_dVec& obj){
    if (this == &obj) return true;

    if (std::equal(obj.data(), obj.data() + obj.size(),this->data())) return true;

    return false;
}




double BB_dVec::length() { return sqrt((*this) * (*this)); }

void BB_dVec::weight() {
  cout << "num: " << size_ << endl;
  cout << "sizeof double " << sizeof(double) << endl;
  cout << "weight " << sizeof(*this) << endl;
  cout << (size_ * sizeof(double *) + sizeof(uint) * 2 == sizeof(*this))
       << endl;
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

BB_Vec2::BB_Vec2() : BB_dVec(2, 2), x(*(this->data_)), y(*(this->data_ + 1)) {
  LOG(cout << this << " -> BB_Vec2 default ctor" << endl;)
}

BB_Vec2::BB_Vec2(double x, double y) : BB_Vec2() {
  *(this->data_) = x;
  *(this->data_ + 1) = y;
  LOG(cout << this << " -> (x, y) parameter ctor" << endl;)
}

BB_Vec3::BB_Vec3()
    : BB_dVec(3, 3), x(*(this->data_)), y(*(this->data_ + 1)),
      z(*(this->data_ + 2)) {
  LOG(cout << this << " -> BB_Vec3 default ctor" << endl;)
}

BB_Vec3::BB_Vec3(double x, double y, double z) : BB_Vec3() {
  this->x = x;
  this->y = y;
  this->z = z;
  LOG(cout << this << " -> (x, y, z) parameter ctor" << endl;)
}

BB_Vec4::BB_Vec4()
    : BB_dVec(4, 4), x(*(this->data_)), y(*(this->data_ + 1)),
      z(*(this->data_ + 2)), w(*(this->data_ + 3)) {
  LOG(cout << this << " -> BB_Vec4 default ctor" << endl;)
}

BB_Vec4::BB_Vec4(double x, double y, double z, double w) : BB_Vec4() {
  this->x = x;
  this->y = y;
  this->z = z;
  this->w = w;
  LOG(cout << this << " -> (x, y, z, w) parameter ctor ctor" << endl;)
}

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

BB_Matrix::BB_Matrix() : BB_Matrix(0, 0) {
    LOG(cout << this << " - > BB_Matrix default ctor" << endl;)
}

BB_Matrix::BB_Matrix(uint row, uint col)
    : row_(row), col_(col), dataVec_(row * col, row * col), dispObj_(new VecDisp) {

  LOG(cout << this << " - > BB_Matrix parametrized (row, col) ctor" << endl;)
}

BB_Matrix::BB_Matrix(const BB_Matrix &copyObj)
    : row_(copyObj.row_), col_(copyObj.col_), dataVec_(copyObj.dataVec_) {
    LOG(cout << this << " -> BB_Matrix copy ctor" << endl;)
    operator=(copyObj);
    LOG(cout << this << " -> BB_Matrix copy ctor end" << endl;)
}

BB_Matrix& BB_Matrix::operator=(const BB_Matrix& copyObj){

    LOG( cout << this << " -> BB_Matrix::copy assignment operator" << endl;)
    this->row_ = copyObj.rows();
    this->col_ = copyObj.cols();
    this->dataVec_ = copyObj.dataVec_;

    if (!this->dispObj_) this->dispObj_ = new VecDisp;

    LOG( cout << this << " -> BB_Matrix::copy assignment operator end" << endl;)
    return *this;
}

BB_Matrix::BB_Matrix(BB_Matrix &&moveObj) {
    LOG( cout << this << " -> BB_Matrix::move Ctor" << endl;)
    operator=(std::move(moveObj));
    LOG( cout << this << " -> BB_Matrix::move Ctor end" << endl;)
}

BB_Matrix& BB_Matrix::operator=(BB_Matrix&& moveObj){
    LOG( cout << this << " -> BB_Matrix::move assignment " << endl;)
    this->row_ = moveObj.rows();
    this->col_ = moveObj.cols();

    this->dispObj_ = moveObj.dispObj_;

    this->dataVec_ = std::move(moveObj.dataVec_);

    moveObj.row_ = 0;
    moveObj.col_ = 0;
    moveObj.dispObj_ = nullptr;

    return *this;
    LOG( cout << this << " -> BB_Matrix::move assignment end" << endl;)
}

BB_Matrix::~BB_Matrix() {
    LOG(cout << this << " -> BB_Matrix ~dtor" << endl;)

    delete dispObj_;
    LOGNOW(cout << this << " -> BB_Matrix ~dtor" << endl;)

}

uint BB_Matrix::rows() const { if (transpose_) return this->col_; else return this->row_; }

uint BB_Matrix::cols() const { if (transpose_) return this->row_; else return this->col_; }

void BB_Matrix::randomize(){

    srand(time(NULL));

    for (uint i = 0; i < this->dataVec_.size(); i++){
        this->dataVec_[i] = rand() % 100;
    }
}

void BB_Matrix::zeros(){
    dataVec_.init(0);
}

BB_Matrix BB_Matrix::operator*(const BB_Matrix &obj) {
  LOG(cout << this << " -> BB_Matrix::operator* " << &obj << endl;)

  BB_Matrix newMat(this->rows(), obj.cols());
#ifdef GPU
  double *d_a;
  double *d_b;
  double *d_res;

  uint width = this->cols();
  dim3 threads(this->rows(), width, obj.cols());

  cudaMalloc((void **)(&d_a), this->numOfElems() * sizeof(double));
  cudaMalloc((void **)(&d_b), obj.numOfElems() * sizeof(double));
  cudaMalloc((void **)(&d_res),  threads.x * threads.y * threads.z * sizeof(double));

  cudaMemcpy((void *)d_a, (const void *)this->data(),
             this->numOfElems() * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy((void *)d_b, (const void *)obj.data(),
             obj.numOfElems() * sizeof(double), cudaMemcpyHostToDevice);

  matrixMultiplication<<<1,threads>>>(d_a, d_b, d_res);
  cudaDeviceSynchronize();

  BB_dVec tmpData(this->rows() * this->cols());

  double tt[threads.x * threads.y * threads.z];
  cudaError err = cudaMemcpy((void *)tt, (const void *)d_res,
             threads.x * threads.y * threads.z* sizeof(double), cudaMemcpyDeviceToHost);



  if (err != cudaSuccess) cout << "jedifovna" << endl;
  cout << threads.x * threads.y * threads.z << endl;
  for (int i = 0; i<threads.x * threads.y * threads.z; i++){
//        cout << "OF i = " << i << " == " << tt[i] << ", (" << i / (this->cols() * obj.cols()) <<", " <<  (i % (this->cols() * obj.cols())) / this->cols() << ")"<< endl;


        newMat[(i / (this->cols() * obj.cols()))][ (i % (this->cols() * obj.cols() )) / this->cols() ] += tt[i];
  }

#else

  if (obj.rows() != this->cols())
      std::runtime_error("Incompatible matrices for multiplication");

  for (uint i = 0; i < this->rows(); i++){
      for(uint j = 0; j < obj.cols(); j++){
          double tmpSum = 0.0;
          for(uint k = 0; k<this->cols(); ++k){
              tmpSum += (*this)[i][k] * const_cast<BB_Matrix&>(obj)[k][j];
          }
          newMat[i][j] = tmpSum;
      }
  }

#endif
  return newMat;
}


BB_Matrix BB_Matrix::operator+(const BB_Matrix& sumand){
    LOG(std::cout << this << " -> BB_Matrix::operator+" <<endl;)

    if(this->rows() != sumand.rows() || this->cols() != sumand.cols())
        throw std::runtime_error("Incompatible matrix for operator + " );

    BB_Matrix tmp(*this);
    tmp.dataVec_ += sumand.dataVec_;

    LOG(std::cout << this << " -> BB_Matrix::operator+ end" <<endl;)
    return tmp;
}

BB_Matrix& BB_Matrix::operator+=(const BB_Matrix& sumand){
    LOG(std::cout << this << " -> BB_Matrix::operator+=" <<endl;)

    if(this->rows() != sumand.rows() || this->cols() != sumand.cols())
        throw std::runtime_error("Incompatible matrix for operator + " );

    this->dataVec_ += sumand.dataVec_;

    LOG(std::cout << this << " -> BB_Matrix::operator+= end" <<endl;)
    return *this;
}

BB_Matrix BB_Matrix::operator-(const BB_Matrix& sumand){
    LOG(std::cout << this << " -> BB_Matrix::operator+" <<endl;)

    if(this->rows() != sumand.rows() || this->cols() != sumand.cols())
        throw std::runtime_error("Incompatible matrix for operator + " );

    BB_Matrix tmp(*this);
    tmp.dataVec_ -= sumand.dataVec_;

    LOG(std::cout << this << " -> BB_Matrix::operator+ end" <<endl;)
    return tmp;
}

BB_Matrix& BB_Matrix::operator-=(const BB_Matrix& sumand){
    LOG(std::cout << this << " -> BB_Matrix::operator+=" <<endl;)

    if(this->rows() != sumand.rows() || this->cols() != sumand.cols())
        throw std::runtime_error("Incompatible matrix for operator + " );

    this->dataVec_ -= sumand.dataVec_;

    LOG(std::cout << this << " -> BB_Matrix::operator+= end" <<endl;)
    return *this;
}
BB_Matrix BB_Matrix::operator*(const double scalar) const {
    LOG(std::cout << this << " -> const BB_Matrix::operator*scalar" <<endl;)
    BB_Matrix tmp(*this);
    tmp.dataVec_ *= scalar;
    LOG(std::cout << this << " -> const BB_Matrix::operator*scalar end" <<endl;)
    return tmp;
}

BB_Matrix BB_Matrix::operator/(const double scalar) const {
    LOG(std::cout << this << " -> const BB_Matrix::operator/scalar" <<endl;)
    BB_Matrix tmp(*this);
    tmp.dataVec_ *= 1/scalar;
    LOG(std::cout << this << " -> const BB_Matrix::operatorscalar end" <<endl;)
    return tmp;
}

bool BB_Matrix::operator==(const BB_Matrix& obj){
    if (this == &obj) return true;

    if (std::equal(this->data(), this->data()+this->dataVec_.size() , obj.data()) && this->rows() == obj.rows())
        return true;
    else
        return false;
}

BB_Matrix BB_Matrix::operator~(){
    BB_Matrix tmp(*this);
    tmp.transpose_ = !tmp.transpose_;

    return tmp;
}

VecDisp& BB_Matrix::operator[](uint index) const {

    if (!transpose_){
        if (this->row_ - 1 < index){
            throw std::runtime_error("BB_Matrix has got less rows than requested[]");
        }

        dispObj_->posPtr_ = const_cast<double*>(this->dataVec_.data() + this->cols() * index);
        dispObj_->num_    = this->cols();
        dispObj_->stride_ = 1;
    }
    else {
        if (this->col_ - 1 < index)
          throw std::runtime_error("BB_Matrix has got less rows than requested()");
        dispObj_->posPtr_ = const_cast<double*>(this->dataVec_.data() + index);
        dispObj_->num_    = this->rows();
        dispObj_->stride_ = this->cols();
    }
    return *dispObj_;
}

VecDisp& BB_Matrix::operator()(uint index) const {

    if (!transpose_){
        if (this->col_ - 1 < index)
          throw std::runtime_error("BB_Matrix has got less rows than requested()");

        dispObj_->posPtr_ = const_cast<double*>(this->dataVec_.data() + index);
        dispObj_->num_    = this->rows();
        dispObj_->stride_ = this->cols();
    }
    else {
        if (this->row_ - 1 < index){
            throw std::runtime_error("BB_Matrix has got less rows than requested[]");
        }

        dispObj_->posPtr_ = const_cast<double*>(this->dataVec_.data() + this->cols() * index);
        dispObj_->num_    = this->cols();
        dispObj_->stride_ = 1;
    }

    return *dispObj_;
}

BB_Matrix::BB_Matrix(std::initializer_list<initializer_list<double> >&& obj):
    BB_Matrix(obj.size(), obj.begin()->size())
{
    LOG( cout <<this << "-> BB_Matrix(initializer_list<initializer_list<double>>) ctor "<< endl;)

    uint row = 0;
    for (auto it = obj.begin(); it!=obj.end(); it++) {
        uint col = 0;
        if (obj.begin()->size() != it->size()) throw std::runtime_error("Init list matrix obj has nonuniform cols num");

        for(auto cit = it->begin(); cit!=it->end(); cit++){
            (*this)[row][col] = *cit;
            col++;
        }
        row++;
    }

    LOG( cout <<this << "-> BB_Matrix(initializer_list<initializer_list<double>>) ctor end"<< endl;)
}

BB_Matrix BB_Matrix::operator=(std::initializer_list<initializer_list<double> >&& obj){
    LOG( cout <<this << "-> BB_Matrix(initializer_list<initializer_list<double>>) ctor "<< endl;)

    BB_Matrix tmp(obj.size(), obj.begin()->size());

    uint row = 0;
    for (auto it = obj.begin(); it!=obj.end(); it++) {
        uint col = 0;
        if (obj.begin()->size() != it->size()) throw std::runtime_error("Init list matrix obj has nonuniform cols num");

        for(auto cit = it->begin(); cit!=it->end(); cit++){
            tmp[row][col] = *cit;
            col++;
        }
        row++;
    }

    *this = std::move(tmp);

    return *this;
    LOG( cout <<this << "-> BB_Matrix(initializer_list<initializer_list<double>>) ctor end"<< endl;)
}

BB_Matrix::BB_Matrix(std::initializer_list<BB_dVec>&& obj)
    : row_(obj.size()), col_(obj.begin()->size()), dataVec_(row_ * col_),
      dispObj_(new VecDisp){
    LOG( cout <<this << "-> BB_Matrix(initializer_list<BB_dVec>) ctor "<< endl;)

    uint row = 0;
    for (auto it = obj.begin(); it!=obj.end(); it++){
        if ( it->size() != this->cols()) throw std::runtime_error("Init list matrix obj has nonuniform cols num");

        for (uint j = 0; j < this->cols(); j++ ){
            this->dataVec_[row* this->cols() + j] = *(it->data() + j);
        }
        row++;
    }
    LOG( cout <<this << "-> BB_Matrix(initializer_list<BB_dVec>) ctor end "<< endl;)
}

BB_Matrix BB_Matrix::operator=(std::initializer_list<BB_dVec>&& obj){
    LOG( cout <<this << "-> BB_Matrix::operator=(initializer_list<BB_dVec>) ctor "<< endl;)

    uint rows = obj.size();
    uint cols = obj.begin()->size();

    cout << "uno " << rows << " < " << cols << endl;

    BB_Matrix tmp(rows,cols);

    uint row = 0;
    for (auto it = obj.begin(); it!=obj.end(); it++){
        if ( it->size() != cols) throw std::runtime_error("Init list matrix obj has nonuniform cols num");

        for (uint j = 0; j < cols; j++ ){
            tmp[row][j] = *(it->data() + j);
        }
        row++;
    }

    *this = std::move(tmp);

    LOG( cout <<this << "-> BB_Matrix::operator=(initializer_list<BB_dVec>) ctor end "<< endl;)
    return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

BB_SquareMatrix::BB_SquareMatrix(): BB_Matrix (0,0)
{
    LOG(cout << this << " -> BB_SquareMatrix default ctor");
}

BB_SquareMatrix::BB_SquareMatrix(uint size): BB_Matrix (size, size)
{
    LOG(cout << this << " -> BB_SquareMatrix parametrized ctor");
}

BB_SquareMatrix::BB_SquareMatrix(const BB_SquareMatrix& obj): BB_Matrix (obj)
{
    LOG(cout << this << " -> BB_SquareMatrix copy ctor");
}

BB_SquareMatrix& BB_SquareMatrix::operator=(const BB_SquareMatrix& obj){
    *this = obj;
    return *this;
    LOG(cout << this << " -> BB_SquareMatrix copy ctor");
}


BB_SquareMatrix::BB_SquareMatrix(BB_SquareMatrix&& obj):
    BB_Matrix(obj)
{
    LOG(cout << this << " -> BB_SquareMatrix move ctor");
}

BB_SquareMatrix& BB_SquareMatrix::operator=(BB_SquareMatrix&& obj){
    *this = obj;
    return *this;
    LOG(cout << this << " -> BB_SquareMatrix move ctor");
}

//BB_SquareMatrix::BB_SquareMatrix(std::initializer_list<initializer_list<double> >&& obj) {
//    LOG(cout << this << " -> BB_SquareMatrix initializer_list<initiliazer_list ctor");
//}

//BB_SquareMatrix BB_SquareMatrix::operator=(std::initializer_list<initializer_list<double> >&& obj){
//    LOG(cout << this << " -> BB_SquareMatrix move ctor");
//}

//BB_SquareMatrix::BB_SquareMatrix(std::initializer_list<BB_dVec> &&obj)
//{
//this->BB_Matrix(obj);
//}
//BB_SquareMatrix BB_SquareMatrix::operator=(std::initializer_list<BB_dVec>&& obj){

//}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

BB_SymMatrix::BB_SymMatrix(): BB_SquareMatrix (0){
    LOG(cout << this << " -> BB_SymMatrix default ctor " << endl;)
}

BB_SymMatrix::BB_SymMatrix(uint size)
    : BB_SquareMatrix (0)
{
    this->row_ = size;
    this->col_ = size;

    this->dataVec_.resize(size*(size+1)/2);

    LOG(cout << this << " -> BB_SymMatrix default ctor " << endl;)
}



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////


// concrete instatiation
template class BB_nVec<double>;
template class BB_nVec<double *>;
template class BB_nVec<A>;

