#include "APP.h"

void APP::run()
{
	window.run();
}

void APP::init()
{
#ifdef _WIN32
  vkApi.reinitialize(window.getInstanceHandle(), *window.getWindowHandle());

#else
	vkApi.reinitialize(window.getConnectionPtr(), window.getWindowID());
#endif
}
