#pragma  once
#include "WindowWin32.h"
#include <assert.h>

#include <iostream>

WindowWin32::WindowWin32()
{
	init();
	registerClass();
	createWindow();

	if (!this->wndHandle_)
		printf("Failed to create window");
}

void WindowWin32::run()
{
	ShowWindow(wndHandle_, SW_SHOW);
	SetForegroundWindow(wndHandle_);
	SetFocus(wndHandle_);

	MSG message;

	while (GetMessage(&message, NULL, 0, 0)) {
		TranslateMessage(&message);
		DispatchMessage(&message);
	}

	return;
}

WindowWin32::~WindowWin32()
{
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	switch (message) {
	case WM_DESTROY:
		DestroyWindow(hWnd);
		PostQuitMessage(0);
		break;
	case WM_PAINT:
		ValidateRect(hWnd, NULL);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}

}

void WindowWin32::registerClass()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEXW);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hinstance_;
	wcex.hIcon = LoadIcon(hinstance_, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = "JAIHO";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	static ATOM atom = ::RegisterClassEx(&wcex);
	assert(atom > 0);
}

void WindowWin32::createWindow()
{
	wndHandle_ = ::CreateWindowEx(NULL, "JAIHO", "AppName", WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		xPos, yPos, windowWidth, windowHeight, NULL, NULL, hinstance_, NULL);

	if (wndHandle_)
		std::cout << "KREJZI TREJN " << std::endl;
}

void WindowWin32::init()
{
	hinstance_ = GetModuleHandle(NULL);

	screenWidth = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	windowWidth = screenWidth / 2;
	windowHeight = screenHeight / 2;
	
	xPos = screenWidth / 5;
	yPos = screenHeight / 5;

	return;
}
