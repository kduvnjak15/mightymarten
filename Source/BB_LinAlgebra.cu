#include "stdafx.h"

#include "BB_LinAlgebra.h"

#include <utility>
using namespace bb;



//////////////////////////////////////////////////////////////////////////////////////////
// dVec ctors
//////////////////////
dVec::dVec(){
    LOG(std::cout << this << " -> dVec default ctor " << std::endl;)
}
dVec::dVec(uint dim){

    this->data().resize(dim);
    this->data().randomize();

    LOG(std::cout << this << " -> dVec param " << dim << " Ctor" << std::endl;)
}

dVec::dVec(const dVec& copyObj){

    this->data_ = copyObj.data();

    LOG(std::cout << this << " -> dVec copyCtor" << std::endl;)
}


dVec& dVec::operator=(const dVec &copyObj) {
  LOG(std::cout << this << " -> dVec copy assignment operator" << std::endl;)

  if (this == &copyObj)
    return *this;

  this->data_ = copyObj.data();

  return *this;
}

double& dVec::operator[](uint pos) {
    return this->data_.at(pos);
}

const double& dVec::operator[](uint pos) const {
    return this->data_.at(pos);
}

dVec::dVec(dVec &&moveObj)  {
  LOG(std::cout << this << " -> dVec move ctor " << std::endl;)

  this->data_ = std::move(moveObj.data_);

  LOG(std::cout << this << " -> dVec move ctor end" << std::endl;)
}

void dVec::operator=(dVec &&moveObj) {
  LOG(std::cout << this << " -> dVec move assignment operator on " << this
                << " for " << &moveObj << std::endl;)

   this->data_ = std::move(moveObj.data_);
}

const uint dVec::size() const { return this->data_.size(); }


std::ostream& operator<<(std::ostream &os, const dVec &obj) {
    if (obj.size() == 0) {
        os << "[ " << &obj << " is empty >" << std::endl;
    }
    else
    {
        os << "[ ";
        for (uint i = 0; i < obj.size() - 1; ++i) {
            os << *(obj.data().first() + i) << ", ";
        }

        os << *(obj.data().first() + obj.size() - 1);
        os << " ]";
    }
    return os;
}

dVec::dVec(std::initializer_list<double> &initList){
  this->data_ = initList;
  LOG(std::cout << this << " -> dVec initList size ref" << this->size()
                << std::endl;)
}

dVec::dVec(std::initializer_list<double> &&initList){
  this->data_ = initList;
  LOG(std::cout << this << " -> dVec initList size rval" << this->size()
                << std::endl;)
}

void dVec::operator=(std::initializer_list<double> &initlist) {

    for (auto item : initlist )
    {
        this->data_.push_back(item);

    }
  LOG(std::cout << "initlist assign " << std::endl;)
}

dVec::~dVec(){

    LOG(std::cout << this << " -> dVec default ~dtor " << std::endl;)
}

bb::nVec<double> &dVec::data() { return data_; }
const bb::nVec<double> &dVec::data() const { return data_; }

void dVec::push_back(double elem){

    this->data_.push_back(elem);

    return;
}


const double dVec::length() const { return std::sqrt((*this) * (*this)); }


//////////////////////////////////////////////////
////  operators
//////////////////////////////////////////////////
dVec dVec::operator+(const dVec &operand) const {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  dVec tmpVec(*this);
   for (int i = 0; i < tmpVec.size(); i++) {
    tmpVec[i] += operand[i];
  }

  return std::move(tmpVec);
}

dVec dVec::operator-(const dVec &operand) const{
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  std::cout << "operator - start for " << this << std::endl;
  dVec tmpVec(*this);
  std::cout << "operator - temp " << &tmpVec << std::endl;
  std::cout << "operator- : " << this << " - " << &tmpVec << std::endl;
  for (uint i = 0; i < tmpVec.size(); i++) {
    tmpVec[i] -= operand[i];
  }

  return std::move(tmpVec);
}

dVec& dVec::operator+=(const dVec &operand) {

  if (this->size() == 0)
  {
      this->data().resize( operand.size());
  }
  else
  {
      if (this->size() != operand.size())
          throw std::runtime_error("Incompatible sizes");
  }

  LOG(std::cout << this << " -> dVec::operator+= " << std::endl;)
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] += operand[i];
  }
  LOG(std::cout << this << " -> dVec::operator+= end" << std::endl;)
  return (*this);
}

dVec& dVec::operator-=(const dVec &operand) {
    if (this->size() == 0)
    {
        this->data().resize( operand.size());
    }
    else
    {
        if (this->size() != operand.size())
            throw std::runtime_error("Incompatible sizes");
    }

  LOG(std::cout << this << " -> dVec::operator-= " << std::endl;)
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] -= operand[i];
  }
  LOG(std::cout << this << " -> dVec::operator-= end" << std::endl;)
  return (*this);
}

double dVec::operator*(const dVec &operand) const {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  LOG(std::cout << "dotProduct " << this << " * " << &operand << std::endl;)
  double dotProduct = 0.0;
  for (uint i = 0; i < this->size(); i++) {
    dotProduct += (*this)[i] * operand[i];
  }
  return dotProduct;
}

dVec dVec::operator*(const double scalar) {
  dVec tmp(*this);
  for (int i = 0; i < this->size(); i++) {
    tmp[i] *= scalar;
  }
  return std::move(tmp);
}

dVec& dVec::operator*=(const double scalar) {


  for (int i = 0; i < this->size(); i++) {
    (*this)[i] *= scalar;
  }
  return *this;
}

bb::dVec operator*(double &scalar, dVec& operand){

}
bb::dVec operator*(const double scalar,const dVec& operand) {

  dVec tmp(operand);
  for (int i = 0; i < operand.size(); i++) {
    tmp[i] *= scalar;
  }
  return std::move(tmp);
}

const Vec2 Vec2::operator*(const double scalar) const
{
    bb::Vec2 tmpVec(*this);
    tmpVec.x() *= scalar;
    tmpVec.y() *= scalar;

    return std::move(tmpVec);
}

const bb::Vec2 Vec2::operator*(const uint scalar) const
{
    bb::Vec2 tmpVec(*this);
    tmpVec.x() *= scalar;
    tmpVec.y() *= scalar;

    return std::move(tmpVec);
}

dVec dVec::operator/(const double scalar) {
  dVec tmp(*this);
  for (int i = 0; i < this->size(); i++) {
    tmp[i] /= scalar;
  }
  return std::move(tmp);
}

dVec& dVec::operator/=(const double scalar) {

  for (int i = 0; i < this->size(); i++) {
    (*this)[i] /= scalar;
  }
  return *this;
}

bool dVec::operator==(const dVec &obj) {
  if (this == &obj)
    return true;

  if (std::equal(obj.data().first(), obj.data().first() + obj.size(), this->data().first()))
    return true;

  return false;
}

dVec dVec::operator!(){
    std::cout << "dVec::operator !  not implemented" <<std::endl;
}

void dVec::operator~(){
    this->transposed = !this->transposed;
    std::cout << "operator~()" << std::endl;
}

void dVec::normalize(){
    *this /= this->length();

    return;
}

dVec dVec::getNormalized() const {
  dVec tmp(*this);
  return tmp / this->length();
}


//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// dVec implementation
//////////////////////////////////////////////////////////////////////////////////////////
SquareMatrix::SquareMatrix(const SymMatrix &obj)
    : Matrix(obj.rows(), obj.cols()) {

  throw std::runtime_error("not implemented symMatrix -> SquareMatrix");
  //  for (uint i = 0; i < obj.cols(); i++) {
  //    for (uint j = i; j < obj.cols(); j++) {
  //      this->dataVec_[i * obj.cols() + j] = obj[i][j];
  //      this->dataVec_[j * obj.cols() + i] = obj[i][j];
  //    }
  //  }
}

Quaternion Mat3x3::quaternion() {

  double trace = this->trace();
  if (trace > 0) {
    double w = sqrt(1 + this->trace()) / 2.0;
    Quaternion tmp(w, (this->dataVec_[7] - this->dataVec_[5]) / (4 * w),
                   (this->dataVec_[2] - this->dataVec_[6]) / (4 * w),
                   (this->dataVec_[3] - this->dataVec_[1]) / (4 * w));
    return tmp;
  } else if (this->dataVec_[0] > this->dataVec_[4] &&
             this->dataVec_[0] > this->dataVec_[8]) {
    double s =
        sqrt(1 + this->dataVec_[0] - this->dataVec_[4] - this->dataVec_[8]) / 2;
    Quaternion tmp((this->dataVec_[7] - this->dataVec_[5]) / (4 * s), s,
                   (this->dataVec_[1] + this->dataVec_[3]) / (4 * s),
                   (this->dataVec_[2] + this->dataVec_[6]) / (4 * s));
    return tmp;

  } else if (this->dataVec_[4] > this->dataVec_[8]) {
    double s =
        sqrt(1 + this->dataVec_[4] - this->dataVec_[0] - this->dataVec_[8]) / 2;
    Quaternion tmp((this->dataVec_[2] - this->dataVec_[6]) / (4 * s),
                   (this->dataVec_[1] + this->dataVec_[3]) / (4 * s), s,
                   (this->dataVec_[5] + this->dataVec_[7]) / (4 * s));
    return tmp;
  } else {
    double s =
        sqrt(1 + this->dataVec_[8] - this->dataVec_[0] - this->dataVec_[4]) / 2;
    Quaternion tmp((this->dataVec_[3] - this->dataVec_[1]) / (4 * s),
                   (this->dataVec_[2] + this->dataVec_[6]) / (4 * s),
                   (this->dataVec_[5] + this->dataVec_[7]) / (4 * s), s);
    return tmp;
  }
}

Mat3x3 Mat3x3::rotMatAxis(double phi, const Vec3 &axisvec) {
  Vec3 tmpUnit(axisvec);
  //  tmpUnit = tmpUnit.normalized();
  Quaternion rq(phi, tmpUnit.x(), tmpUnit.y(), tmpUnit.z());

  return rq.rotMatrix();
}

void VecDisp::setDataPtr(double *ptr) { dataPtr_ = ptr; }

