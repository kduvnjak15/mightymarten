#include "BB_FEMSolver.h"
#include "BB_FiniteElements.h"
#include <BB_Algorithms.h>

BB_FEMSolver::BB_FEMSolver() {

  std::cout << "BB_FEMSolver ctor " << this << std::endl;
}

void BB_FEMSolver::setMesh(const bb::Mesh *meshPtr) {
  this->meshObj = std::make_shared<bb::Mesh>(*meshPtr);
  this->checkList.meshDefined = true;

  std::cout << "BB_FEMSolver::setMesh" << std::endl;
}

void BB_FEMSolver::defineStiffMatrix() {

  const uint numNodes = this->meshObj->getNumVertices();
  const uint numDOFs = numNodes * 4;
  const bb::nVec<bb::PolyCell> &meshCells = this->meshObj->getCells();

  globalStiffMatrix_ = SquareMatrix::zeros(numDOFs, numDOFs);

  for (uint i = 0; i < meshCells.size(); i++) {

    const bb::PolyCell &tmpCell = meshCells[i];
    const bb::nVec<uint> &polyfaceIDs = tmpCell.getCellPolyFaceIDs();
    const bb::nVec<PolyFace> &polyFaces = meshObj->getFaces();
    for (uint j = 0; j < polyfaceIDs.size(); j++) {
      const bb::PolyFace &tmpFace = polyFaces[polyfaceIDs[j]];
      bb::Vec2 minPt = meshObj->getVertex(tmpFace[0]).xy();
      bb::Vec2 maxPt = meshObj->getVertex(tmpFace[2]).xy();
      bb::SymMatrix tmpLocalStiffMat =
          std::move(rect16fem::getLocalStiffnessMatrix(minPt, maxPt));

      std::cout << tmpLocalStiffMat << " _ " << i << std::endl;

      const bb::nVec<uint> &faceNodes = tmpFace.polyFaceNodes_();
      //      std::cout << faceNodes << std::endl;
      for (uint row = 0; row < tmpLocalStiffMat.rows(); row++) {
        uint globalRowIdx = faceNodes[row / 4] * 4 + row % 4;
        for (uint col = 0; col < tmpLocalStiffMat.cols(); col++) {
          uint globalColIDx = faceNodes[col / 4] * 4 + col % 4;

          globalStiffMatrix_[globalRowIdx][globalColIDx] +=
              tmpLocalStiffMat[row][col];
        }
      }
    }
  }

  std::cout << globalStiffMatrix_ << std::endl;
}

void BB_FEMSolver::defineSourceVector() {

  const uint numNodes = this->meshObj->getNumVertices();
  const uint numDOFs = numNodes * 4;
  const bb::nVec<bb::PolyCell> &meshCells = this->meshObj->getCells();

  globalSourceVector_.data().resize(numDOFs);
  globalSourceVector_.data().init(0);

  loadQ_ = 0.01;
  double E = 200000;
  double h = 10;
  double ni = 0.3;

  double D = E * h * h * h / (12 * (1 - ni * ni));

  double qz = loadQ_ / D;
  for (uint i = 0; i < meshCells.size(); i++) {

    const bb::PolyCell &tmpCell = meshCells[i];
    const bb::nVec<uint> &polyfaceIDs = tmpCell.getCellPolyFaceIDs();
    const bb::nVec<PolyFace> &polyFaces = meshObj->getFaces();
    for (uint j = 0; j < polyfaceIDs.size(); j++) {
      const bb::PolyFace &tmpFace = polyFaces[polyfaceIDs[j]];
      bb::Vec2 minPt = meshObj->getVertex(tmpFace[0]).xy();
      bb::Vec2 maxPt = meshObj->getVertex(tmpFace[2]).xy();
      bb::dVec tmpLocalSourceVec =
          std::move(rect16fem::getSourceVector(minPt, maxPt));

      const bb::nVec<uint> &faceNodes = tmpFace.polyFaceNodes_();

      bb::nVec<uint> nemir;
      for (uint col = 0; col < tmpLocalSourceVec.size(); col++) {
        uint globalColIDx = faceNodes[col / 4] * 4 + col % 4;
        nemir.push_back(globalColIDx);
        globalSourceVector_[globalColIDx] += tmpLocalSourceVec[col] * qz;
      }
      std::cout << tmpLocalSourceVec * qz << std::endl;
    }
  }

  std::cout << globalSourceVector_ << std::endl;

  this->checkList.sourceVectorDefined = true;

  return;
}

void BB_FEMSolver::detectContraintNodes() {

  const uint numNodes = this->meshObj->getNumVertices();
  const bb::nVec<bb::PolyCell> &meshCells = this->meshObj->getCells();

  dVec nodesCnt(numNodes);
  nodesCnt.data().init(0);
  for (uint i = 0; i < meshCells.size(); i++) {

    const bb::PolyCell &tmpCell = meshCells[i];
    const bb::nVec<uint> &polyfaceIDs = tmpCell.getCellPolyFaceIDs();
    const bb::nVec<PolyFace> &polyFaces = meshObj->getFaces();
    for (uint j = 0; j < polyfaceIDs.size(); j++) {
      const bb::PolyFace &tmpFace = polyFaces[polyfaceIDs[j]];

      const bb::nVec<uint> &faceNodes = tmpFace.polyFaceNodes_();

      for (uint faceID = 0; faceID < faceNodes.size(); faceID++) {
        nodesCnt[faceNodes[faceID]]++;
      }
    }
  }

  for (uint i = 0; i < nodesCnt.size(); i++) {
    if (nodesCnt[i] < 4)
      constraintVector_.push_back(i);
  }

  this->checkList.constrDefined = true;
  return;
}

void BB_FEMSolver::defineBNDconditions() {
  loadQ_ = 1;
  elasticity_ = 10000;
}

void BB_FEMSolver::runAssembler() {

  bb::dVec constrainedDOFs;
  for (uint i = 0; i < constraintVector_.size(); i++) {
    uint DOFpos = (constraintVector_[i]) * 4;
    constrainedDOFs.push_back(DOFpos);
  }

  std::cout << "CONSTRDOF " << constrainedDOFs << std::endl;

  SortingAlgorithm<double>::getSortingAlgorithm().bubbleSort(
      constrainedDOFs.data().first(), constrainedDOFs.size());

  //  globalStiffMatrix_ = SquareMatrix(globalStiffMatrix_.rows());

  //  for (uint i = 0; i < globalStiffMatrix_.rows(); i++) {
  //    for (uint j = 0; j < globalStiffMatrix_.cols(); j++) {
  //      globalStiffMatrix_[i][j] = j;
  //    }
  //  }

  Matrix globCpy = globalStiffMatrix_;

  std::cout << globCpy << std::endl;

  for (int i = constrainedDOFs.size() - 1; i >= 0; --i) {

    Matrix::removeRow(globCpy, constrainedDOFs[i]);
    Matrix::removeCol(globCpy, constrainedDOFs[i]);

    globalSourceVector_.data().remove(constrainedDOFs[i]);
  }

  globalStiffMatrix_ = globCpy;

  std::cout << "pre nego izadje" << std::endl
            << globalStiffMatrix_ << std::endl;
  //  std::cout << globalSourceVector_ << std::endl;
  return;
}

void BB_FEMSolver::runSolver() {

  Matrix inverseMat = globalStiffMatrix_.inverse();
  resultVec_ = inverseMat * globalSourceVector_;
  std::cout << globalSourceVector_ << std::endl;
  std::cout << "res vec " << resultVec_ << std::endl;
  //  for (auto el : resultVec_.data()) {
  //    if (el > 1e-8)
  //      std::cout << " -> " << el << std::endl;
  //  }
  return;
}
