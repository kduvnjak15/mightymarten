#ifdef WIN32
#include "stdafx.h"
#endif

#include "BB_LinAlgebra.h"

#include <utility>
using namespace bb;

//////////////////////////////////////////////////////////////////////////////////////////
// dVec ctors
//////////////////////
dVec::dVec() {
  LOG(std::cout << this << " -> dVec default ctor " << std::endl;)
}
dVec::dVec(uint dim) {

  this->data().resize(dim);
  //  this->data().randomize();

  LOG(std::cout << this << " -> dVec param " << dim << " Ctor" << std::endl;)
}

dVec::dVec(const dVec &copyObj) {

  this->data_ = copyObj.data();

  LOG(std::cout << this << " -> dVec copyCtor" << std::endl;)
}

dVec &dVec::operator=(const dVec &copyObj) {
  LOG(std::cout << this << " -> dVec copy assignment operator" << std::endl;)

  if (this == &copyObj)
    return *this;

  this->data_ = copyObj.data();

  return *this;
}

double &dVec::operator[](uint pos) { return this->data_.at(pos); }

const double &dVec::operator[](uint pos) const { return this->data_.at(pos); }

dVec::dVec(dVec &&moveObj) {
  LOG(std::cout << this << " -> dVec move ctor " << std::endl;)

  this->data_ = std::move(moveObj.data_);

  LOG(std::cout << this << " -> dVec move ctor end" << std::endl;)
}

void dVec::operator=(dVec &&moveObj) {
  LOG(std::cout << this << " -> dVec move assignment operator on " << this
                << " for " << &moveObj << std::endl;)

  this->data_ = std::move(moveObj.data_);
}

const uint dVec::size() const { return this->data_.size(); }

std::ostream &operator<<(std::ostream &os, const dVec &obj) {
  if (obj.size() == 0) {
    os << "[ " << &obj << " is empty >" << std::endl;
  } else {
    os << "[ ";
    for (uint i = 0; i < obj.size() - 1; ++i) {
      os << *(obj.data().first() + i) << ", ";
    }

    os << *(obj.data().first() + obj.size() - 1);
    os << " ]";
  }
  return os;
}

dVec::dVec(std::initializer_list<double> &initList) {
  this->data_ = initList;
  LOG(std::cout << this << " -> dVec initList size ref" << this->size()
                << std::endl;)
}

dVec::dVec(std::initializer_list<double> &&initList) {
  this->data_ = initList;
  LOG(std::cout << this << " -> dVec initList size rval" << this->size()
                << std::endl;)
}

void dVec::operator=(std::initializer_list<double> &initlist) {

  for (auto item : initlist) {
    this->data_.push_back(item);
  }
  LOG(std::cout << "initlist assign " << std::endl;)
}

dVec::~dVec() {

  LOG(std::cout << this << " -> dVec default ~dtor " << std::endl;)
}

const bb::nVec<double> &dVec::data() const { return data_; }
bb::nVec<double> &dVec::data() { return data_; }

void dVec::push_back(double elem) {

  this->data_.push_back(elem);

  return;
}

const double dVec::length() const { return std::sqrt((*this) * (*this)); }

//////////////////////////////////////////////////
////  operators
//////////////////////////////////////////////////
dVec dVec::operator+(const dVec &operand) const {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  dVec tmpVec(*this);
  for (int i = 0; i < tmpVec.size(); i++) {
    tmpVec[i] += operand[i];
  }

  return std::move(tmpVec);
}

dVec dVec::operator-(const dVec &operand) const {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  //  std::cout << "operator - start for " << this << std::endl;
  dVec tmpVec(*this);
  //  std::cout << "operator - temp " << &tmpVec << std::endl;
  //  std::cout << "operator- : " << this << " - " << &tmpVec << std::endl;s
  for (uint i = 0; i < tmpVec.size(); i++) {
    tmpVec[i] -= operand[i];
  }

  return tmpVec;
}

dVec &dVec::operator+=(const dVec &operand) {

  if (this->size() == 0) {
    this->data().resize(operand.size());
  } else {
    if (this->size() != operand.size())
      throw std::runtime_error("Incompatible sizes");
  }

  LOG(std::cout << this << " -> dVec::operator+= " << std::endl;)
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] += operand[i];
  }
  LOG(std::cout << this << " -> dVec::operator+= end" << std::endl;)
  return (*this);
}

dVec &dVec::operator-=(const dVec &operand) {
  if (this->size() == 0) {
    this->data().resize(operand.size());
  } else {
    if (this->size() != operand.size())
      throw std::runtime_error("Incompatible sizes");
  }

  LOG(std::cout << this << " -> dVec::operator-= " << std::endl;)
  for (int i = 0; i < this->size(); i++) {
    (*this)[i] -= operand[i];
  }
  LOG(std::cout << this << " -> dVec::operator-= end" << std::endl;)
  return (*this);
}

double dVec::operator*(const dVec &operand) const {
  if (this->size() != operand.size())
    throw std::runtime_error("Incompatible sizes");

  LOG(std::cout << "dotProduct " << this << " * " << &operand << std::endl;)
  double dotProduct = 0.0;
  for (uint i = 0; i < this->size(); i++) {
    dotProduct += (*this)[i] * operand[i];
  }
  return dotProduct;
}

dVec dVec::operator*(const double scalar) {
  dVec tmp(*this);
  for (int i = 0; i < this->size(); i++) {
    tmp[i] *= scalar;
  }
  return std::move(tmp);
}

dVec &dVec::operator*=(const double scalar) {

  for (int i = 0; i < this->size(); i++) {
    (*this)[i] *= scalar;
  }
  return *this;
}

bb::dVec operator*(double &scalar, dVec &operand) { return dVec(); }
bb::dVec operator*(const double scalar, const dVec &operand) {

  dVec tmp(operand);
  for (int i = 0; i < operand.size(); i++) {
    tmp[i] *= scalar;
  }
  return std::move(tmp);
}

const Vec2 Vec2::operator*(const double scalar) const {
  bb::Vec2 tmpVec(*this);
  tmpVec.x() *= scalar;
  tmpVec.y() *= scalar;

  return std::move(tmpVec);
}

const bb::Vec2 Vec2::operator*(const uint scalar) const {
  bb::Vec2 tmpVec(*this);
  tmpVec.x() *= scalar;
  tmpVec.y() *= scalar;

  return std::move(tmpVec);
}

dVec dVec::operator/(const double scalar) {
  dVec tmp(*this);
  for (int i = 0; i < this->size(); i++) {
    tmp[i] /= scalar;
  }
  return std::move(tmp);
}

dVec &dVec::operator/=(const double scalar) {

  for (int i = 0; i < this->size(); i++) {
    (*this)[i] /= scalar;
  }
  return *this;
}

bool dVec::operator==(const dVec &obj) {
  if (this == &obj)
    return true;

  if (std::equal(obj.data().first(), obj.data().first() + obj.size(),
                 this->data().first()))
    return true;

  return false;
}

dVec dVec::operator!() {
  std::cout << "dVec::operator !  not implemented" << std::endl;
  return dVec();
}

void dVec::operator~() {
  this->transposed = !this->transposed;
  std::cout << "operator~()" << std::endl;
}

void dVec::normalize() {
  *this /= this->length();

  return;
}

dVec dVec::getNormalized() const {
  dVec tmp(*this);
  return tmp / this->length();
}

//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// dVec implementation
//////////////////////////////////////////////////////////////////////////////////////////
SquareMatrix::SquareMatrix() : Matrix(0, 0) {
  LOG(std::cout << this << " -> SquareMatrix default ctor" << std::endl;);
}

SquareMatrix::SquareMatrix(uint size) : Matrix(size, size) {
  LOG(std::cout << this << " -> SquareMatrix parametrized ctor" << std::endl;);
}

SquareMatrix::SquareMatrix(const Matrix &obj) : Matrix(obj) {
  LOG(std::cout << this << " -> SquareMatrix copy ctor from Matrix"
                << std::endl;)
}
SquareMatrix::SquareMatrix(const SquareMatrix &obj) : Matrix(obj) {
  LOG(std::cout << this << " -> SquareMatrix copy ctor" << std::endl;);
}

SquareMatrix::SquareMatrix(const SymMatrix &obj)
    : Matrix(obj.rows(), obj.cols()) {

  this->dataVec_.data().init(0);
  for (uint i = 0; i < obj.cols(); i++) {
    for (uint j = i; j < obj.cols(); j++) {
      this->dataVec_[i * obj.cols() + j] = obj[i][j];
      this->dataVec_[j * obj.cols() + i] = obj[i][j];
    }
  }

  return;
}

SquareMatrix &SquareMatrix::operator=(const SquareMatrix &obj) {

  Matrix::operator=(obj);

  return *this;
  LOG(std::cout << this << " -> SquareMatrix copy ctor" << std::endl;);
}

SquareMatrix &SquareMatrix::operator=(const Matrix &obj) {

  Matrix::operator=(obj);

  return *this;
  LOG(std::cout << this << " -> Matrix -> SquareMatrix copy ctor"
                << std::endl;);
}

SquareMatrix::SquareMatrix(SquareMatrix &&moveObj) : Matrix(moveObj) {
  LOGNOW(std::cout << this << " -> SquareMatrix move ctor" << std::endl;);
}

SquareMatrix &SquareMatrix::operator=(SquareMatrix &&obj) {
  std::cout << obj.rows() << std::endl;
  Matrix::operator=(std::move(obj));

  LOGNOW(std::cout << this << " -> SquareMatrix move ctor" << std::endl;);
  return *this;
}

SquareMatrix::SquareMatrix(
    std::initializer_list<std::initializer_list<double>> &&obj)
    : Matrix(std::move(obj)) {
  LOG(std::cout << this
                << " -> SquareMatrix initializer_list<initiliazer_list ctor"
                << std::endl;);
}

SquareMatrix &SquareMatrix::
operator=(std::initializer_list<std::initializer_list<double>> &&obj) {

  Matrix::operator=(std::move(obj));

  return *this;
  LOG(std::cout << this
                << " -> SquareMatrix "
                   "initializer_list<initiliazer_list operator= "
                << std::endl;);
}

SquareMatrix &SquareMatrix::operator=(std::initializer_list<dVec> &&obj) {

  for (auto it = obj.begin(); it != obj.end(); it++) {
    if (it->size() != obj.size())
      throw std::runtime_error(
          "Square Matrix cannot be intialized with nonuniform vectors");
  }

  Matrix::operator=(std::move(obj));

  return *this;
}
SquareMatrix operator*(const double scalar, const SquareMatrix &obj) { return SquareMatrix(); }
SquareMatrix SquareMatrix::operator*(const SquareMatrix &obj) const {

  Matrix tmp(*this);

  return tmp * obj;
}

SquareMatrix SquareMatrix::operator*(const double scalar) const {

  Matrix tmp(*this);

  return tmp * scalar;
}

void SquareMatrix::eye() {
  for (uint i = 0; i < this->rows(); i++) {
    for (uint j = 0; j < this->cols(); j++) {
      if (i == j)
        this->dataVec_[i * this->cols() + j] = 1;
      else
        this->dataVec_[i * this->cols() + j] = 0;
    }
  }
}

SquareMatrix SquareMatrix::eye(uint size) {
  SquareMatrix tmp(size);
  tmp.eye();
  return tmp;
}

SquareMatrix SquareMatrix::eye(const SquareMatrix &obj) {
  SquareMatrix tmp(obj.cols());
  tmp.eye();
  return tmp;
}

void SquareMatrix::LUdecomposition(const SquareMatrix &a, const SquareMatrix &l,
                                   const SquareMatrix &u) const {
  int i = 0, j = 0, k = 0;
  uint n = a.rows();
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      if (j < i)
        l[j][i] = 0;
      else {
        l[j][i] = a[j][i];
        for (k = 0; k < i; k++) {
          l[j][i] = l[j][i] - l[j][k] * u[k][i];
        }
      }
    }
    for (j = 0; j < n; j++) {
      if (j < i)
        u[i][j] = 0;
      else if (j == i)
        u[i][j] = 1;
      else {
        u[i][j] = a[i][j] / l[i][i];
        for (k = 0; k < i; k++) {
          u[i][j] = u[i][j] - ((l[i][k] * u[k][j]) / l[i][i]);
        }
      }
    }
  }
  return;
}

double SquareMatrix::determinant2() const {

  double det = 0;
  SquareMatrix subMatrix(this->row_ - 1);
  std::cout << subMatrix.rows() << std::endl;
  if (this->row_ == 2) {
    return ((this->dataVec_[0] * this->dataVec_[3]) -
            (this->dataVec_[1] * this->dataVec_[2]));
  } else {
    for (int x = 0; x < this->cols(); x++) {
      int subi = 0;
      for (int i = 1; i < this->cols(); i++) {
        int subj = 0;
        for (int j = 0; j < this->cols(); j++) {
          if (j == x)
            continue;

          subMatrix[subi][subj] = (*this)[i][j];
          subj++;
        }
        subi++;
      }
      det = det + (pow(-1, x) * (*this)[0][x] * subMatrix.determinant());
    }
  }
  return det;
}

SquareMatrix SquareMatrix::inverse() const {

  SquareMatrix AA = (*this) * (*this);
  double detA = this->determinant();
  double trA = this->trace();
  SquareMatrix I = SquareMatrix::eye(this->cols()) * 1. / 2.0;

  SquareMatrix tmp;
  tmp = ((I * (trA * trA - AA.trace()) - (*this) * trA + AA) * 1 / detA);
  std::cout << "inverse func" << detA << std::endl;
  std::cout << "inverse " << tmp << std::endl;
  return tmp;
  //  return (1.0 / detA * I);
}

double SquareMatrix::trace() const {
  double sum = 0;
  for (uint i = 0; i < row_; i++) {
    sum += *(this->data().data().first() + i * col_ + i);
  }
  return sum;
}

double SquareMatrix::trace(const SquareMatrix &obj) { return obj.trace(); }

double SquareMatrix::determinant() const {
  SquareMatrix L(this->rows());
  SquareMatrix U(this->rows());

  this->LUdecomposition(*this, L, U);

  double lproduct = 1.0;
  double uproduct = 1.0;
  for (uint i = 0; i < this->rows(); i++) {
    lproduct *= L[i][i];
    uproduct *= U[i][i];
  }

  return lproduct * uproduct;
}

Mat3x3::Mat3x3() : SquareMatrix(3) {
  LOG(std::cout << this << " -> Mat3x3 default ctor" << std::endl;)
  this->randomize();
}

Mat3x3::Mat3x3(const Matrix &obj) : SquareMatrix(3) {
  if (obj.cols() == 2 && obj.rows() == 2) {
    memcpy(const_cast<double *>(this->data().data().first()),
           obj.data().data().first(), obj.numOfElems() * sizeof(double));
  } else {
    throw std::runtime_error("Incompatible matrix with Mat2x2");
  }
}

Mat3x3::Mat3x3(SymMatrix &obj) : SquareMatrix(3) {
  for (uint i = 0; i < 3; i++) {
    for (uint j = 0; j < 3; j++) {
      (*this)[i][j] = obj[i][j];
    }
  }
}

Vec3 Mat3x3::operator*(const Vec3 &vecObj) {
  Vec3 tmp;
  for (uint i = 0; i < 3; i++) {
    tmp[0] += (*this)[0][i] * vecObj[i];
    tmp[1] += (*this)[1][i] * vecObj[i];
    tmp[2] += (*this)[2][i] * vecObj[i];
  }

  return tmp;
}

Mat3x3 Mat3x3::operator*(const Mat3x3 &matObj) {
  Mat3x3 tmp;
  tmp.zeros();
  for (uint r = 0; r < 3; r++) {
    for (uint c = 0; c < 3; c++) {
      for (uint in = 0; in < 3; in++) {
        tmp[r][c] += (*this)[r][in] * matObj.dataVec_[in * 3 + c];
      }
    }
  }
  return tmp;
}

Quaternion Mat3x3::quaternion() {

  double trace = this->trace();
  if (trace > 0) {
    double w = sqrt(1 + this->trace()) / 2.0;
    Quaternion tmp(w, (this->dataVec_[7] - this->dataVec_[5]) / (4 * w),
                   (this->dataVec_[2] - this->dataVec_[6]) / (4 * w),
                   (this->dataVec_[3] - this->dataVec_[1]) / (4 * w));
    return tmp;
  } else if (this->dataVec_[0] > this->dataVec_[4] &&
             this->dataVec_[0] > this->dataVec_[8]) {
    double s =
        sqrt(1 + this->dataVec_[0] - this->dataVec_[4] - this->dataVec_[8]) / 2;
    Quaternion tmp((this->dataVec_[7] - this->dataVec_[5]) / (4 * s), s,
                   (this->dataVec_[1] + this->dataVec_[3]) / (4 * s),
                   (this->dataVec_[2] + this->dataVec_[6]) / (4 * s));
    return tmp;

  } else if (this->dataVec_[4] > this->dataVec_[8]) {
    double s =
        sqrt(1 + this->dataVec_[4] - this->dataVec_[0] - this->dataVec_[8]) / 2;
    Quaternion tmp((this->dataVec_[2] - this->dataVec_[6]) / (4 * s),
                   (this->dataVec_[1] + this->dataVec_[3]) / (4 * s), s,
                   (this->dataVec_[5] + this->dataVec_[7]) / (4 * s));
    return tmp;
  } else {
    double s =
        sqrt(1 + this->dataVec_[8] - this->dataVec_[0] - this->dataVec_[4]) / 2;
    Quaternion tmp((this->dataVec_[3] - this->dataVec_[1]) / (4 * s),
                   (this->dataVec_[2] + this->dataVec_[6]) / (4 * s),
                   (this->dataVec_[5] + this->dataVec_[7]) / (4 * s), s);
    return tmp;
  }
}

Mat3x3 Mat3x3::rotMatAxis(double phi, const Vec3 &axisvec) {
  Vec3 tmpUnit(axisvec);
  //  tmpUnit = tmpUnit.normalized();
  Quaternion rq(phi, tmpUnit.x(), tmpUnit.y(), tmpUnit.z());

  return rq.rotMatrix();
}

void VecDisp::setDataPtr(double *ptr) { dataPtr_ = ptr; }

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

Matrix::Matrix() : Matrix(0, 0) {
  LOG(std::cout << this << " - > Matrix default ctor" << std::endl;)
}

Matrix::Matrix(uint row, uint col)
    : row_(row), col_(col), dataVec_(row * col),
      dispObj_(new DispObj(dataVec_.data().begin(), col)) {
  //  this->dataVec_ = bb::dVec(row * col);
  //  this->randomize();
  LOG(std::cout << this << " - > Matrix parametrized (row, col) ctor"
                << std::endl;)
}

Matrix::Matrix(const Matrix &copyObj)
    : row_(copyObj.row_), col_(copyObj.col_), dataVec_(copyObj.dataVec_),
      dispObj_(new DispObj(dataVec_.data().begin(), copyObj.col_)) {
  LOG(std::cout << this << " -> Matrix copy ctor" << std::endl;)
  operator=(copyObj);
  LOG(std::cout << this << " -> Matrix copy ctor end" << std::endl;)
}

Matrix::Matrix(const SymMatrix &copyObj)
    : Matrix(copyObj.rows(), copyObj.cols()) {

  for (uint i = 0; i < copyObj.rows(); i++) {
    for (uint j = 0; j < copyObj.cols(); j++) {
      (*this)[i][j] = copyObj[i][j];
    }
  }

  std::cout << "Created matrix from symmatrix" << std::endl;
}

Matrix &Matrix::operator=(const Matrix &copyObj) {

  LOG(std::cout << this << " -> Matrix::copy assignment operator" << std::endl;)
  this->row_ = copyObj.rows();
  this->col_ = copyObj.cols();
  this->dataVec_ = copyObj.dataVec_;

  LOG(std::cout << this << " -> Matrix::copy assignment operator end"
                << std::endl;)
  return *this;
}

Matrix::Matrix(Matrix &&moveObj) : dispObj_(new DispObj(*moveObj.dispObj_)) {
  LOG(std::cout << this << " -> Matrix::move Ctor" << std::endl;)
  operator=(std::move(moveObj));
  LOG(std::cout << this << " -> Matrix::move Ctor end" << std::endl;)
}

Matrix &Matrix::operator=(Matrix &&moveObj) {
  LOG(std::cout << this << " -> Matrix::move assignment " << std::endl;)
  this->row_ = moveObj.rows();
  this->col_ = moveObj.cols();

  this->dataVec_ = std::move(moveObj.dataVec_);

  moveObj.row_ = 0;
  moveObj.col_ = 0;

  return *this;
  LOG(std::cout << this << " -> Matrix::move assignment end" << std::endl;)
}

Matrix::Matrix(std::initializer_list<std::initializer_list<double>> &&obj)
    : Matrix(obj.size(), obj.begin()->size()) {
  LOG(std::cout << this
                << "-> Matrix(initializer_list<initializer_list<double>>) ctor "
                << std::endl;)

  uint row = 0;
  for (auto it = obj.begin(); it != obj.end(); it++) {
    uint col = 0;
    if (obj.begin()->size() != it->size())
      throw std::runtime_error("Init list matrix obj has nonuniform cols num");

    for (auto cit = it->begin(); cit != it->end(); cit++) {
      (*this)[row][col] = *cit;
      col++;
    }
    row++;
  }

  LOG(std::cout << this
                << "-> Matrix(initializer_list<initializer_list<double>>) "
                   "ctor end"
                << std::endl;)
}

Matrix Matrix::
operator=(std::initializer_list<std::initializer_list<double>> &&obj) {
  LOG(std::cout << this
                << "-> Matrix(initializer_list<initializer_list<double>>) ctor "
                << std::endl;)

  Matrix tmp(obj.size(), obj.begin()->size());

  uint row = 0;
  for (auto it = obj.begin(); it != obj.end(); it++) {
    uint col = 0;
    if (obj.begin()->size() != it->size())
      throw std::runtime_error("Init list matrix obj has nonuniform cols num");

    for (auto cit = it->begin(); cit != it->end(); cit++) {
      tmp[row][col] = *cit;
      col++;
    }
    row++;
  }

  *this = std::move(tmp);

  return *this;
  LOG(std::cout << this
                << "-> Matrix(initializer_list<initializer_list<double>>) "
                   "ctor end"
                << std::endl;)
}

Matrix::Matrix(std::initializer_list<dVec> &&obj)
    : dispObj_(new DispObj(dataVec_.data().begin(), obj.size()))
/*   : row_(obj.size()), col_(obj.begin()->size()), dataVec_(row_ * col_)*/ {
  LOG(std::cout << this << "-> Matrix(initializer_list<dVec>) ctor "
                << std::endl;)

  uint row = 0;
  for (auto it = obj.begin(); it != obj.end(); it++) {
    if (it->size() != this->cols())
      throw std::runtime_error("Init list matrix obj has nonuniform cols num");

    for (uint j = 0; j < this->cols(); j++) {
      this->dataVec_[row * this->cols() + j] = *(it->data().first() + j);
    }
    row++;
  }
  LOG(std::cout << this << "-> Matrix(initializer_list<dVec>) ctor end "
                << std::endl;)
}

Matrix Matrix::operator=(std::initializer_list<dVec> &&obj) {
  LOG(std::cout << this << "-> Matrix::operator=(initializer_list<dVec>) ctor "
                << std::endl;)

  uint rows = obj.size();
  uint cols = obj.begin()->size();

  std::cout << "uno " << rows << " < " << cols << std::endl;

  Matrix tmp(rows, cols);

  uint row = 0;
  for (auto it = obj.begin(); it != obj.end(); it++) {
    if (it->size() != cols)
      throw std::runtime_error("Init list matrix obj has nonuniform cols num");

    for (uint j = 0; j < cols; j++) {
      tmp[row][j] = *(it->data().first() + j);
    }
    row++;
  }

  *this = std::move(tmp);

  LOG(std::cout << this
                << "-> Matrix::operator=(initializer_list<dVec>) ctor end "
                << std::endl;)
  return *this;
}

Matrix::~Matrix() {
  LOG(std::cout << this << " -> Matrix ~dtor" << std::endl;)

  if (dispObj_)
    delete dispObj_;

  LOG(std::cout << this << " -> Matrix ~dtor end" << std::endl;)
}

const dVec &Matrix::data() const { return dataVec_; }

dVec &Matrix::data() { return dataVec_; }

void Matrix::randomize() {

  for (uint i = 0; i < this->dataVec_.size(); i++) {
    this->dataVec_[i] = rand() % 10;
  }
}

void Matrix::zeros() { dataVec_.data().init(0); }

void Matrix::ones() { dataVec_.data().init(1); }

const uint Matrix::rows() const { return this->row_; }

const uint Matrix::cols() const { return this->col_; }

const uint Matrix::numOfElems() const { return row_ * col_; }

double Matrix::determinant() const {
  if (row_ != col_)
    std::cout << "Non square matrix have no determinant" << std::endl;

  bb::SquareMatrix tmp(*this);

  return tmp.determinant();
}

DispObj &Matrix::operator[](uint index) {

  if (this->row_ - 1 < index) {
    throw std::runtime_error("Matrix has got less rows than requested[]");
  }

  dispObj_->posPtr_ =
      const_cast<double *>(this->data().data().first() + this->cols() * index);
  dispObj_->num_ = this->cols();
  dispObj_->stride_ = 1;

  return *dispObj_;
}

DispObj Matrix::operator[](uint index) const {

  DispObj tmp(*this->dispObj_);

  if (this->row_ - 1 < index) {
    throw std::runtime_error("Matrix has got less rows than requested[]");
  }

  tmp.posPtr_ =
      const_cast<double *>(this->data().data().first() + this->cols() * index);
  tmp.num_ = this->cols();
  tmp.stride_ = 1;

  return tmp;
}

DispObj Matrix::operator()(uint col) const {
  DispObj tmp(*this->dispObj_);
  if (this->col_ - 1 < col) {
    throw std::runtime_error("Matrix has got less cols than requested[]");
  }

  tmp.posPtr_ = const_cast<double *>(this->data().data().first() + col);

  tmp.num_ = this->row_;
  tmp.stride_ = this->col_;

  return tmp;
}

DispObj &Matrix::operator()(uint col) {

  if (this->col_ - 1 < col) {
    throw std::runtime_error("Matrix has got less cols than requested[]");
  }

  dispObj_->posPtr_ = const_cast<double *>(this->data().data().first() + col);

  dispObj_->num_ = this->row_;
  dispObj_->stride_ = this->col_;

  return *dispObj_;
}

Matrix Matrix::operator*(const Matrix &obj) {
  LOG(std::cout << this << " -> Matrix::operator* " << &obj << std::endl;)

  Matrix newMat(this->rows(), obj.cols());
#ifdef GPU
  double *d_a;
  double *d_b;
  double *d_res;

  uint width = this->cols();
  dim3 threads(this->rows(), width, obj.cols());

  cudaMalloc((void **)(&d_a), this->numOfElems() * sizeof(double));
  cudaMalloc((void **)(&d_b), obj.numOfElems() * sizeof(double));
  cudaMalloc((void **)(&d_res),
             threads.x * threads.y * threads.z * sizeof(double));

  cudaMemcpy((void *)d_a, (const void *)this->data(),
             this->numOfElems() * sizeof(double), cudaMemcpyHostToDevice);
  cudaMemcpy((void *)d_b, (const void *)obj.data(),
             obj.numOfElems() * sizeof(double), cudaMemcpyHostToDevice);

  matrixMultiplication<<<1, threads>>>(d_a, d_b, d_res);
  cudaDeviceSynchronize();

  dVec tmpData(this->rows() * this->cols());

  double tt[threads.x * threads.y * threads.z];
  cudaError err = cudaMemcpy((void *)tt, (const void *)d_res,
                             threads.x * threads.y * threads.z * sizeof(double),
                             cudaMemcpyDeviceToHost);

  if (err != cudaSuccess)
    cout << "jedifovna" << std::endl;
  cout << threads.x * threads.y * threads.z << std::endl;
  for (int i = 0; i < threads.x * threads.y * threads.z; i++) {
    //        cout << "OF i = " << i << " == " << tt[i] << ", (" << i /
    //        (this->cols() * obj.cols()) <<", " <<  (i % (this->cols() *
    //        obj.cols())) / this->cols() << ")"<< std::endl;

    newMat[(i / (this->cols() * obj.cols()))]
          [(i % (this->cols() * obj.cols())) / this->cols()] += tt[i];
  }

#else

  if (obj.rows() != this->cols())
    std::runtime_error("Incompatible matrices for multiplication");

  for (uint i = 0; i < this->rows(); i++) {
    for (uint j = 0; j < obj.cols(); j++) {
      double tmpSum = 0.0;
      for (uint k = 0; k < this->cols(); ++k) {
        tmpSum += (*this)[i][k] * const_cast<Matrix &>(obj)[k][j];
      }
      newMat[i][j] = tmpSum;
    }
  }

#endif
  return newMat;
}

Matrix Matrix::operator~() {
  Matrix tmp(this->cols(), this->rows());
  for (uint i = 0; i < tmp.rows() * tmp.cols(); i++) {
    tmp.dataVec_[i] = *(this->data().data().first() +
                        (i % tmp.cols()) * tmp.rows() + (i / tmp.cols()));
  }
  return tmp;
}

bool Matrix::operator==(const Matrix &obj) {
  if (this == &obj)
    return true;

  if (std::equal(this->data().data().first(),
                 this->data().data().first() + this->dataVec_.size(),
                 obj.data().data().first()) &&
      this->rows() == obj.rows())
    return true;
  else
    return false;
}

Matrix Matrix::zeros(uint rows, uint cols) {
  Matrix tmp(rows, cols);
  tmp.zeros();
  return std::move(tmp);
}

Matrix Matrix::ones(uint rows, uint cols) {
  Matrix tmp(rows, cols);
  tmp.ones();
  return tmp;
}

Matrix Matrix::slice(uint startX, uint startY, uint stopX, uint stopY) {
  if (startX > stopX || startY > stopY)
    throw std::runtime_error("Invalid input for slice parameters");

  Matrix temp(stopX - startX, stopY - startY);
  for (uint i = startX; i < stopX; i++) {
    for (uint j = startY; j < stopY; j++) {
      temp[i][j] = (*this)[i][j];
    }
  }

  return std::move(temp);
}

dVec Matrix::sliceRow(uint row, uint startX, int stopX) {

  if (stopX < 0)
    stopX = this->col_ - 1;

  dVec temp(stopX - startX);
  for (uint i = startX; i < stopX; i++) {

    temp[i] = (*this)[row][i];
  }

  return std::move(temp);
}

dVec Matrix::sliceCol(uint col, uint startY, int stopY) {

  if (stopY < 0)
    stopY = this->row_ - 1;

  dVec temp(stopY - startY);
  for (uint i = startY; i < stopY; i++) {

    temp[i] = (*this)[i][col];
  }

  return std::move(temp);
}

void Matrix::removeRow(Matrix &matObj, uint row) {

  if (row >= matObj.rows()) {
    std::cout << "Invalid row index to remove!" << std::endl;
    return;
  }
  if (row < matObj.rows() - 1) {

    uint offset = matObj.cols();

    double *startPtr = matObj.data().data().first();

    for (uint i = (row + 1) * matObj.cols(); i < matObj.rows() * matObj.cols();
         i++) {
      //      std::cout << "Transfering " << matObj.data().data()[i] << "
      //      Instead of "
      //                << *(startPtr + i - offset) << std::endl;
      *(startPtr + i - offset) = std::move(matObj.data().data()[i]);
    }
  }

  matObj.row_--;
  matObj.data().data().resize(matObj.cols() * matObj.rows());
}

void Matrix::removeCol(Matrix &matObj, uint col) {
  matObj = ~(matObj);

  Matrix::removeRow(matObj, col);
  matObj = ~(matObj);
}

Matrix &Matrix::embedd(uint startX, uint startY, Matrix &matObj) {
  uint dimy = this->row_;
  uint dimx = this->col_;

  if (dimy < (matObj.row_ + startY)) {
    dimy = matObj.row_ + startY;
  }
  if (dimx < (matObj.col_ + startX)) {
    dimx = matObj.col_ + startX;
  }

  Matrix temp(dimy, dimx);
  temp.zeros();

  for (uint i = 0; i < this->row_; i++) {
    for (uint j = 0; j < this->col_; j++) {
      temp[i][j] = (*this)[i][j];
    }
  }

  for (uint i = 0; i < matObj.rows(); i++) {
    for (uint j = 0; j < matObj.cols(); j++) {
      temp[i + startY][j + startX] = matObj[i][j];
    }
  }

  *this = temp;

  return *this;
}

void Matrix::setSize(uint rows, uint cols) {
  bb::Matrix temp(rows, cols);
  for (uint i = 0; i < rows; i++) {
    for (uint j = 0; j < cols; j++) {
      temp[i][j] = (*this)[i][j];
    }
  }

  (*this) = temp;
  return;
}

Matrix Matrix::operator+(const Matrix &sumand) const {
  LOG(std::cout << this << " -> Matrix::operator+" << std::endl;)

  if (this->rows() != sumand.rows() || this->cols() != sumand.cols())
    throw std::runtime_error("Incompatible matrix for operator + ");

  Matrix tmp(*this);
  tmp.dataVec_ += sumand.dataVec_;

  LOG(std::cout << this << " -> Matrix::operator+ end" << std::endl;)
  return tmp;
}

Matrix &Matrix::operator+=(const Matrix &sumand) {
  LOG(std::cout << this << " -> Matrix::operator+=" << std::endl;)

  if (this->rows() != sumand.rows() || this->cols() != sumand.cols())
    throw std::runtime_error("Incompatible matrix for operator + ");

  this->dataVec_ += sumand.dataVec_;

  LOG(std::cout << this << " -> Matrix::operator+= end" << std::endl;)
  return *this;
}

Matrix Matrix::operator-(const Matrix &sumand) {
  LOG(std::cout << this << " -> Matrix::operator+" << std::endl;)

  if (this->rows() != sumand.rows() || this->cols() != sumand.cols())
    throw std::runtime_error("Incompatible matrix for operator + ");

  Matrix tmp(*this);
  tmp.dataVec_ -= sumand.dataVec_;

  LOG(std::cout << this << " -> Matrix::operator+ end" << std::endl;)
  return tmp;
}

Matrix &Matrix::operator-=(const Matrix &sumand) {
  LOG(std::cout << this << " -> Matrix::operator+=" << std::endl;)

  if (this->rows() != sumand.rows() || this->cols() != sumand.cols())
    throw std::runtime_error("Incompatible matrix for operator + ");

  this->dataVec_ -= sumand.dataVec_;

  LOG(std::cout << this << " -> Matrix::operator+= end" << std::endl;)
  return *this;
}

Matrix Matrix::operator*(const double scalar) const {
  LOG(std::cout << this << " -> const Matrix::operator*scalar" << std::endl;)
  Matrix tmp(*this);
  tmp.dataVec_ *= scalar;
  LOG(std::cout << this << " -> const Matrix::operator*scalar end"
                << std::endl;)
  return tmp;
}

Matrix Matrix::operator/(const double scalar) const {
  LOG(std::cout << this << " -> const Matrix::operator/scalar" << std::endl;)
  Matrix tmp(*this);
  tmp.dataVec_ *= 1 / scalar;
  LOG(std::cout << this << " -> const Matrix::operatorscalar end" << std::endl;)
  return tmp;
}

dVec Matrix::operator*(const dVec &vecObj) const {
  bb::dVec res(this->rows());
  res.data().init(0);
  for (uint row = 0; row < this->rows(); row++) {
    for (uint col = 0; col < this->cols(); col++) {
      res[row] += (*this)[row][col] * vecObj[col];
    }
  }

  return res;
}

Matrix operator*(const double scalar, Matrix &obj) { return obj * scalar; }

SymMatrix::SymMatrix() : SquareMatrix(0) {}

SymMatrix::SymMatrix(uint size) : SquareMatrix(size) {
  this->row_ = size;
  this->col_ = size;

  std::cout << this->row_ << std::endl;
  std::cout << this->col_ << std::endl;

  delete dispObj_;
  dispObj_ = new SymDispObj(dataVec_.data().begin(), this->cols());

  dataVec_.data().clear(); // this is bug
  dataVec_.data().resize((size * size + size) / 2);

  this->randomize();
}

SymMatrix::SymMatrix(const Matrix &obj) : SymMatrix(obj.rows()) {

  for (uint i = 0; i < obj.rows(); i++) {
    for (uint j = i; j < obj.cols(); j++) {
      (*this)[i][j] = obj[i][j];
    }
  }
}

SymMatrix::SymMatrix(const SquareMatrix &obj) : SquareMatrix(obj) {}

SymMatrix::SymMatrix(const SymMatrix &obj) : SquareMatrix(obj) {}

Matrix &SymMatrix::operator=(const Matrix &obj) {

  Matrix::operator=(obj);

  return *this;
  LOG(std::cout << this << " -> Matrix -> SquareMatrix copy ctor"
                << std::endl;);
}

SymMatrix::SymMatrix(SymMatrix &&moveObj) {
  this->row_ = moveObj.row_;
  this->col_ = moveObj.col_;

  this->dataVec_ = std::move(moveObj.dataVec_);

  std::cout << "Move ctor called" << std::endl;
  //  std::cout << (*this) << std::endl;
  std::cout << "Move ctor succesefull " << std::endl;
}

SymMatrix SymMatrix::operator+(const SymMatrix &obj) {
  SymMatrix tmp(*this);
  tmp.operator+=(obj);
  return tmp;
}

SymMatrix SymMatrix::operator-(const SymMatrix &obj) {

  SymMatrix tmp(obj);

  tmp.dataVec_ = this->dataVec_ - obj.data();

  return tmp;
}

SymDispObj SymMatrix::operator[](uint index) const {

  SymDispObj tmp(this->data().data().begin(), this->row_, index);

  return tmp;
}

SymMatrix SymMatrix::operator~() {

  Matrix tmp(*this);
  std::cout << "stuc " << tmp << std::endl;
  SymMatrix symtmp(~tmp);

  std::cout << symtmp << std::endl;
  return symtmp;
}

double SymMatrix::determinant() {
  throw std::runtime_error("Not implemented SymMatrix::determinant");
  return 0;
}

Quaternion::Quaternion()
    : x_(*(this->data_ + 1)), y_(*(this->data_ + 2)), z_(*(this->data_ + 3)),
      w_(*(this->data_)) {
  data_[0] = 0;
  data_[1] = 0;
  data_[2] = 0;
  data_[3] = 0;
}

Quaternion::Quaternion(double w, double x, double y, double z)
    : x_(*(this->data_ + 1)), y_(*(this->data_ + 2)), z_(*(this->data_ + 3)),
      w_(*(this->data_)) {
  data_[0] = w;
  data_[1] = x;
  data_[2] = y;
  data_[3] = z;
}

Quaternion::Quaternion(double phi, const Vec3 &axis)
    : Quaternion(cos(phi / 2), axis.normalized().x() * sin(phi / 2),
                 axis.normalized().y() * sin(phi / 2),
                 axis.normalized().z() * sin(phi / 2)) {}

Quaternion::Quaternion(const Quaternion &copyObj)
    : x_(*(this->data_ + 1)), y_(*(this->data_ + 2)), z_(*(this->data_ + 3)),
      w_(*(this->data_)) {
  memcpy(this->data_, copyObj.data_, 4 * sizeof(double));
}

Quaternion Quaternion::operator=(const Quaternion &copyObj) {
  memcpy(this->data_, copyObj.data_, 4 * sizeof(double));
  return *this;
}

Quaternion::Quaternion(std::initializer_list<double> &initList)
    : x_(*(this->data_ + 1)), y_(*(this->data_ + 2)), z_(*(this->data_ + 3)),
      w_(*(this->data_)) {
  this->operator=(initList);
}

void Quaternion::operator=(std::initializer_list<double> &initList) {
  if (initList.size() != 4) {
    throw std::runtime_error("Invalid quaternion size, must be 4");
  }

  memcpy(this->data_, initList.begin(), 4 * sizeof(double));

  return;
}

Quaternion operator*(double scalar, Quaternion &obj) { return obj * scalar; }

Quaternion Quaternion::operator+(const Quaternion &obj) {
  Quaternion tmp;
  tmp.data_[0] = this->data_[0] + obj.data_[0];
  tmp.data_[1] = this->data_[1] + obj.data_[1];
  tmp.data_[2] = this->data_[2] + obj.data_[2];
  tmp.data_[3] = this->data_[3] + obj.data_[3];

  return tmp;
}

Quaternion Quaternion::operator-(const Quaternion &obj) {
  Quaternion tmp;
  tmp.data_[0] = this->data_[0] - obj.data_[0];
  tmp.data_[1] = this->data_[1] - obj.data_[1];
  tmp.data_[2] = this->data_[2] - obj.data_[2];
  tmp.data_[3] = this->data_[3] - obj.data_[3];

  return tmp;
}

Quaternion Quaternion::operator*(const double scalar) {
  Quaternion tmp;
  tmp.data_[0] = this->data_[0] * scalar;
  tmp.data_[1] = this->data_[1] * scalar;
  tmp.data_[2] = this->data_[2] * scalar;
  tmp.data_[3] = this->data_[3] * scalar;

  return tmp;
}

Quaternion Quaternion::operator/(const double scalar) {
  Quaternion tmp;
  tmp.data_[0] = this->data_[0] / scalar;
  tmp.data_[1] = this->data_[1] / scalar;
  tmp.data_[2] = this->data_[2] / scalar;
  tmp.data_[3] = this->data_[3] / scalar;

  return tmp;
}

Quaternion Quaternion::operator*(const Quaternion &obj) {

  Quaternion tmp;
  tmp.data_[0] = this->data_[0] * obj.data_[0] - this->data_[1] * obj.data_[1] -
                 this->data_[2] * obj.data_[2] - this->data_[3] * obj.data_[3];

  tmp.data_[1] = this->data_[0] * obj.data_[1] + this->data_[1] * obj.data_[0] +
                 this->data_[2] * obj.data_[3] - this->data_[3] * obj.data_[2];

  tmp.data_[2] = this->data_[0] * obj.data_[2] - this->data_[1] * obj.data_[3] +
                 this->data_[2] * obj.data_[0] + this->data_[3] * obj.data_[1];

  tmp.data_[3] = this->data_[0] * obj.data_[3] + this->data_[1] * obj.data_[2] -
                 this->data_[2] * obj.data_[1] + this->data_[3] * obj.data_[0];

  return tmp;
}

Quaternion Quaternion::operator~() {
  Quaternion tmp(*this);

  tmp.data_[1] *= -1.;
  tmp.data_[2] *= -1.;
  tmp.data_[3] *= -1.;

  return tmp;
}

double Quaternion::norm() {
  return std::sqrt(this->x_ * this->x_ + this->y_ * this->y_ +
                   this->z_ * this->z_ + this->w_ * this->w_);
}

Quaternion Quaternion::unit() { return (*this) / this->norm(); }

Quaternion Quaternion::inverse() {
  return (~(*this)) / (this->norm() * this->norm());
}

Mat3x3 Quaternion::rotMatrix() {
  Mat3x3 tmp;
  double s = 1 / this->norm() / this->norm();
  tmp[0][0] = 1 - 2 * s * (this->y_ * this->y_ + this->z_ * this->z_);
  tmp[0][1] = 2 * s * (this->x_ * this->y_ - this->z_ * this->w_);
  tmp[0][2] = 2 * s * (this->x_ * this->z_ + this->y_ * this->w_);

  tmp[1][0] = 2 * s * (this->x_ * this->y_ + this->z_ * this->w_);
  tmp[1][1] = 1 - 2 * s * (this->x_ * this->x_ + this->z_ * this->z_);
  tmp[1][2] = 2 * s * (this->y_ * this->z_ - this->x_ * this->w_);

  tmp[2][0] = 2 * s * (this->x_ * this->z_ - this->y_ * this->w_);
  tmp[2][1] = 2 * s * (this->y_ * this->z_ + this->x_ * this->w_);
  tmp[2][2] = 1 - 2 * s * (this->x_ * this->x_ + this->y_ * this->y_);

  return tmp;
}

Vec3 Quaternion::rotateVec3(Vec3 &obj) {
  Vec3 u(this->x_, this->y_, this->z_);
  double s = this->w_;
  Vec3 vprime =
      /*2.0f * (obj * u) * u + (s * s - (u * u)) * obj   +2.0f * s */
      (u ^ obj);

  return vprime;
}

Mat2x2::Mat2x2() : SquareMatrix(2) {
  LOG(std::cout << this << " -> Mat2x2 default ctor" << std::endl;)
  this->randomize();
}

Mat2x2::Mat2x2(const Matrix &obj) : SquareMatrix(2) {
  if (obj.cols() == 2 && obj.rows() == 2) {
    memcpy(const_cast<double *>(this->data().data().first()),
           obj.data().data().first(), obj.numOfElems() * sizeof(double));
  } else {
    throw std::runtime_error("Incompatible matrix with Mat2x2");
  }
}

Mat2x2::Mat2x2(SymMatrix &obj) : SquareMatrix(2) {
  for (uint i = 0; i < 2; i++) {
    for (uint j = 0; j < 2; j++) {
      (*this)[i][j] = obj[i][j];
    }
  }
}

Mat4x4::Mat4x4() : SquareMatrix(4) {
  LOG(std::cout << this << " -> Mat4x4 default ctor" << std::endl;)
  this->randomize();
}

DispObj::DispObj(double *posPtr, uint num, uint stride)
    : posPtr_(posPtr), num_(num), stride_(stride) {}

DispObj::DispObj(const DispObj &obj)
    : num_(obj.num_), stride_(obj.stride_), posPtr_(obj.posPtr_) {}

DispObj::DispObj(const dVec &obj) {
  throw std::runtime_error("STill not implementd");
}

DispObj DispObj::operator=(const dVec &obj) {
  throw std::runtime_error("STill not implementd");
}

double &DispObj::operator[](uint colIndex) {
  return *(posPtr_ + colIndex * stride_);
}

SymDispObj::SymDispObj(double *posPtr, uint num, uint idx)
    : DispObj(posPtr, num, idx) {}
