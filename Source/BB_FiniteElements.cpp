#include "BB_FiniteElements.h"
#include "BB_Quad16Functors.h"
#include <functional>
#include <thread>
double eta2[2] = {-0.577350269189626, 0.577350269189626};
double eta3[3] = {-0.774596669241483, 0.0, 0.774596669241483};
double eta4[4] = {-0.861136311594053, -0.339981043584856, 0.339981043584856,
                  0.861136311594053};

double weightsCR2[2] = {1.0, 1.0};
double weightsCR3[3] = {0.55555555555556, 0.888888888889, 0.55555555555556};
double weightsCR4[4] = {0.347854845137454, 0.652145154862546, 0.652145154862546,
                        0.347854845137454};

SquareMatrix rect16fem2(Vec2 pt1, Vec2 pt2, Vec2 pt3, Vec2 pt4,
                        uint integrationPts) {
  if (integrationPts < 2 || integrationPts > 4)
    throw std::runtime_error("Wrong number of integration pts");

  SquareMatrix tmp(16);

  double a = pt2.x() - pt1.x();
  double b = pt4.y() - pt1.y();
  double sum = 0.0;

  for (uint i = 0; i < integrationPts; i++) {
    for (uint j = 0; j < integrationPts; j++) {

      //      sum += weightsCR4[i] * weightsCR4[j] * func(a, b, eta4[i],
      //      eta2[j]);
    }
  }

  return tmp;
}

SymMatrix rect16fem::getLocalStiffnessMatrix(bb::Vec2 minPt, bb::Vec2 maxPt,
                                             uint integrationPts) {

  uint dim = stiffMatElementFunctors_.size();

  double a = (maxPt.x() - minPt.x()) / 2.0;
  double b = (maxPt.y() - minPt.y()) / 2.0;

  bb::SymMatrix sum(dim);
  sum.zeros();
  for (uint i = 0; i < integrationPts; i++) {
    for (uint j = 0; j < integrationPts; j++) {

      double x = a * eta4[i];
      double y = b * eta4[j];

      ////////////////////////////////////////////////////////////////////
      for (uint row = 0; row < dim; row++) {
        for (uint col = 0; col < dim - row; col++) {
          auto func = stiffMatElementFunctors_[row][col];
          double resval =
              func(a, b, x, y) * weightsCR4[i] * weightsCR4[j] * a * b;
          sum[row][col + row] += resval;
        }
      }

      ///////////////////////////////////////////////////////////////////////
    }
  }
  std::cout << sum << std::endl;
  return sum;
}

dVec rect16fem::getSourceVector(Vec2 minPt, Vec2 maxPt, uint integrationPts) {

  uint dim = sourceVectorElementFunctors_.size();

  double a = (maxPt.x() - minPt.x()) / 2.0;
  double b = (maxPt.y() - minPt.y()) / 2.0;

  bb::dVec sum(dim);
  sum.data().init(0.0);
  for (uint i = 0; i < integrationPts; i++) {
    for (uint j = 0; j < integrationPts; j++) {

      double x = a * eta4[i];
      double y = b * eta4[j];

      for (uint idx = 0; idx < dim; idx++) {
        //        sum[idx] += weightsCR4[i] * weightsCR4[j] * resval;

        auto func = sourceVectorElementFunctors_[idx];
        double resval =
            func(a, b, x, y) * weightsCR4[i] * weightsCR4[j] * a * b;
        sum[idx] += resval;
      }
    }
  }

  return sum;
}

bb::nVec<bb::nVec<std::function<double(double, double, double, double)>>>
    rect16fem::stiffMatElementFunctors_{
        bb::nVec<std::function<double(double, double, double, double)>>{
            K1_1,
            K1_2,
            K1_3,
            K1_4,
            K1_5,
            K1_6,
            K1_7,
            K1_8,
            K1_9,
            K1_10,
            K1_11,
            K1_12,
            K1_13,
            K1_14,
            K1_15,
            K1_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K2_2,
            K2_3,
            K2_4,
            K2_5,
            K2_6,
            K2_7,
            K2_8,
            K2_9,
            K2_10,
            K2_11,
            K2_12,
            K2_13,
            K2_14,
            K2_15,
            K2_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K3_3,
            K3_4,
            K3_5,
            K3_6,
            K3_7,
            K3_8,
            K3_9,
            K3_10,
            K3_11,
            K3_12,
            K3_13,
            K3_14,
            K3_15,
            K3_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K4_4,
            K4_5,
            K4_6,
            K4_7,
            K4_8,
            K4_9,
            K4_10,
            K4_11,
            K4_12,
            K4_13,
            K4_14,
            K4_15,
            K4_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K5_5,
            K5_6,
            K5_7,
            K5_8,
            K5_9,
            K5_10,
            K5_11,
            K5_12,
            K5_13,
            K5_14,
            K5_15,
            K5_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K6_6,
            K6_7,
            K6_8,
            K6_9,
            K6_10,
            K6_11,
            K6_12,
            K6_13,
            K6_14,
            K6_15,
            K6_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K7_7,
            K7_8,
            K7_9,
            K7_10,
            K7_11,
            K7_12,
            K7_13,
            K7_14,
            K7_15,
            K7_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K8_8,
            K8_9,
            K8_10,
            K8_11,
            K8_12,
            K8_13,
            K8_14,
            K8_15,
            K8_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K9_9,
            K9_10,
            K9_11,
            K9_12,
            K9_13,
            K9_14,
            K9_15,
            K9_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K10_10,
            K10_11,
            K10_12,
            K10_13,
            K10_14,
            K10_15,
            K10_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K11_11,
            K11_12,
            K11_13,
            K11_14,
            K11_15,
            K11_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K12_12,
            K12_13,
            K12_14,
            K12_15,
            K12_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K13_13,
            K13_14,
            K13_15,
            K13_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K14_14,
            K14_15,
            K14_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K15_15,
            K15_16,
        },
        bb::nVec<std::function<double(double, double, double, double)>>{
            K16_16,
        },

    };

bb::nVec<std::function<double(double, double, double, double)>>
    rect16fem::sourceVectorElementFunctors_{N_1,  N_2,  N_3,  N_4,  N_5,  N_6,
                                            N_7,  N_8,  N_9,  N_10, N_11, N_12,
                                            N_13, N_14, N_15, N_16

    };
