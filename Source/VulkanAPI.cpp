#include "VulkanAPI.h"

#include <stdexcept>
#include <assert.h>

VulkanAPI::VulkanAPI(void* connectionPtr, uint32_t windowID)
    :
    connPtr_(connectionPtr), windowID_(windowID), isInitialized_(false)
{
    if (connPtr_ == nullptr)
    {
        isInitialized_ = false;
        return;
    }

    init();

    createPhysicalDevice();
    createDevice();
    createSurface();
}

void VulkanAPI::reinitialize(void* cP, uint32_t wndID) { connPtr_ = cP; windowID_ = wndID; init(); }

VulkanAPI::~VulkanAPI()
{
    vkDestroySurfaceKHR(instance_, surface_, nullptr);
    vkDestroyDevice(device_, nullptr);
    vkDestroyInstance(instance_, NULL);
}

void VulkanAPI::init()
{
    if (connPtr_ == nullptr)
        return;
    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pNext = NULL;
    appInfo.pApplicationName = "VulkanAPI";
    appInfo.pEngineName = "ENGINE NAME";
    appInfo.apiVersion = 0;

    VkInstanceCreateInfo createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pNext = NULL;
    createInfo.flags = 0;
    createInfo.pApplicationInfo = &appInfo;
    createInfo.enabledLayerCount = 0;

    enabledExtensions_ = { VK_KHR_SURFACE_EXTENSION_NAME,  "VK_KHR_xcb_surface" };




#ifdef __linux__
    //enabledExtensions_.push_back("VK_KHR_xcb_surface");
#endif

    createInfo.enabledExtensionCount = enabledExtensions_.size();
    createInfo.ppEnabledExtensionNames = enabledExtensions_.data();

    //
    VkResult result = vkCreateInstance(&createInfo, NULL, &instance_);
    if (result != VK_SUCCESS) {
        throw std::runtime_error("Failed to create instance");
    }

    isInitialized_ = true;
}

void VulkanAPI::createPhysicalDevice() {

    uint32_t physicalDevicesCount = 0;
    VkResult res = vkEnumeratePhysicalDevices(instance_, &physicalDevicesCount, nullptr);

    std::vector<VkPhysicalDevice> physDevices(physicalDevicesCount);
    res = vkEnumeratePhysicalDevices(instance_, &physicalDevicesCount, physDevices.data());


    VkPhysicalDeviceProperties vkPDProps;
    vkGetPhysicalDeviceProperties(physDevices[0], &vkPDProps);

    //
    physDevice_ = physDevices[0];
}

void VulkanAPI::createDevice() {


    float priorities[] = { 1.0 };
    VkDeviceQueueCreateInfo deviceQueueInfo = {};
    deviceQueueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    deviceQueueInfo.pNext = NULL;
    deviceQueueInfo.flags = 0;
    deviceQueueInfo.queueFamilyIndex = 0;
    deviceQueueInfo.queueCount = 1;
    deviceQueueInfo.pQueuePriorities = &priorities[0];

    std::vector<const char*>  deviceEnabledExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

    VkDeviceCreateInfo deviceInfo = {};
    deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceInfo.pNext = NULL;
    deviceInfo.flags = 0;
    deviceInfo.queueCreateInfoCount = 1;
    deviceInfo.pQueueCreateInfos = &deviceQueueInfo;
    deviceInfo.enabledExtensionCount = deviceEnabledExtensions.size();
    deviceInfo.ppEnabledExtensionNames = deviceEnabledExtensions.data();
    deviceInfo.pEnabledFeatures = NULL;

    VkResult res = vkCreateDevice(physDevice_, &deviceInfo, NULL, &device_);
    if (res != VK_SUCCESS)
        throw std::runtime_error("Failed to create logical device!");
}

void VulkanAPI::createSurface()
{
#ifdef __linux__
    VkXcbSurfaceCreateInfoKHR surfaceCreateInfo = {};
    surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
    surfaceCreateInfo.pNext = NULL;
    surfaceCreateInfo.flags = 0;
    surfaceCreateInfo.connection = connPtr_;
    surfaceCreateInfo.window = windowID_;

    VkResult res = vkCreateXcbSurfaceKHR(instance_, &surfaceCreateInfo, nullptr, &surface_);
#else
    
    VkResult res{};
#endif
    if (res != VK_SUCCESS)
    {
        throw std::runtime_error("Failed to create surface!");
    }
}

void VulkanAPI::detectQueue() {
    uint32_t queueCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physDevice_, &queueCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueProperties(queueCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physDevice_, &queueCount, queueProperties.data());

    std::vector<VkBool32> supportsPresenting(queueCount);

    // graphics
    for (uint32_t i = 0; i < queueCount; i++) {
        vkGetPhysicalDeviceSurfaceSupportKHR(physDevice_, i, surface_, &supportsPresenting[i]);

        if ((queueProperties[i].queueFlags && VK_QUEUE_GRAPHICS_BIT) != 0)
        {
            if (supportsPresenting[i] == VK_TRUE)
                graphicsQueue = i;

            break;
        }
    }

    // computing
    for (uint32_t i = 0; i < queueCount; i++) {
        vkGetPhysicalDeviceSurfaceSupportKHR(physDevice_, i, surface_, &supportsPresenting[i]);

        if ((queueProperties[i].queueFlags && VK_QUEUE_COMPUTE_BIT) != 0)
        {
            computeQueue = i;
            break;
        }
    }

}

void VulkanAPI::getSurfaceFormats()
{
    uint32_t formatCount = 0;
    VkResult res = vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice_, surface_, &formatCount, nullptr);
    assert(res == VK_SUCCESS);
    assert(formatCount > 0);

    std::vector<VkSurfaceFormatKHR> surfaceFormat(formatCount);
    res = vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice_, surface_, &formatCount, surfaceFormat.data());
    assert(res == VK_SUCCESS);

    if ((formatCount == 1) && (surfaceFormat[0].format == VK_FORMAT_UNDEFINED))
    {
        colorFormat = VK_FORMAT_B8G8R8A8_UNORM;
    }
    else
    {
        colorFormat = surfaceFormat[0].format;
    }

    colorSpace = surfaceFormat[0].colorSpace;

}

void VulkanAPI::run() 
{
    ;
}